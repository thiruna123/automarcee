<?php include_once('header.php');
if($uid > 0):
$sql = mysql_query("SELECT * FROM ms_customer WHERE CSTMR_ID = '$uid'");
$row = mysql_fetch_assoc($sql);	
$nsql = mysql_query("SELECT * FROM ms_newsletter WHERE CSTMR_ID = '$row[CSTMR_ID]'");
$nrow = mysql_fetch_assoc($nsql);	
endif;?>

<!-- 
	SLIDER
	Classes:
		.fullheight = full height slider
-->
<!-- <section style="background:url(assets/images/dashboard_bg.png) 0px 0px no-repeat; height: 425px;"></section> -->
<section class="padding" style="background:#000; height: 80px;">
<div class="container">		
<div class="row">
<div class="col-lg-6"><h3 class="color-white">MY DASHBOARD</h3> </div>
</div>
</div>
</section>
<!-- /SLIDER -->
<!-- -->
<section class="nopadding-bottom">
	<div class="container">

		<!-- RIGHT -->
		<div class="col-lg-9 col-md-9 col-sm-8 col-lg-push-3 col-md-push-3 col-sm-push-4 margin-bottom-80">
			<div class="tab-content margin-top-20">

				<!-- AVATAR TAB -->
				<div class="tab-pane fade in active" id="avatar">

							<div class="row">

								<div class="col-md-12 col-sm-12">
									
									<h3 class="nomargin-bottom">Hello <?=$row['FIRST_NAME']." ".$row['LAST_NAME'];?>! </h3>
									<p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
									
								<div class="col-md-8 col-sm-8">
										<h3 class="color-orange ">Account Information </h3>
										<h4 class="nomargin-bottom">Contact Information </h4>
										
										<button type="submit" class="btn btn-primary noradius pull-right acc-tab" name="smt_membership">
							Edit</button>
									<div class="progress progress-sx"></div>
										<h5 class="color-grey nomargin-bottom"><?=$row['FIRST_NAME']." ".$row['LAST_NAME'];?></h5>
										<h5 class="color-grey "><?=$row['M_PHONE'];?><br>
										<?=$row['EMAIL'];?></h5>
										
										<button type="submit" class="btn btn-primary noradius pull-left" name="smt_membership">
							Change Password</button>

								</div>    
									 
								<div class="col-md-12 col-sm-12 padding-top-20">

										<h4 class=" nomargin-bottom">Newsletters </h4>
									<button type="submit" class="btn btn-primary nomargin noradius pull-right acc-tab" name="smt_membership">Edit</button>
										<div class="progress progress-sx"></div>
										<? if($nrow['N_STS'] == 1): ?>
											<h5 class="color-grey">You are currently subscribed to Tasteclub 966 newsletters.</h5>
										<? else: ?>
											<h5 class="color-grey">You are currently not subscribed to any newsletters.</h5>
										<? endif; ?>
									
								</div> 
									
								<div class="col-md-12 col-sm-12">
									

										<h4 class="nomargin-bottom">Address Book </h4>
									<button type="submit" class="btn btn-primary nomargin noradius pull-right acc-tab" name="smt_membership">Manage Address</button>
										<div class="progress progress-sx"></div>
										<p> xxxxxxxxxx (Billing Address)</p>
										
										<a href="<?=$root_path.$slang;?>/user-address" class="btn btn-primary nomargin noradius pull-left" name="smt_membership">Edit Address</a>
							  </div>     
								
								<div class="col-md-12 col-sm-12 padding-top-30 ">
									<p> xxxxxxxxxx (Shipping Address)</p>
									<a href="<?=$root_path.$slang;?>/user-address" class="btn btn-primary nomargin noradius pull-left" name="smt_membership">Edit Address</a>
								</div>

									
									
								</div>

							</div>

				</div>
				<!-- /AVATAR TAB -->
			</div>

		</div>

		
		<!-- LEFT -->
		<div class="col-lg-3 col-md-3 col-sm-4 col-lg-pull-9 col-md-pull-9 col-sm-pull-8">
		
			<!-- completed -->
			<div class="margin-bottom-30">
				<h3 class="nomargin-bottom">My Account </h3>
				<div class="progress progress-sx"></div>
			<!-- /completed -->

				<ul class="nav nav-tabs nopadding-left">
					<li class="active"><a href="<?php echo BASEPATH; ?>user-dashboard">Account Dashboard</a></li>
					<li><a href="<?php echo BASEPATH; ?>user-profile">Account Information</a></li>
					<li><a href="<?php echo BASEPATH; ?>user-addressbook">Address Book</a></li>
					<li><a href="<?php echo BASEPATH; ?>user-order">My Orders</a></li>
					<!-- <li><a href="#info" data-toggle="tab">Recurring Profiles</a></li> -->
					<li><a href="<?php echo BASEPATH; ?>user-newsletter">Newsletter Subscriptions</a></li>
					<!--<li><a href="<?=$root_path.$slang;?>/purchased-cards">Purchased Cards</a></li>
					 <li><a href="<?=$root_path.$slang;?>claimed-offers">Claimed Offers</a></li> -->
				</ul>
		   </div>
		</div>
</section>
<!-- / -->
<?php include_once('footer.php');  ?>