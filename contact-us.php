<?php include_once('header.php'); ?>
<section class="page-header page-header-xlg parallax parallax-3" style="background-image:url('assets/img/contact-bg.png')">				<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>

				<div class="container">

					<h1 class="size-50 font-alice">CONTACT US</h1>
				</div>
			</section>
			<!-- /PAGE HEADER -->


<!-- -->
<section>
    <div class="container">
        <div class="row">
            <div class=" col-sm-7">				

                    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:440px;width:700px;'><div id='gmap_canvas' style='height:440px;width:700px;'></div><div><small><a href="http://embedgooglemaps.com">									embed google maps							</a></small></div><div><small><a href="https://disclaimergenerator.net">disclaimer generator</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(25.03428,-77.39627999999999),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(25.03428,-77.39627999999999)});infowindow = new google.maps.InfoWindow({content:'<strong>Automarce</strong><br>Bahamas<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                        
            </div>
        

        <div class="clo-sm-5 contact-over-box pull-right">

            <h3 class="size-20">Drop us a line or just say <strong><em>Hello!</em></strong></h3>


            <!--
                MESSAGES

                    How it works?
                    The form data is posted to php/contact.php where the fields are verified!
                    php.contact.php will redirect back here and will add a hash to the end of the URL:
                        #alert_success 		= email sent
                        #alert_failed		= email not sent - internal server error (404 error or SMTP problem)
                        #alert_mandatory	= email not sent - required fields empty
                        Hashes are handled by assets/js/contact.js

                    Form data: required to be an array. Example:
                        contact[email][required]  WHERE: [email] = field name, [required] = only if this field is required (PHP will check this)
                        Also, add `required` to input fields if is a mandatory field. 
                        Example: <input required type="email" value="" class="form-control" name="contact[email][required]">

                    PLEASE NOTE: IF YOU WANT TO ADD OR REMOVE FIELDS (EXCEPT CAPTCHA), JUST EDIT THE HTML CODE, NO NEED TO EDIT php/contact.php or javascript
                                 ALL FIELDS ARE DETECTED DINAMICALY BY THE PHP

                    WARNING! Do not change the `email` and `name`!
                                contact[name][required] 	- should stay as it is because PHP is using it for AddReplyTo (phpmailer)
                                contact[email][required] 	- should stay as it is because PHP is using it for AddReplyTo (phpmailer)
            -->

            <!-- Alert Success -->
            <div id="alert_success" class="alert alert-success margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Thank You!</strong> Your message successfully sent!
            </div><!-- /Alert Success -->


            <!-- Alert Failed -->
            <div id="alert_failed" class="alert alert-danger margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>[SMTP] Error!</strong> Internal server error!
            </div><!-- /Alert Failed -->


            <!-- Alert Mandatory -->
            <div id="alert_mandatory" class="alert alert-danger margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Sorry!</strong> You need to complete all mandatory (*) fields!
            </div><!-- /Alert Mandatory -->


            <form id="" action="php/contact.php" method="post" enctype="multipart/form-data">
                <fieldset>
                    <input type="hidden" name="action" value="contact_send" />

                    <div class="row">
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:name">Full Name *</label>
                            <input required type="text" value="" class="form-control" name="contact[name][required]" id="contact:name">
                        </div>
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:email">E-mail Address *</label>
                            <input required type="email" value="" class="form-control" name="contact[email][required]" id="contact:email">
                        </div>
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:phone">Phone</label>
                            <input required type="text" value="" class="form-control" name="contact[phone]" id="contact:phone">
                        </div>

                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:subject">Subject *</label>
                            <input required type="text" value="" class="form-control" name="contact[subject][required]" id="contact:subject">
                        </div>
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact_department">Department</label>
                            <select class="form-control pointer" name="contact[department]">
                                <option value="">--- Select ---</option>
                                <option value="Marketing">Marketing</option>
                                <option value="Webdesign">Webdesign</option>
                                <option value="Architecture">Architecture</option>
                            </select>
                        </div>

                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:message">Message *</label>
                            <textarea required maxlength="10000" rows="6" class="form-control" name="contact[message]" id="contact:message"></textarea>
                        </div>
                    </div>

                </fieldset>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> SEND MESSAGE</button>
                    </div>
                </div>
            </form>


        </div>
            </div>
    </div>
</section>
<!-- / -->
<?php include_once('footer.php'); ?>