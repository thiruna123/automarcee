<?php include_once('header.php'); ?>
<section class="page-header page-header-xlg parallax parallax-3" style="background-image:url('assets/img/contact-bg.png')">				<div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>
<div class="container">
<h1 class="size-50 font-alice">Request Car</h1>
</div>
</section>
			<!-- /PAGE HEADER -->


<!-- -->
<section>
    <div class="container">
        <div class="row">
          
        

        <div class="col-sm-6 col-sm-offset-4 contact-over-box text-center">

            <h3 class="size-20">Fill and select the car as per your <strong><em>wish!</em></strong></h3>


            <!--
                MESSAGES

                    How it works?
                    The form data is posted to php/contact.php where the fields are verified!
                    php.contact.php will redirect back here and will add a hash to the end of the URL:
                        #alert_success 		= email sent
                        #alert_failed		= email not sent - internal server error (404 error or SMTP problem)
                        #alert_mandatory	= email not sent - required fields empty
                        Hashes are handled by assets/js/contact.js

                    Form data: required to be an array. Example:
                        contact[email][required]  WHERE: [email] = field name, [required] = only if this field is required (PHP will check this)
                        Also, add `required` to input fields if is a mandatory field. 
                        Example: <input required type="email" value="" class="form-control" name="contact[email][required]">

                    PLEASE NOTE: IF YOU WANT TO ADD OR REMOVE FIELDS (EXCEPT CAPTCHA), JUST EDIT THE HTML CODE, NO NEED TO EDIT php/contact.php or javascript
                                 ALL FIELDS ARE DETECTED DINAMICALY BY THE PHP

                    WARNING! Do not change the `email` and `name`!
                                contact[name][required] 	- should stay as it is because PHP is using it for AddReplyTo (phpmailer)
                                contact[email][required] 	- should stay as it is because PHP is using it for AddReplyTo (phpmailer)
            -->

            <!-- Alert Success -->
            <div id="alert_success" class="alert alert-success margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Thank You!</strong> Your message successfully sent!
            </div><!-- /Alert Success -->


            <!-- Alert Failed -->
            <div id="alert_failed" class="alert alert-danger margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>[SMTP] Error!</strong> Internal server error!
            </div><!-- /Alert Failed -->


            <!-- Alert Mandatory -->
            <div id="alert_mandatory" class="alert alert-danger margin-bottom-30">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Sorry!</strong> You need to complete all mandatory (*) fields!
            </div><!-- /Alert Mandatory -->
<?php 
if(isset($_POST['request_send']))
{
	$name = $_POST['name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$brand = $_POST['brand'];
	$car_name = $_POST['car_name'];
	$condition = $_POST['condition'];
	$model = $_POST['model'];
	$gear_type = $_POST['gear_type'];
	$body_type = $_POST['body_type'];
	$message = $_POST['message'];
	mysql_query("INSERT INTO ms_request_car(NAME,EMAIL,PHONE,BRAND,CAR_NAME,CNDTN,MODEL,GEAR_TYPE,BODY_TYPE,MESSAGE) VALUES('$name', '$email', '$phone', '$brand', '$car_name', '$condition', '$model', '$gear_type', '$body_type', '$message')") or die('Error in requested cars details inserting query !!!!');

	$to = "radhakrishnanskr@gmail.com"; // this is your Email address
    $from = $email; // this is the sender's Email address
    $first_name = $name;
    $subject = "Form submission";
    $subject2 = "Copy of your form submission";
    $message = $first_name . " wrote the following:" . "\n\n" . $message;
    $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $message;

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    
	
	?> 
<script type="text/javascript"> alert("Your request send successfully !!!"); window.location.href="<?php echo BASEPATH; ?>request-car"; </script>
<?php } ?>

            <form method="post" enctype="multipart/form-data">
                <fieldset>
                    <input type="hidden" name="action" value="contact_send" />

                    <div class="row">
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:name">Full Name *</label>
                            <input required type="text" value="" class="form-control" name="name" id="contact:name">
                        </div>
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:email">E-mail Address *</label>
                            <input required type="email" value="" class="form-control" name="email" id="contact:email">
                        </div>
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:phone">Phone</label>
                            <input required type="text" value="" class="form-control" name="phone" id="contact:phone">
                        </div>

                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:brand">Brand *</label>
                            <input required type="text" value="" class="form-control" name="brand" id="contact:brand">
                        </div>
						
						<div class="col-md-12 margin-bottom-20">
                            <label for="contact:car-name">Car name *</label>
                            <input required type="text" value="" class="form-control" name="car_name" id="contact:car_name">
                        </div>
						
                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:condition">Condition</label>
                            <select class="form-control pointer" name="condition">
                                <option value="">--- Select ---</option>
                                <option value="New">New</option>
                                <option value="Old">Old</option>
                             </select>
                        </div>
						
						<div class="col-md-12 margin-bottom-20">
                            <label for="contact:model">Model</label>
                            <select class="form-control pointer" name="model">
                                <option value="">--- Select ---</option>
                                <option value="Sport">Sport</option>
                                <option value="Normal">Normal</option>
                             </select>
                        </div>
						
						<div class="col-md-12 margin-bottom-20">
                            <label for="contact:gear type">Gear Type</label>
                            <select class="form-control pointer" name="gear_type">
                                <option value="">--- Select ---</option>
                                <option value="Transmission">Transmission</option>
                                <option value="Automatic">Automatic</option>
								<option value="Manual">Manual</option>
                             </select>
                        </div>
						
						<div class="col-md-12 margin-bottom-20">
                            <label for="contact:body type">Body Type</label>
                            <select class="form-control pointer" name="body_type">
                                <option value="">--- Select ---</option>
                                <option value="Hatchback">Hatchback</option>
                                <option value="Sedan">Sedan</option>
								<option value="Jeep">Jeep</option>
                             </select>
                        </div>

                        <div class="col-md-12 margin-bottom-20">
                            <label for="contact:message">Message *</label>
                            <textarea required maxlength="10000" name="message" rows="4" cols="40"></textarea>
                        </div>
                    </div>

                </fieldset>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="request_send" class="btn btn-primary"><i class="fa fa-check"></i> SEND MESSAGE</button>
                    </div>
                </div>
            </form>


        </div>
            </div>
    </div>
</section>
<!-- / -->
<?php include_once('footer.php'); ?>