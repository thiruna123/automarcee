<?php include_once('header.php'); ?> 
<section class="page-header page-header-xs">
<div class="container">
<h1>SHOP CART</h1>
</div>
</section>
<section>
<div class="container">
<div class="row">
<div class="col-lg-12 col-sm-12">
<form class="cartContent clearfix" name="car_tire" method="post" action="<?php echo BASEPATH; ?>shop-checkout">
<div id="cartContent">
<div class="item head clearfix">
<span class="cart_img product_name bold">PRODUCT NAME</span>
<span class="product_name cart_img size-13 bold">PRODUCT IMAGE</span>
<span class="remove_item size-13 bold"></span>
<span class="total_price size-13 bold">SUB TOTAL</span>
<span class="qty size-13 bold">QUANTITY</span>
</div>							<!-- cart item -->
<?php 
$cart = unserialize(serialize($_SESSION['cart']));
$s = 0;
$indexed = 0;
for($i=0; $i<count($cart); $i++){
if(!empty($cart[$i]->prdct_price)){ $s += $cart[$i]->prdct_price * $cart[$i]->prdct_qnty; } 
if(!empty($cart[$i]->prdct_cost)){ $s += $cart[$i]->prdct_cost * $cart[$i]->prdct_qnty; }
?>
<div class="item">
<div class="cart_img  padding-10">
<a class="product_name">
<input type="hidden" name="prdct_id" class="cls_prdct_id" id="id_prdct" value="<?php echo $cart[$i]->prdct_id; ?>"/>
<input type="hidden" name="prdct_name" class="cls_prdct_name" id="id_prdct_name" value="<?php echo $cart[$i]->prdct_name; ?>"/>
<span><?php echo $cart[$i]->prdct_name; ?></span>
</a>
</div>
<div class="cart_img  padding-10 "><img src="<?php echo BASEPATH.'admin/product_image/'.$cart[$i]->prdct_image; ?>" alt="" width="200" /></div>    
<a href="<?php echo BASEPATH; ?>shop-cart.php?indexed=<?php echo $indexed; ?>&&<?php if(isset($car_item) && $car_item=='index'){ ?>car_page=<?php echo $car_item; }elseif(isset($tire_item) && $tire_item=='tyre-list'){ ?>tire_page=<?php echo $tire_item; }else{?>index<?php }  ?>" class="remove_item" onclick="return confirm('Are you sure?')">
<i class="fa fa-times"></i></a>  
<div class="total_price">
<input type="hidden" name="sub_tot" class="cls_sub_tot" id="id_sub_tot" value="<?php if(!empty($cart[$i]->prdct_price)){ echo $cart[$i]->prdct_price * $cart[$i]->prdct_qnty; } if(!empty($cart[$i]->prdct_cost)){ echo $cart[$i]->prdct_cost * $cart[$i]->prdct_qnty; } ?>"/>
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'; }else{ echo 'USD'.'-'; } ?><span><?php if(!empty($cart[$i]->prdct_price)){ echo $cart[$i]->prdct_price * $cart[$i]->prdct_qnty; } if(!empty($cart[$i]->prdct_cost)){ echo $cart[$i]->prdct_cost * $cart[$i]->prdct_qnty; } ?>
<!---<input type="text" id="sub_tot_id" value="<?php echo $cart[$i]->prdct_qnty * $cart[$i]->prdct_price; ?>"/>--->
</span></div>
<div class="qty">
<input type="hidden" name="qnty" class="cls_qnty" id="id_qnty" value="<?php echo $cart[$i]->prdct_qnty; ?>"/>
<?php echo $cart[$i]->prdct_qnty; ?>
<!---<input type="number" id="qnty_id" onchange="each_price();" value="<?php echo $cart[$i]->prdct_qnty; ?>" name="qty" maxlength="3" max="999" min="1" />--->&nbsp;&nbsp;*&nbsp;&nbsp;
<input type="hidden" name="price" class="cls_price" id="id_price" value="<?php if(!empty($cart[$i]->prdct_price)){ echo $cart[$i]->prdct_price; } if(!empty($cart[$i]->prdct_cost)){ echo $cart[$i]->prdct_cost; } ?>"/>
<?php if(!empty($cart[$i]->prdct_price)){ echo $cart[$i]->prdct_price; } if(!empty($cart[$i]->prdct_cost)){ echo $cart[$i]->prdct_cost; } ?>
<!---<input type="text" id="each_price_id" value="<?php echo $cart[$i]->prdct_price; ?>"/>---></div>
  <div class="clearfix"></div>
</div>
<?php $indexed++; } ?>
<script type="text/javascript">
function each_price()
{
var get_qnty = $('#qnty_id').val(); 
var get_each_price = $('#each_price_id').val();
var sub_tot = + get_qnty *  + get_each_price; 
$('#sub_tot_id').val(sub_tot);
}
</script>
<!-- /cart item -->
<div class="item" align="right"><br/>
<table border="0">
<tr>
<td width="150" align="right"><span style="font-size: 20px; font-weight: bold;">TOTAL&nbsp;&nbsp;:</span></td>
<td width="150" align="right">
<input type="hidden" id="total" name="total" value="<?php echo $s; ?>"/>
<span style="font-size: 20px; font-weight: bold;"><?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'; }else{ echo 'USD'.'-'; } echo $s; ?></span></td>
</tr>
</table>
</div>
<div class="add_prdct_id"></div>
<div class="add_prdct_name"></div>
<div class="add_qnty"></div>
<div class="add_price"></div>
<div class="add_sub_tot"></div>
<!-- update cart -->
<button type="submit" onclick="checkout_shopcart();" name="car_and_tire" class="btn btn-danger margin-top-20 margin-right-10 pull-right"><i class="glyphicon glyphicon-forward" style="color: white;"></i>Proceed to Checkout</button>
<button class="btn btn-success margin-top-20 margin-right-10 pull-right"><i class="glyphicon glyphicon-ok"></i> <a class="color-white" href="<?php echo BASEPATH; ?>car-sale">UPDATE CART</a></button>
<!-- /update cart -->
<div class="clearfix"></div>
</div>
<!-- /cart content -->
</form>
<!-- /CART -->
</div>
</section>
<!-- /CART -->
<script type="text/javascript">
function checkout_shopcart(t){ 
var ext_prdct_id="";
$(".cls_prdct_id").each(function(){
ext_prdct_id = ext_prdct_id+"<input type='hidden' name='prdct_id[]' value='"+$(this).val()+"' />";
})
$(".add_prdct_id").append(ext_prdct_id);
$("form [name='car_tire']").submit();
var ext_prdct_name="";
$(".cls_prdct_name").each(function(){
ext_prdct_name = ext_prdct_name+"<input type='hidden' name='prdct_name[]' value='"+$(this).val()+"' />";
})
$(".add_prdct_name").append(ext_prdct_name);
$("form [name='car_tire']").submit();
var ext_qnty="";
$(".cls_qnty").each(function(){
ext_qnty = ext_qnty+"<input type='hidden' name='qnty[]' value='"+$(this).val()+"' />";
})
$(".add_qnty").append(ext_qnty);
$("form [name='car_tire']").submit();
var ext_price="";
$(".cls_price").each(function(){
ext_price = ext_price+"<input type='hidden' name='price[]' value='"+$(this).val()+"' />";
})
$(".add_price").append(ext_price);
$("form [name='car_tire']").submit();
var ext_sub_tot="";
$(".cls_sub_tot").each(function(){
ext_sub_tot = ext_sub_tot+"<input type='hidden' name='sub_tot[]' value='"+$(this).val()+"' />";
})
$(".add_sub_tot").append(ext_sub_tot);
$("form [name='car_tire']").submit();
}
</script>
<?php include_once('footer.php'); ?>