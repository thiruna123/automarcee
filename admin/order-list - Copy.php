<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Order List</h1>
      <ol class="breadcrumb">
        <li><a class="menu-col" href="index.php">Home</a></li>
        <li><a class="menu-col" href="#">Orders</a></li>
        <li class="active">Order list</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">
   <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <!---<a href="prdct-add.php"><button>Add New order </button></a>--->
        </div>
        <div class="panel-body table-responsive">
<table id="example0" class="table display">
               <thead>
                    <tr>
                        <th>ID</th>
                        <!--<th>Order ID</th>-->
						<th>Customer Name</th> 	
                        <th>Category</th>						
                       	<th>Price</th>
						<th>Status</th>
						<th>Payment Option</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
                </thead>
            
                <tfoot>
                    <tr>
                        <th>ID</th>  
                       <!--<th>Order ID</th>-->						
						<th>Customer Name</th> 	
                        <th>Category</th>						
                       	<th>Price</th>
						<th>Status</th>
						<th>Payment Option</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
                </tfoot>
             
                <tbody> 
<?php 
$prdct_tb = mysql_query("SELECT t1.ORD_ID, t1.CSTMR_ID, t1.PRDCT_ID, t1.PRDCT_NAME, 
t1.SUB_TOT, t1.STATUS, t1.PAYMENT_OPTION, t1.CREATED_ON, t2.payment_id, t2.txn_id, 
t2.payment_status, t3.CTGRY_NAME FROM ms_order t1 LEFT JOIN payments t2 ON t1.PRDCT_ID = t2.item_number
LEFT JOIN prdct_in_ctgry t3 ON t1.PRDCT_ID = t3.PRDCT_ID
GROUP BY t1.PRDCT_ID") or die("Error in order listing selecting query !!!"); 
 while($prdct_rw = mysql_fetch_assoc($prdct_tb)){
$order_id = $prdct_rw['ORD_ID'];
$cstmr_id = $prdct_rw['CSTMR_ID'];
$prd_id = explode(',',$prdct_rw['PRDCT_ID']);
$prd_name = explode(',',$prdct_rw['PRDCT_NAME']);
$prd_subtot = explode(',',$prdct_rw['SUB_TOT']);
$status = $prdct_rw['STATUS'];
$pay_option = $prdct_rw['PAYMENT_OPTION'];
$dates = $prdct_rw['CREATED_ON'];
$payment_id = $prdct_rw['payment_id'];
$payment_status = $prdct_rw['payment_status'];
$txn_id = $prdct_rw['txn_id'];
$cnt = count($prd_id);	
$ct = 0; 
while($ct<$cnt){ 
$prdct = mysql_query("
SELECT t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.CTGRY_NAME FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_in_ctgry t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
WHERE t1.PRDCT_ID = '$prd_id[$ct]'
GROUP BY t1.PRDCT_ID
") or die("Error in product listing selecting query !!!"); 
$prd_rw = mysql_fetch_assoc($prdct);
$cst_tb = mysql_query("SELECT * FROM ms_customer WHERE CSTMR_ID = '$cstmr_id'") or die("error in custmer select query !!!");
$cst_rw = mysql_fetch_assoc($cst_tb);
$first_name = $cst_rw['FIRST_NAME'];
$last_name = $cst_rw['LAST_NAME'];
?>

<tr>
<td><?php echo $ct+1; ?></td>
<!--<td><?php echo $order_id; ?></td>-->
<td><?php echo $first_name.'-'.$last_name; ?></td>
<td><?php if(isset($prd_rw['CTGRY_NAME']) && $prd_rw['CTGRY_NAME'] == 'Car sale' || $prd_rw['CTGRY_NAME'] == 'Our Cars' || $prd_rw['CTGRY_NAME'] == 'Latest Arrivals' || $prd_rw['CTGRY_NAME'] == 'Discounted Cars'){ echo 'Car sale'; }elseif(isset($prd_rw['CTGRY_NAME']) && $prd_rw['CTGRY_NAME'] == 'Car rental'){ echo 'Car rental'; }elseif(isset($prd_rw['CTGRY_NAME']) && $prd_rw['CTGRY_NAME'] == 'Car tyre sale' || $prd_rw['CTGRY_NAME'] == 'Discounted tires' || $prd_rw['CTGRY_NAME'] == 'New tires' || $prd_rw['CTGRY_NAME'] == 'Used tires'){ echo 'Car tyre sale'; }  ?></td>
<td><?php echo $prd_subtot[$ct]; ?></td>
<td><?php echo $status; ?></td>
<td><?php if($pay_option==1){ echo "Paypal"; }elseif($pay_option==2){ echo "Bank transfer"; }elseif($pay_option==3){ echo "Cash on Delivery"; } ?></td>
<td><?php echo $dates; ?></td>
<td><a href="order-full-details.php?prdct_id=<?php echo $prdct_rw['PRDCT_ID']; ?>&&cstmr_id=<?php echo $cstmr_id; ?>">Preview</a></td>
</tr>
<?php $ct++; } } ?>
</tbody></table>
</div>

</div>
</div>
<!-- End Panel -->
</div>
<!-- End Row -->






</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a class="menu-col" href="http://www.automarce.com" target="_blank">Automarce</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a class="menu-col" href="http://www.brightlivingstone.com/ecommerce-website-development.php"target="_blank">Ecommerce Website Development</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>

<!-- Action for Preview, edit, delete script start --->
<script type="text/javascript"> 
function prdct_action(act_id)
{
var act = $(act_id).val();	
var split_action = act.split(','); 
if(split_action[0]=='edit_id') 
{
	window.location.href='prdct-add.php?edit_id='+split_action[1];
}
if(split_action[0]=='delete_id')
{
	if (confirm("Do your really want to delete this product?"))
        {
            window.location.href= 'prdct-add.php?delete_id='+split_action[1]; 
        }
        else
        {
           window.location.href = 'index.php'; 
        } 
}
}
</script>   
<!-- Action for Preview, edit, delete script end --->

</body>
</html>