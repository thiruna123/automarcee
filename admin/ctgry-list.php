<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Products List</h1>
      <ol class="breadcrumb">
        <li><a class="menu-col" href="index.html">Home</a></li>
        <li><a  class="menu-col" href="#">Category</a></li>
        <li class="active">Category list</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <a href="ctgry-add.php"><button class="new-button">Add New Category </button></a>
        </div>
        <div class="panel-body table-responsive">

            <table id="example0" class="table display">
                <thead>
                    <tr>
                        <th>Check</th>
                        <th>ID</th>                       
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created On</th>
						<th>Displayed</th>						
					    <th>Action</th>
                    </tr>
                </thead>
             
                <tfoot>
                    <tr>
                        <th>Check</th>
                        <th>ID</th>                       
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created On</th>
						<th>Displayed</th>						
					    <th>Action</th>
                    </tr>
                </tfoot>
             
                <tbody>
				    <!--<tr>
                        <td align="left"></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 300px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="submit" name="" value="Search"/></td>						
                     </tr>--->
<?php
$delete_id = $_GET['delete_id'];
if(isset($_GET['delete_id']) && $_GET['delete_id'] != ''):
$delete_id = (int)$_GET['delete_id'];
mysql_query("DELETE FROM category WHERE CTGRY_ID = '$delete_id' || SUB_CTGRY = '$delete_id'") or die('Category delete query error !!!');
?> <script type="text/javascript"> alert("Deleted Succesfully !!!"); window.location.href="ctgry-list.php"; </script>
<?php endif; 

$view_id = $_GET['view_id'];
if(isset($_GET['view_id']) && $_GET['view_id'] == $_GET['view_id']):
$view_id = (int)$_GET['view_id'];
$ctvw_tb = mysql_query("SELECT * FROM category WHERE CTGRY_ID = '$view_id' || SUB_CTGRY = '$view_id'") or die('Error in selected category listing select query !!!');  
while($ctvw_rw = mysql_fetch_assoc($ctvw_tb)): ?>
<tr>
                        <td><input type="checkbox" name="" value=""/></td>
						<td><?=$ctvw_rw['CTGRY_ID']; ?></td>
                        <td><?=$ctvw_rw['MAIN_CTGRY']; ?></td>
                        <td><?=$ctvw_rw['CTGRY_DESC']; ?> </td>
                        <td><?=$ctvw_rw['CREATED_ON']; ?></td>
                        <td><?=$ctvw_rw['DISPLAY']; ?></td>                        
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php 
$ctlstvw_tb = mysql_query("SELECT * FROM category WHERE SUB_CTGRY = '$view_id'") or die('Error in last category listing select query !!!'); 
$ctlstvw_rw = mysql_fetch_assoc($ctlstvw_tb);
if(isset($ctlstvw_rw['SUB_CTGRY']) && $ctlstvw_rw['SUB_CTGRY'] != ''): ?>
<a href="ctgry-list.php?view_id=<?=$ctvw_rw['CTGRY_ID']; ?>">View</a><br/>
<?php endif; ?>						
						<a href="ctgry-add.php?edit_id=<?=$ctvw_rw['CTGRY_ID']; ?>">Edit</a>
						&nbsp;&nbsp;
						<a href="ctgry-list.php?delete_id=<?=$ctvw_rw['CTGRY_ID']; ?>">Delete</a>
						</td>
                    </tr>
<?php 
endwhile;
else:
$ctgry_tb = mysql_query("SELECT * FROM category") or dir('Error in category listing select query !!!'); 
$i = 1;
while($ctrgy_rw = mysql_fetch_assoc($ctgry_tb)):
?>
                        <tr>
                        <td><input type="checkbox" name="" value=""/></td>
						<td><?php echo $i; ?></td>
                        <td><?=$ctrgy_rw['MAIN_CTGRY']; ?></td>
                        <td><?=$ctrgy_rw['CTGRY_DESC']; ?> </td>
                        <td><?=$ctrgy_rw['CREATED_ON']; ?></td>
                        <td><?=$ctrgy_rw['DISPLAY']; ?></td>                        
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="ctgry-list.php?view_id=<?=$ctrgy_rw['CTGRY_ID']; ?>">View</a><br/>
						<a href="ctgry-add.php?edit_id=<?=$ctrgy_rw['CTGRY_ID']; ?>">Edit</a>
						&nbsp;&nbsp;
						<a href="ctgry-list.php?delete_id=<?=$ctrgy_rw['CTGRY_ID']; ?>">Delete</a>
						</td>
                        </tr>
<?php $i++; endwhile; endif; ?>
                  
                   
                </tbody>
            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->






</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a class="menu-col" href="http://www.automarce.com" target="_blank">Automarce</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a class="menu-col" href="http://www.brightlivingstone.com/ecommerce-website-development.php"  target="_blank">Ecommerce Website Development</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>


</body>
</html>