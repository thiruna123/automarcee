<?php include_once('header.php'); ?>

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">New product image</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Catalog</a></li>
        <li class="active">Add Product Image</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">
<?php
if(isset($_POST['send_image'])):

foreach ($_FILES['file']['name'] as $f => $name) {
 $allowedExts = array("gif", "jpeg", "jpg", "png");
    $temp = explode(".", $name);
    $extension = end($temp);
	$name=uniqid().$name;
if ((($_FILES["file"]["type"][$f] == "image/gif")
|| ($_FILES["file"]["type"][$f] == "image/jpeg")
|| ($_FILES["file"]["type"][$f] == "image/jpg")
|| ($_FILES["file"]["type"][$f] == "image/png"))
&& ($_FILES["file"]["size"][$f] < 2000000)
&& in_array($extension, $allowedExts))
{
  if ($_FILES["file"]["error"][$f] > 0)
  {    echo "Return Code: " . $_FILES["file"]["error"][$f] . "<br>";  }
  else
  {    if (file_exists("photo_upload/" . $name))    {    }
    else
    { move_uploaded_file($_FILES["file"]["tmp_name"][$f], "photo_upload/" . $name); }
  }
}
else
{  $error =  "Invalid file"; }
}
			
$caption = $_POST['caption'];
$position = $_POST['position'];
mysql_query("INSERT INTO prdct_image (PRDCT_THUMBNAIL,PRDCT_IMAGE,CAPTION,POSITION) VALUES ('$name', '$name', '$caption', '$position')") or die("Error in product image insert query query !!!");
?><script type="text/javascript"> alert("Upadte Successfully !!!");  window.location.href="prdct-images.php"; </script> 
<?php endif; ?>

  
  <!-- Start Row -->
  <div class="row">
<div class="col-md-2">
 <style> ul {padding-left: 0px;} ul.prdct_menu li {display: block; cursor: pointer; padding: 10px; background: #fff; border-radius: 7px; border: 1px solid #f5f5f5; } </style>
 <ul class="prdct_menu">
 <li> <a href="index.php">Product List </a></li>
 <li> <a href="prdct-add.php">Add Product </a></li>
 <li> <a href="prdct-price.php">Price </a></li>
 <li> <a href="prdct-seo.php">SEO </a></li>
 <li> <a href="prdct-category.php">Category associate </a></li>
 <li> <a href="prdct-shipping.php">Shipping </a></li>
 <li> <a href="prdct-attribute-combination.php">Attribute combination </a></li>
 <li> <a href="prdct-quantity.php">Quantity </a></li>
 <li> <a href="prdct-images.php">Images </a></li>
 </ul>
 </div>
    <div class="col-md-10">
      <div class="panel panel-default">

        <div class="panel-title">
          Product Image
          <ul class="panel-tools">
            <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
            <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>

            
              <form class="form-horizontal" method="post" enctype="multipart/form-data">
                
				<div class="panel-body" >
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Product thumbnail</label>
                  <div class="col-sm-10">
                    <input type="file" name="file[]" value="" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
				 <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Product image</label>
                  <div class="col-sm-10">
                    <input type="file" name="file[]" value="" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>	
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Caption</label>
                  <div class="col-sm-10">
                    <input type="text" name="caption" value="" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>	

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Position</label>
                  <div class="col-sm-10">
                    <select name="position" class="selectpicker">
                        <option value="5">All captions</option>
						<option value="1">Position-1</option>
						<option value="2">Position-2</option>
						<option value="3">Position-3</option>
						<option value="4">Position-4</option>                     
                      </select> 
                  </div>
                </div>
				
				

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="send_image" value="Update" class="btn btn-default"/>
                  </div>
                </div>
                </div>
				
              </form> 

            
			
				
			
      </div>
	  
	  <div class="panel panel-default">

        <div class="panel-title">  Uploaded Image     </div>

            <div class="panel-body table-responsive" style="border: 1px solid blue; border-radius: 5px;">

            <table id="example0" class="table display">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Caption</th>                        
                        <th>Position</th>
                        <th>Cover</th>
                        <th>Action</th>
                    </tr>
                </thead>
             
                <tfoot>
                    <tr>
                        <th>Image</th>
                        <th>Caption</th>                        
                        <th>Position</th>
                        <th>Cover</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
             
                <tbody>
<?php
$img_tb = mysql_query("SELECT * FROM prdct_image") or die("Error in display select query !!!");
while($img_rw = mysql_fetch_assoc($img_tb)):  $get_image = $img_rw['PRDCT_THUMBNAIL'];
?>
                    <tr>
                        <td> <img src="photo_upload/<?php echo $get_image; ?>" style="width: 100px; height: 100px;" /></td>                        
                        <td><?=$img_rw['CAPTION'];?></td>
                        <td><?=$img_rw['POSITION'];?></td>
                        <td><input type="checkbox" value=""/></td>
                        <td>
						<a href="?edit=<?=$img_rw['IMAGE_ID'];?>">Edit</a>
						<a href="?delete=<?=$img_rw['IMAGE_ID'];?>">Delete</a>
						</td>
                    </tr>
<?php endwhile; ?>                   
                </tbody>
            </table>


        </div>
			
      </div>
	  
	  
    </div>

  </div>
  <!-- End Row -->




</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>


<!-- Basic Date Range Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-picker').daterangepicker(null, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Date Range and Time Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-and-time-picker').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'MM/DD/YYYY h:mm A'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

</body>
</html>