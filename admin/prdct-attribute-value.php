<?php include_once('header.php'); ?>

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Add Attribute Value</h1>
      <ol class="breadcrumb">
        <li><a  class="menu-col" href="index.php">Home</a></li>
        <li><a  class="menu-col" href="#">Catalog</a></li>
        <li class="active">Add new attribute value</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  
  <!-- Start Row -->
  <div class="row">
 
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-title"> Attributes value   </div>
<?php
$edit_id = $_GET['edit_id'];
$at_val_tb = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$edit_id'") or die("Error in product attribute select query !!!");
$at_val_rw = mysql_fetch_assoc($at_val_tb);

if(isset($_POST['send_attr_value'])): 
$attr_id = $_POST['attr_name'];
$attr_value = $_POST['attr_value'];
$url_key = $_POST['url_key'];
$meta_title = $_POST['meta_title'];

if(isset($_POST['color']) && $_POST['color'] != 'FFFFFF'):
$color = $_POST['color'];

foreach ($_FILES['file']['name'] as $f => $name) {
 $allowedExts = array("gif", "jpeg", "jpg", "png");
    $temp = explode(".", $name);
    $extension = end($temp);
	$name=uniqid().$name;
if ((($_FILES["file"]["type"][$f] == "image/gif")
|| ($_FILES["file"]["type"][$f] == "image/jpeg")
|| ($_FILES["file"]["type"][$f] == "image/jpg")
|| ($_FILES["file"]["type"][$f] == "image/png"))
&& ($_FILES["file"]["size"][$f] < 2000000)
&& in_array($extension, $allowedExts))
{
  if ($_FILES["file"]["error"][$f] > 0)
  {    echo "Return Code: " . $_FILES["file"]["error"][$f] . "<br>";  }
  else
  {    if (file_exists("attribute_image/" . $name))    {    }
    else
    { move_uploaded_file($_FILES["file"]["tmp_name"][$f], "attribute_image/" . $name); }
  }
}
else
{  $error =  "Invalid file"; }
}
if(isset($_POST['at_val_hid_id']) && $_POST['at_val_hid_id'] != ''):
$at_val_hid_id = $_POST[at_val_hid_id];
mysql_query("UPDATE prdct_attr_value SET ATTR_ID = '$attr_id', ATTR_VAL = '$attr_value', URL_KEY = '$url_key', META_TITLE = '$meta_title', COLOR = '$color', TEXTURE = '$name' WHERE ATTR_VAL_ID = '$at_val_hid_id'") or die("Error in updated 1 !!!");
else:
mysql_query("INSERT INTO prdct_attr_value(ATTR_ID,ATTR_VAL,URL_KEY,META_TITLE,COLOR,TEXTURE) VALUES('$attr_id', '$attr_value', '$url_key', '$meta_title', '$color', '$name')") or die("Error in attr value insert query !!!");

endif;

else:
if(isset($_POST['at_val_hid_id']) && $_POST['at_val_hid_id'] != ''):
$at_val_hid_id = $_POST[at_val_hid_id];
mysql_query("UPDATE prdct_attr_value SET ATTR_ID = '$attr_id', ATTR_VAL = '$attr_value', URL_KEY = '$url_key', META_TITLE = '$meta_title' WHERE ATTR_VAL_ID = '$at_val_hid_id'") or die("Error in updated 1 !!!");
else:
mysql_query("INSERT INTO prdct_attr_value(ATTR_ID,ATTR_VAL,URL_KEY,META_TITLE) VALUES('$attr_id', '$attr_value', '$url_key', '$meta_title')") or die('Error in attr value insert query !!!');
endif; endif; ?>
<script type="text/javascript"> alert("Attribute value updated successfully !!!"); window.location.href="prdct-attribute-list.php"; </script>
<?php endif; ?>
            <div class="panel-body">
              <form class="form-horizontal" method="post" name="attr_value_form" enctype="multipart/form-data">
				
                 <div class="form-group">
                  <label class="col-sm-2 control-label form-label">Attribute group</label>
                  <div class="col-sm-10">
<select required class="selectpicker" name="attr_name" onchange="get_attr(this);">
      <option>-select-</option>
      <?php
      $attr_tb = mysql_query("SELECT * FROM prdct_attr") or die("Error in product attribute select query !!!");
      while($attr_rw = mysql_fetch_assoc($attr_tb)): ?>	  
      <option value="<?=$attr_rw['ATTR_ID']; ?>"<?php if(isset($at_val_rw['ATTR_ID']) && $at_val_rw['ATTR_ID'] == $attr_rw['ATTR_ID']): echo "selected"; endif; ?>><?=$attr_rw['ATTR_NAME']; ?></option>
      <?php endwhile; ?>
</select>   
                  <?php if(isset($at_val_rw['ATTR_VAL_ID'])): ?>
				  <input type="hidden" name="at_val_hid_id" value="<?php echo $at_val_rw['ATTR_VAL_ID']; ?>"/>
				  <?php endif; ?>               
                  </div>
                </div>	
				
					
                <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Value</label>
                  <div class="col-sm-10">
                    <input required type="text" name="attr_value" value="<?php if(isset($at_val_rw['ATTR_VAL'])): echo $at_val_rw['ATTR_VAL']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>
									
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">URL Key</label>
                  <div class="col-sm-10">
                    <input required type="text" name="url_key" value="<?php if(isset($at_val_rw['URL_KEY'])): echo $at_val_rw['URL_KEY']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Meta title</label>
                  <div class="col-sm-10">
                    <input required type="text" name="meta_title" value="<?php if(isset($at_val_rw['META_TITLE'])): echo $at_val_rw['META_TITLE']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>
				
				<div id="show-clr-txtr"> </div>
				
				<!--<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Color</label>
                  <div class="col-sm-10">
                    <input type="text" id="chosen-value" name="color" value="<?php if(isset($at_val_rw['COLOR'])): echo $at_val_rw['COLOR']; endif; ?>	">
					<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}">
		            Pick text color
	                </button>
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Texture</label>
                  <div class="col-sm-10">
                    <input type="file" name="file[]" value="<?php if(isset($at_val_rw['TEXTURE'])): echo $at_val_rw['TEXTURE']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div--->
								
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="send_attr_value" value="Update" class="btn btn-default"/>
                  </div>
                </div>
			

              </form> 

            </div>

      </div>
    </div>

  </div>
  <!-- End Row -->

  <!-- Start Row -->
  <div class="row">
  </div>
  <!-- End Row -->

</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a  class="menu-col" href="http://www.automarce.com"  target="_blank">Automarce</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a class="menu-col"href="http://www.brightlivingstone.com/ecommerce-website-development.php" target="_blank">Ecommerce Website Development</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>

<script src="jscolor.js"></script>
	<script>
	function setTextColor(picker) {
		//document.getElementsByTagName('body')[0].style.color = '#' + picker.toString()
	}
	</script>
<!-- Basic Date Range Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-picker').daterangepicker(null, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Date Range and Time Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-and-time-picker').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'MM/DD/YYYY h:mm A'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<script type="text/javascript">
function get_attr(at){
	var attr = $(at).val(); 
	// alert(attr);
	$.ajax({
		url:"action/attribute-action.php",
		type:"post",
		data:{get_attr:attr},
		success:function(data_recived){	
		var test=$.parseHTML(data_recived);
		$("#show-clr-txtr").append(test);
		var myColor = new jscolor("chosen-value1");
		 // jscolor("","refresh");
		}
		});
}
</script>

</body>
</html>