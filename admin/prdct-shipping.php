<?php include_once('header.php'); ?>

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Shipping</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Catalog</a></li>
        <li class="active">Add Shipping</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  
  <!-- Start Row -->
  <div class="row">
<div class="col-md-2">
 <style> ul {padding-left: 0px;} ul.prdct_menu li {display: block; cursor: pointer; padding: 10px; background: #fff; border-radius: 7px; border: 1px solid #f5f5f5; } </style>
 <ul class="prdct_menu">
 <li> <a href="index.php">Product List </a></li>
 <li> <a href="prdct-add.php">Add Product </a></li>
 <li> <a href="prdct-price.php">Price </a></li>
 <li> <a href="prdct-seo.php">SEO </a></li>
 <li> <a href="prdct-category.php">Category associate </a></li>
 <li> <a href="prdct-shipping.php">Shipping </a></li>
 <li> <a href="prdct-attribute-combination.php">Attribute combination </a></li>
 <li> <a href="prdct-quantity.php">Quantity </a></li>
 <li> <a href="prdct-images.php">Images </a></li>
 </ul>
 </div>
    <div class="col-md-10">
      <div class="panel panel-default">

        <div class="panel-title">
          Shipping
          <ul class="panel-tools">
            <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
            <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
             <form class="form-horizontal">
            <div class="panel-body">             

                <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Package width</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Package height</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>	

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Package depth</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Package weight</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Additional shipping fees for a single item</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
				<div class="form-group">
                  <label class="col-sm-2 control-label form-label">Available carriers</label>
                  <div class="col-sm-10">
                    <select class="selectpicker">
                        <option>Carrier-1</option>
						<option>Carrier-2</option>
						<option>Carrier-3</option>
						<option>Carrier-4</option>                     
                      </select>               
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Selected carriers</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" style="width: 250px;" rows="3"></textarea>
                  </div>
                </div>
				
				

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label"></label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-default">Submit</button>
                  </div>
                </div>
                  </div>
              </form> 

           

      </div>
    </div>

  </div>
  <!-- End Row -->




</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>


<!-- Basic Date Range Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-picker').daterangepicker(null, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Date Range and Time Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-and-time-picker').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'MM/DD/YYYY h:mm A'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

</body>
</html>