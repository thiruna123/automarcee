<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Add Product Attribute</h1>
      <ol class="breadcrumb">
        <li><a class="menu-col"href="index.php">Home</a></li>
        <li><a  class="menu-col" href="#">Catalog</a></li>
        <li class="active">Add new product attribute</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  
  <!-- Start Row -->
  <div class="row">
 
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-title">
         
		  <a href="prdct-attribute-value.php"><button class="new-button">Add New value</button></a>
          
        </div>
<?php
$edit_id = $_GET['edit_id'];
$attr_tb = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$edit_id'") or die("Error in product attribute select query !!!");
$attr_rw = mysql_fetch_assoc($attr_tb);

if(isset($_POST['send_attr'])):
$attr_name = $_POST['attr_name'];
$pblc_name = $_POST['pblc_name'];
$url_key = $_POST['url_key'];
$meta_title = $_POST['meta_title'];
$indexable = $_POST['indexable'];
$attr_type = $_POST['attr_type'];
if(isset($_POST['attr_hid_id']) && $_POST['attr_hid_id'] != ''): 
$attr_hid_id = (int)$_POST['attr_hid_id'];
mysql_query("UPDATE prdct_attr SET ATTR_NAME = '$attr_name', PBLC_NAME = '$pblc_name', URL_KEY = '$url_key', META_TITLE = '$meta_title', INDEXABLE = '$indexable', ATTR_TYPE = '$attr_type' WHERE ATTR_ID = '$attr_hid_id'") or die("Error in product attribute update query !!!");
else:
mysql_query("INSERT INTO prdct_attr(ATTR_NAME,PBLC_NAME,URL_KEY,META_TITLE,INDEXABLE,ATTR_TYPE) VALUES('$attr_name', '$pblc_name', '$url_key', '$meta_title', '$indexable', '$attr_type')") or die('Error in add product attribute insert query !!!');
endif; ?><script type="text/javascript"> alert("Product attribute updated succesfully !!");window.location.href="prdct-attribute-list.php"; </script>
<?php endif; ?>

                 <div class="panel-body">
                <form class="form-horizontal" name="attr_form" method="post">


                <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Name</label>
                  <div class="col-sm-10">
                    <input required type="text" name="attr_name" value="<?php if(isset($attr_rw['ATTR_NAME'])): echo $attr_rw['ATTR_NAME']; endif; ?>" class="form-control" id="input002"/>
                  <?php if(isset($attr_rw['ATTR_ID'])): ?>
				  <input type="hidden" name="attr_hid_id" value="<?php echo $attr_rw['ATTR_ID']; ?>"/>
				  <?php endif; ?> 
				  </div>
                </div>
								
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Public name</label>
                  <div class="col-sm-10">
                    <input required type="text" name="pblc_name" value="<?php if(isset($attr_rw['PBLC_NAME'])): echo $attr_rw['PBLC_NAME']; endif; ?>" class="form-control" id="input002"/>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">URL Key</label>
                  <div class="col-sm-10">
                    <input required type="text" name="url_key" value="<?php if(isset($attr_rw['URL_KEY'])): echo $attr_rw['URL_KEY']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Meta title</label>
                  <div class="col-sm-10">
                    <input required type="text" name="meta_title" value="<?php if(isset($attr_rw['META_TITLE'])): echo $attr_rw['META_TITLE']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">
                  <label  class="col-sm-2 control-label form-label">Indexable</label>
                  <div class="col-sm-10">
                   <span><input type="radio" name="indexable" value="1"<?php if(isset($attr_rw['INDEXABLE'])){ if($attr_rw['INDEXABLE']==1){ echo "checked"; } } else { echo "checked"; }?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Yes</span>  </span><br/>
				 <span><input type="radio" name="indexable" value="2"<?php if(isset($attr_rw['INDEXABLE']) && $attr_rw['INDEXABLE']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">No</span>  </span><br/>
						
                  </div>
                </div>
								
				<div class="form-group">
                  <label class="col-sm-2 control-label form-label">Attribute type</label>
                  <div class="col-sm-10">
<select required name="attr_type" class="selectpicker">
<option>-select-</option>
<option value="<?php if(isset($attr_rw['ATTR_TYPE']) && $attr_rw['ATTR_TYPE']): echo $attr_rw['ATTR_TYPE']; endif; ?>"
			 <?php if(isset($attr_rw['ATTR_TYPE']) && $attr_rw['ATTR_TYPE']): echo "selected"; endif; ?>>
			 <?php if(isset($attr_rw['ATTR_TYPE']) && $attr_rw['ATTR_TYPE']): echo $attr_rw['ATTR_TYPE']; endif; ?>
			 </option>
<option value="Drop-down list">Drop-down list</option>
<option value="Radio button">Radio button</option>
<option value="Color or texture">Color or texture</option>                        
</select>                  
                  </div>
                </div>	

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="send_attr" value="Update" class="btn btn-default"/>                     
				 </div>
                </div>
              </form> 

            </div>

      </div>
    </div>

  </div>
  <!-- End Row -->



  <!-- Start Row -->
  <div class="row">
  </div>
  <!-- End Row -->

</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a class="menu-col" href="http://www.automarce.com"target="_blank">Automarce</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a class="menu-col" href="http://www.brightlivingstone.com/ecommerce-website-development.php"  target="_blank">Ecommerce Website Development</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>

<script src="jscolor.js"></script>
	<script>
	function setTextColor(picker) {
		document.getElementsByTagName('body')[0].style.color = '#' + picker.toString()
	}
	</script>
<!-- Basic Date Range Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-picker').daterangepicker(null, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Date Range and Time Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-and-time-picker').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'MM/DD/YYYY h:mm A'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>



</body>
</html>