<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Currency List</h1>
      <ol class="breadcrumb">
        <li><a class="menu-col" href="index.html">Home</a></li>
        <li><a class="menu-col" href="#">Price rules</a></li>
        <li class="active">Currency list</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <a href="currency.php"><button class="new-button">Add New Currency </button></a>
        </div>
        <div class="panel-body table-responsive">
<?php
$crncy_del = $_GET['crncy_del'];
if(isset($_GET['crncy_del']) && $_GET['crncy_del'] != ''):
$crncy_del = (int)$_GET['crncy_del'];
mysql_query("DELETE FROM ms_currency WHERE CRNCY_ID = '$crncy_del'") or die('Currency delete query error !!!');
?> <script type="text/javascript"> alert("Deleted Succesfully !!!"); window.location.href="currency-list.php"; </script>
<?php endif; ?>
            <table id="example0" class="table display">
                <thead>
                    <tr>                       
                        <th>ID</th>                       
                        <th>Currency Name</th>
                        <th>Value differ from USD</th>
                        <th>Created On</th>
						<th>Action</th>
                    </tr>
                </thead>
             
                <tfoot>
                    <tr>
                        <th>ID</th>                       
                        <th>Currency Name</th>
                        <th>Value differ from USD</th>
                        <th>Created On</th>
						<th>Action</th>
                    </tr>
                </tfoot>
             
                <tbody>
				   
<?php 
$cr_tb = mysql_query("SELECT * FROM ms_currency") or dir('Error in currency listing select query !!!'); 
while($cr_rw = mysql_fetch_assoc($cr_tb)):
?>
                        <tr>
                       	<td><?=$cr_rw['CRNCY_ID']; ?></td>
                        <td><?=$cr_rw['CURRENCY_NAME']; ?></td>
                        <td><?=$cr_rw['CURRENCY_VALUE']; ?></td>
                        <td><?=$cr_rw['CREATED_ON'];?></td> 
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
						<a href="currency.php?edit_id=<?=$cr_rw['CRNCY_ID']; ?>">Edit</a>
						&nbsp;&nbsp;
						<a onClick="currency_delete(<?=$cr_rw['CRNCY_ID'];?>)">Delete</a>
						</td>
                        </tr>
<?php  endwhile; ?>
                  
                   
                </tbody>
            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->






</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a class="menu-col" href="http://www.automarce.com" target="_blank">Automarce</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a class="menu-col" href="http://www.brightlivingstone.com/ecommerce-website-development.php" target="_blank">Ecommerce Website Development</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>

<!---Comformation for deleting the product image--->
<script type="text/javaScript" >
function currency_delete(crncy_del) { //alert(prdct_img_del);
if (confirm("Do you really want to delete this currency?"))
{ 
window.location.href='currency-list.php?crncy_del='+crncy_del; 
}
else
{
window.location.href='currency-list.php';
}
}
</script>
<!---Comformation for deleting the product image--->


</body>
</html>