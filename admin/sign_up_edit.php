<?php 
error_reporting(0);
include_once('config.php');
$edit_id = $_GET['edit_id'];
if(isset($_GET['edit_id']))
{ $edit_id = $_GET['edit_id'];
$edit_tb = mysql_query("SELECT * FROM admin WHERE aid = '$edit_id'") or die("Error in select query !!!!");
$edit_rw = mysql_fetch_assoc($edit_tb);
}

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title>MSAS - Sign Up</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="styles/d6220a84.bootstrap.css">


        <!-- Proton CSS: -->
        <link rel="stylesheet" href="styles/1b2c4b33.proton.css">
        <link rel="stylesheet" href="styles/vendor/animate.css">

        <!-- adds CSS media query support to IE8   -->
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="scripts/vendor/respond.min.js"></script>
        <![endif]-->

        <!-- Fonts CSS: -->
        <link rel="stylesheet" href="styles/9a41946e.font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="styles/4d9a7458.font-titillium.css" type="text/css" />

        <!-- Common Scripts: -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="scripts/vendor/modernizr.js"></script>
        <script src="scripts/vendor/jquery.cookie.js"></script>
    </head>

    <body class="login-page">
        
        <script>
	        var theme = $.cookie('protonTheme') || 'default';
	        $('body').removeClass (function (index, css) {
	            return (css.match (/\btheme-\S+/g) || []).join(' ');
	        });
	        if (theme !== 'default') $('body').addClass(theme);
        </script>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <form role="form" action="login_submit.php" method="post">
            <section class="wrapper scrollable animated fadeInDown">
                <section class="panel panel-default">
                    <div class="panel-heading">
                        <div>
                             <img src="images/logo.png" style="width: 220px; height: 85px;" alt="MSAS-Logo">
                            <h1>
                                <span class="title">
                                    
                                </span>
                                <span class="subtitle">
                                    
                                </span>
                            </h1>
                        </div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                        	
                            <span class="login-text">
                               Edit your details
                            </span>
							<div class="form-group">
                                <label for="name">Name</label>
								<input type="hidden" name="id" value="<?php echo $edit_rw['aid'];  ?>"/>
                                <input type="text" name="first_name" value="<?php echo $edit_rw['admin_name'];  ?>" class="form-control input-lg" id="name" placeholder="First name">
                            </div>
                            <div class="form-group">
                                <label for="surname">Surname</label>
                                <input type="text" name="sur_name" value="<?php echo $edit_rw['admin_surname'];  ?>" class="form-control input-lg" id="surname" placeholder="Surname">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="txtmail" value="<?php echo $edit_rw['admin_email'];  ?>" class="form-control input-lg" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="txtpass" value="<?php echo $edit_rw['admin_pass'];  ?>" class="form-control input-lg" id="password" placeholder="Password">
                            </div>
							<div class="form-group">
                                <label for="password">Confirm Password</label>
                                <input type="password" name="cnfm_pass" value="<?php echo $edit_rw['admin_cnfmpass'];  ?>" class="form-control input-lg" id="password" placeholder="Confirm Password">
                            </div>
							<div class="form-group">
                                <label for="mobile">Mobile No</label>
                                <input type="text" name="mobile" value="<?php echo $edit_rw['admin_mobile']; ?>" class="form-control input-lg" id="mobile" placeholder="Mobile No">
                            </div>
                        </li>
                    </ul>
                    <div class="panel-footer">
                        <input type="submit" class="btn btn-lg btn-success" name="signup_edit" value="UPDATE" />
                    </div>
					
                </section>
            </section>
        </form>
		
    </body>
</html>
