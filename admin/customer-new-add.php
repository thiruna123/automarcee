<?php include_once('header.php'); ?>

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Customer</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Catalog</a></li>
        <li class="active">Creating new customer</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

  <!-- Start Presentation -->
  <!---<div class="row presentation">

    <div class="col-lg-8 col-md-6 titles">
      <span class="icon color9-bg"><i class="fa fa-check-square-o"></i></span>
      <h1>Form Elements</h1>
      <h4>Responsive and user-friendly form elements</h4>
    </div>

    <div class="col-lg-4 col-md-6">
      <ul class="list-unstyled list">
        <li><i class="fa fa-check"></i>Simplicity<li>
        <li><i class="fa fa-check"></i>Based on <a href="http://getbootstrap.com/" target="_blank">Bootstrap</a><li>
        <li><i class="fa fa-check"></i><a href="https://github.com/flatlogic/awesome-bootstrap-checkbox" target="_blank">Awesome Bootstrap Checkbox</a><li>
        <li><i class="fa fa-check"></i><a href="http://www.bootstraptoggle.com/" target="_blank">Bootstrap Toggle</a><li>
        <li><i class="fa fa-check"></i><a href="http://silviomoreto.github.io/bootstrap-select/" target="_blank">Bootstrap Select</a><li>
      </ul>
    </div>

  </div>---->
  <!-- End Presentation -->


 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  
  <!-- Start Row -->
  <div class="row">
    <div class="col-md-12">
	
	<div class="panel panel-default" style="border: 0px solid red;">

        <div class="panel-title">  Add New  </div>
<?php
if(isset($_POST['send_customer'])): 
$gender = $_POST['gender'];
$first_name =$_POST['first_name'];
$last_name = $_POST['last_name'];
$email_id = $_POST['email_id'];
$password = $_POST['password'];
$bod = $_POST['bod'];
$enable = $_POST['enable'];
$newsletter = $_POST['newsletter'];
$optin = $_POST['optin'];
$grp_acss = implode(",",($_POST['grp_acss']));
$dflt_cstmr_grp = $_POST['dflt_cstmr_grp'];
mysql_query("INSERT INTO customers(GENDER,FIRST_NAME,LAST_NAME,EMAIL_ID,PASSWORD,BOD,ENABLE,NEWSLETTER,OPTIN,GRP_ACSS,DFLT_CSTMR_GRP) VALUES('$gender', '$first_name', '$last_name', '$email_id', '$password', '$bod', '$enable', '$newsletter', '$optin', '$grp_acss', '$dflt_cstmr_grp')") or die('Error in customer insert query !!!');
?> <script type="text/javascript"> alert("Customer details updated successfully !!!"); window.location.href="customer-new-add.php"; </script>
<?php endif; ?>
            <div class="panel-body">
              <form class="form-horizontal" name="customer_form" method="post">

              <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Social Title</label>
                  <div class="col-sm-10">
                    <span><input type="radio" name="gender" value="1"<?php if(isset($prdct_rw['AVAIL_STOCK'])){ if($prdct_rw['AVAIL_STOCK']==1){ echo "checked"; } } else { echo "checked"; }?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Mr</span>  </span>&nbsp;&nbsp;&nbsp;&nbsp;
						 <span><input type="radio" name="gender" value="2"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Mrs</span>  </span><br/>
						 
                  </div>
                </div>

                <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">First Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="first_name" value="" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Last Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="last_name" value="" class="form-control" id="input002">
                  </div>
                </div>
				
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Email Address</label>
                  <div class="col-sm-10">
                    <input type="text" name="email_id" value="" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Password</label>
                  <div class="col-sm-10">
                    <input type="text" name="password" value="" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">               
                 <label  class="col-sm-2 control-label form-label">Birthday</label>                    
                       <div class="col-sm-10">                        
                         <input type="text" name="bod" value="" id="date-picker" class="form-control" value="03/18/2015"/>
                       </div>
                </div>
				
				<div class="form-group">
                  <label  class="col-sm-2 control-label form-label">Enabled</label>
                  <div class="col-sm-10">
                     <span><input type="radio" name="enable" value="1"<?php if(isset($prdct_rw['AVAIL_STOCK'])){ if($prdct_rw['AVAIL_STOCK']==1){ echo "checked"; } } else { echo "checked"; }?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Yes</span>  </span>&nbsp;&nbsp;&nbsp;&nbsp;
						 <span><input type="radio" name="enable" value="2"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">No</span>  </span><br/>
						 </div>
                </div>
				
				<div class="form-group">
                  <label  class="col-sm-2 control-label form-label">Newsletter</label>
                  <div class="col-sm-10">
                    <span><input type="radio" name="newsletter" value="1"<?php if(isset($prdct_rw['AVAIL_STOCK'])){ if($prdct_rw['AVAIL_STOCK']==1){ echo "checked"; } } else { echo "checked"; }?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Yes</span>  </span>&nbsp;&nbsp;&nbsp;&nbsp;
						 <span><input type="radio" name="newsletter" value="2"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">No</span>  </span><br/>
					</div>
                </div>
				
				<div class="form-group">
                  <label  class="col-sm-2 control-label form-label">Opt In</label>
                  <div class="col-sm-10">
                   <span><input type="radio" name="optin" value="1"<?php if(isset($prdct_rw['AVAIL_STOCK'])){ if($prdct_rw['AVAIL_STOCK']==1){ echo "checked"; } } else { echo "checked"; }?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Yes</span>  </span>&nbsp;&nbsp;&nbsp;&nbsp;
						 <span><input type="radio" name="optin" value="2"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">No</span>  </span><br/>
					</div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Group Access</label>
                  <div class="col-sm-10">
                   <span><input type="checkbox" name="grp_acss[]" value="1"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Visitor</span>  </span><br/>
				   <span><input type="checkbox" name="grp_acss[]" value="2"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Guest</span>  </span><br/>
				   <span><input type="checkbox" name="grp_acss[]" value="3"<?php if(isset($prdct_rw['AVAIL_STOCK']) && $prdct_rw['AVAIL_STOCK']==2){ echo "checked";} ?>>&nbsp;&nbsp;<span style="position: relative; top: -13px;">Customer</span>  </span><br/>
                  </div>
                </div>
				
				<div class="form-group">
                  <label class="col-sm-2 control-label form-label">Default Customer Group</label>
                  <div class="col-sm-10">
                    <select name="dflt_cstmr_grp" class="selectpicker">
					<option value="Visitor">Visitor</option>
					<option value="Guest">Guest</option>
                    <option value="Customer">Customer</option>                        
                    </select>                  
                  </div>
                </div>	

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="send_customer" value="Update" class="btn btn-default"/>
                  </div>
                </div>

              </form> 

            </div>

      </div>
    </div>

  </div>
  <!-- End Row -->

  <!-- Start Row -->
  <div class="row">
  </div>
  <!-- End Row -->

</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>


<!-- Basic Date Range Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-picker').daterangepicker(null, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Date Range and Time Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-and-time-picker').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'MM/DD/YYYY h:mm A'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

</body>
</html>