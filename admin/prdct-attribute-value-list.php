<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Products Attribute Values</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="#">Catalog</a></li>
        <li class="active">Products attribute values list</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <a href="prdct-attribute.php"><button>Add New Attribute</button></a>
		  <a href="prdct-attribute-value.php"><button>Add New value</button></a>
        </div>
        <div class="panel-body table-responsive">

            <table id="example0" class="table display">
                <thead>
                    <tr>
                        <th>Check</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Value count</th>                        
						<th>Created</th>
					    <th>Action</th>
                    </tr>
                </thead>
             
                <tfoot>
                    <tr>
                        <th>Check</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Value count</th>                        
						<th>Created</th>
					    <th>Action</th>
                    </tr>
                </tfoot>
             
                <tbody>
				   <!--- <tr>
                        <td align="left"></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 300px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left">---</td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="submit" name="" value="Search"/></td>						
                     
                    </tr>--->
<?php
$delete_id = $_GET['delete_id'];
if(isset($_GET['delete_id']) && $_GET['delete_id'] != ''): 
$delete_id = (int)$_GET['delete_id'];
$get_tb = mysql_query("SELECT DISTINCT ATTR_ID FROM prdct_attr_value WHERE ATTR_VAL_ID = '$delete_id'") or die("Error in select query !!!");
$get_rw = mysql_fetch_assoc($get_tb); 
$get_id = $get_rw['ATTR_ID'];
mysql_query("DELETE FROM prdct_attr_cmbntn WHERE FIND_IN_SET('$get_id:$delete_id',ATTR_CMBNTN)") or die("Error in prdct attribute attr combination delete query !!!!");
mysql_query("DELETE FROM prdct_attr_value WHERE ATTR_VAL_ID = '$delete_id'") or die("Error in prdct attribute delete query !!!!"); 
?> <script type="text/javascript"> alert("Selected Product attribute and value deleted successfully !!!"); window.location.href="prdct-attribute-value-list.php"; </script> <?php 
endif;

$preview_id = (int)$_GET['preview_id'];
if(isset($preview_id))
{
$attr_tb = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_ID = '$preview_id'") or die("Error in product attribute value select query !!!");
$i=1;
while($attr_rw = mysql_fetch_assoc($attr_tb)){
?>
<tr>
<td><input type="checkbox" name="" value=""/></td>                       
<td><?php echo $i; ?></td>
                        <td><?php echo $attr_rw['ATTR_VAL']; ?></td>
                        <td><?php  ?></td>
                        <td><?php echo $attr_rw['CREATED_ON']; ?></td>
						<td><select name="prdct_actions" id="change_action_id" class="change_action_class" onchange="prdct_atrrval_action(this);" style="width: 70px; height: 30px;">
						<option>-Select-</option>
						<option value="edit_id,<?php echo $attr_rw['ATTR_VAL_ID']; ?>">Edit</option>
						<option value="delete_id,<?php echo $attr_rw['ATTR_VAL_ID']; ?>">Delete</option>
						</select></td>
                    </tr>
<?php $i++; } 
}
else{	
$attr_tb = mysql_query("SELECT * FROM prdct_attr_value") or die("Error in product attribute value select query !!!");
$i=1;
while($attr_rw = mysql_fetch_assoc($attr_tb)){
?>
<tr>
<td><input type="checkbox" name="" value=""/></td>                       
<td><?php echo $i; ?></td>
                        <td><?php echo $attr_rw['ATTR_VAL']; ?></td>
                        <td><?php  ?></td>
                        <td><?php echo $attr_rw['CREATED_ON']; ?></td>
						<td><select name="prdct_actions" id="change_action_id" class="change_action_class" onchange="prdct_atrrval_action(this);" style="width: 70px; height: 30px;">
						<option>-Select-</option>
						<option value="edit_id,<?php echo $attr_rw['ATTR_VAL_ID']; ?>">Edit</option>
						<option value="delete_id,<?php echo $attr_rw['ATTR_VAL_ID']; ?>">Delete</option>
						</select></td>
                    </tr>
<?php  $i++; } } ?>                   
                </tbody>
            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->






</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 



<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>
<!-- Action for Preview, edit, delete script start --->
<script type="text/javascript"> 
function prdct_atrrval_action(act_id)
{
var act = $(act_id).val();	
var split_action = act.split(','); 
if(split_action[0]=='edit_id') 
{
	window.location.href='prdct-attribute-value.php?edit_id='+split_action[1];
}
if(split_action[0]=='delete_id')
{
	if (confirm("Do your really want to delete this product attribute value ?"))
        {
            window.location.href= 'prdct-attribute-value-list.php?delete_id='+split_action[1]; 
        }
        else
        {
           window.location.href = 'prdct-attribute-value-list.php'; 
        } 
}
}
</script>   
<!-- Action for Preview, edit, delete script end --->

</body>
</html>