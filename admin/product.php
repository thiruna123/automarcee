<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Products List</h1>
      <ol class="breadcrumb">
        <li><a href="product-list.php">Home</a></li>
        <li><a href="#">Catalog</a></li>
        <li class="active">Products list</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="product-list.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">
   <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <a href="prdct-add.php"><button>Add New Product </button></a>
        </div>
        <div class="panel-body table-responsive">

            <table id="example0" class="table display">
                <thead>
<?php 
/*$prdct_tb = mysql_query("SELECT * FROM 
prdct_add t1 LEFT JOIN prdct_price t2 ON t1.PRDCT_ID=t2.PRDCT_ID 
LEFT JOIN prdct_tax t3 ON t1.PRDCT_ID=t3.PRDCT_ID LEFT JOIN prdct_seo t4 ON t1.PRDCT_ID=t4.PRDCT_ID WHERE t1.PRDCT_ID = '$edit_id'") or die('Add product select query error !!!');
*/													  
?>
                    <tr>
                        <th>Check</th>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
						
                        <th>Category</th>
						<th>Base Price</th>
						<th>Final Price</th>
						<th>Quantity</th>
						<th>Status</th>
					    <th>Action</th>
                    </tr>

                </thead>
             
                <tfoot>
                    <tr>
                        <th>Check</th>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                       
                        <th>Category</th>
						<th>Base Price</th>
						<th>Final Price</th>
						<th>Quantity</th>
						<th>Status</th>
					    <th>Action</th>
                    </tr>
                </tfoot>
             
                <tbody>
			    <!---<tr>
                        <td align="left"></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
                        <td align="left"><input style="width: 50px; height: 30px;" type="submit" name="" value="Search"/></td>						
                     
                    </tr>--->
<?php
$prdct_tb = mysql_query("SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t1.ENABLE, t2.PRDCT_THUMBNAIL, t3.RETAIL_PRICE, t3.TOTAL_PRICE, t4.AVAIL_QNTY, t5.CTGRY_NAME FROM prdct_add t1 LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID LEFT JOIN prdct_avail_qnty t4 ON t1.PRDCT_ID = t4.PRDCT_ID LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!"); 
$i=1; while($prdct_rw = mysql_fetch_assoc($prdct_tb)):
?>
                    <tr>
                        <td><input type="checkbox" name="" value=""/></td>
                        <td><?php echo $i; ?></td>
                        <td><img src="<?php echo 'product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>" style="width: 50px; height: 50px; border: 1px solid red; border-radius: 3px;"></td>
                        <td><?php echo $prdct_rw['PRDCT_NAME']; ?></td>
                        <td><?php echo $prdct_rw['CTGRY_NAME']; ?></td>
						<td><?php echo $prdct_rw['RETAIL_PRICE']; ?></td>
                        <td><?php echo $prdct_rw['TOTAL_PRICE']; ?></td>
                        <td><?php echo $prdct_rw['AVAIL_QNTY']; ?></td>
                        <td><?php if($prdct_rw['ENABLE']=='1'){echo 'Yes';}else{echo 'No';} ; ?></td>
                        <td><select name="prdct_actions" id="change_action_id" class="change_action_class" onchange="prdct_action(this);" style="width: 70px; height: 30px;"><option>-select-</option><option value="">Preview</option><option value="edit_id,<?php echo $prdct_rw['PRDCT_ID']; ?>">Edit</option><option value="delete_id,<?php echo $prdct_rw['PRDCT_ID']; ?>">Delete</option></select></td>
                    </tr>
<?php $i++; endwhile; ?>                 
                </tbody>
            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->
  </div>
  <!-- End Row -->






</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>

<!-- Action for Preview, edit, delete script start --->
<script type="text/javascript"> 
function prdct_action(act_id)
{
var act = $(act_id).val();	
var split_action = act.split(','); 
if(split_action[0]=='edit_id') 
{
	window.location.href='prdct-add.php?edit_id='+split_action[1];
}
if(split_action[0]=='delete_id')
{
	if (confirm("Do your really want to delete this product?"))
        {
            window.location.href= 'prdct-add.php?delete_id='+split_action[1]; 
        }
        else
        {
           window.location.href = 'product-list.php'; 
        } 
}
}
</script>   
<!-- Action for Preview, edit, delete script end --->

</body>
</html>