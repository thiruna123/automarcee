<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Dashboard</h1>
     <!--- <ol class="breadcrumb">
        <li class="active">This is a quick overview of some features</li>
    </ol>--->

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->


 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-widget">

  <!-- Start Top Stats -->
  <div class="col-md-12">
 
  <ul class="topstats clearfix">
    <li class="arrow"></li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Our Car</span>
      <h3><?php $prdct_tb = mysql_query(
"SELECT 
t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.PRDCT_ID, t6.SEO_URL FROM prdct_add t1 
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID 
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_NAME = 'Our Cars'
GROUP BY t1.PRDCT_ID ORDER BY t1.PRDCT_ID DESC") or die("Error in product listing selecting query !!!"); 
echo $rowcount=mysql_num_rows($prdct_tb); ?></h3>
      <!--<span class="diff"><b class="color-down"><i class="fa fa-caret-down"></i> 26%</b> from yesterday</span>--->
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Latest Arrival Cars</span>
      <h3><?php $prdct_tb = mysql_query(
"SELECT 
t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.PRDCT_ID, t6.SEO_URL FROM prdct_add t1 
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID 
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_NAME = 'Latest Arrivals'
GROUP BY t1.PRDCT_ID ORDER BY t1.PRDCT_ID DESC") or die("Error in product listing selecting query !!!"); 
echo $rowcount=mysql_num_rows($prdct_tb); ?></h3>
      <!---<span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last week</span>--->
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Discounted Cars</span>
      <h3 class="color-up"><?php $prdct_tb = mysql_query(
"SELECT 
t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.PRDCT_ID, t6.SEO_URL FROM prdct_add t1 
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID 
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_NAME = 'Discounted Cars'
GROUP BY t1.PRDCT_ID ORDER BY t1.PRDCT_ID DESC") or die("Error in product listing selecting query !!!"); 
echo $rowcount=mysql_num_rows($prdct_tb); ?></h3>
      <!---<span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last month</span>--->
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Hatchback Cars</span>
      <h3><?php $cbntn_tb = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn");
while($cbntn_rw  = mysql_fetch_assoc($cbntn_tb)): 
$sprt_atr_vall = explode(':',$cbntn_rw['ATTR_CMBNTN']); 
$atr_val_cntl = count($sprt_atr_vall);
$atr_vall = 0;
while($atr_vall<$atr_val_cntl)
{
      $sprt_atr_vall[$atr_vall];
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);	
	  $atr_vall = $atr_vall+1;
	  $sprt_atr_vall[$atr_vall];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl);

if($atr_rwl['ATTR_NAME'] == 'Body type' && $val_rwl['ATTR_VAL'] == 'Hatchback'):
$one =  $atr_rwl['ATTR_ID']; $two = ':'; $three = $val_rwl['ATTR_VAL_ID'];
$hatchback = $one."$two".$three;
$prdct_tb = mysql_query(
"SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT,
t4.ACT_ID, t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL
FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes' AND FIND_IN_SET('$hatchback', t4.ATTR_CMBNTN) 
AND t5.CTGRY_NAME =  'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!");
$i=1;  
echo $rowcount=mysql_num_rows($prdct_tb); 

 endif;  $atr_vall++;  }  endwhile; 
?></h3>
      <!---<span class="diff"><b class="color-down"><i class="fa fa-caret-down"></i> 26%</b> from yesterday</span>---->
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Sedan Cars</span>
      <h3 class="color-up"><?php $cbntn_tb = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn");
while($cbntn_rw  = mysql_fetch_assoc($cbntn_tb)): 
$sprt_atr_vall = explode(':',$cbntn_rw['ATTR_CMBNTN']); 
$atr_val_cntl = count($sprt_atr_vall);
$atr_vall = 0;
while($atr_vall<$atr_val_cntl)
{
      $sprt_atr_vall[$atr_vall];
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);	
	  $atr_vall = $atr_vall+1;
	  $sprt_atr_vall[$atr_vall];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl);

if($atr_rwl['ATTR_NAME'] == 'Body type' && $val_rwl['ATTR_VAL'] == 'Sedan'):
$one =  $atr_rwl['ATTR_ID']; $two = ':'; $three = $val_rwl['ATTR_VAL_ID'];
$sedan = $one."$two".$three;
$prdct_tb = mysql_query(
"SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE,
t4.ACT_ID, t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL
FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes' AND FIND_IN_SET('$sedan', t4.ATTR_CMBNTN) 
AND t5.CTGRY_NAME =  'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!");
 echo $rowcount=mysql_num_rows($prdct_tb); 
 endif;  $atr_vall++;  }  endwhile; ?></h3>
      <!---<span class="diff"><b class="color-down"><i class="fa fa-caret-down"></i> 26%</b> from yesterday</span>--->
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Jeep </span>
      <h3 class="color-down"><?php $cbntn_tb = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn");
while($cbntn_rw  = mysql_fetch_assoc($cbntn_tb)): 
$sprt_atr_vall = explode(':',$cbntn_rw['ATTR_CMBNTN']); 
$atr_val_cntl = count($sprt_atr_vall);
$atr_vall = 0;
while($atr_vall<$atr_val_cntl)
{
      $sprt_atr_vall[$atr_vall];
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);	
	  $atr_vall = $atr_vall+1;
	  $sprt_atr_vall[$atr_vall];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl);

if($atr_rwl['ATTR_NAME'] == 'Body type' && $val_rwl['ATTR_VAL'] == 'Jeep'):
$one =  $atr_rwl['ATTR_ID']; $two = ':'; $three = $val_rwl['ATTR_VAL_ID'];
$jeep = $one."$two".$three;
$prdct_tb = mysql_query(
"SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE,
t4.ACT_ID, t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL
FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes' AND FIND_IN_SET('$jeep', t4.ATTR_CMBNTN) 
AND t5.CTGRY_NAME =  'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!");
 echo $rowcount=mysql_num_rows($prdct_tb);
 endif;  $atr_vall++;  }  endwhile; ?></h3>
      <!----<span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last week</span>--->
    </li>
	
	<li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-clock-o"></i> Discounted Tires</span>
      <h3 class="color-down"><?php $ctgry_tb = mysql_query("SELECT * FROM category WHERE MAIN_CTGRY = 'Car tyre sale'") or die('Error in category selecting query !!!');
$ctgry_rw = mysql_fetch_assoc($ctgry_tb);

$ctgry_tb1 = mysql_query("SELECT * FROM category WHERE SUB_CTGRY = '$ctgry_rw[CTGRY_ID]'") or die('Error in category selecting query !!!');
$ctgry_rw1 = mysql_fetch_assoc($ctgry_tb1);
if(!empty($ctgry_rw1['CTGRY_ID']))
{ ?> 
<?php 
$prdct_tb_but = mysql_query("SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME FROM prdct_add t1 LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID 
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() 
AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_ID = '$ctgry_rw1[CTGRY_ID]' AND 
t5.CTGRY_NAME = 'Discounted tires'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!"); 
echo $rw_cnt = mysql_num_rows($prdct_tb_but); }  ?></h3>
      <!---<span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last week</span>---->
    </li>
	
	<li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-clock-o"></i> New Tires</span>
      <h3 class="color-down"><?php 
$ctgry_tb = mysql_query("SELECT * FROM category WHERE MAIN_CTGRY = 'Car tyre sale'") or die('Error in category selecting query !!!');
$ctgry_rw = mysql_fetch_assoc($ctgry_tb);

$ctgry_tb1 = mysql_query("SELECT * FROM category WHERE SUB_CTGRY = '$ctgry_rw[CTGRY_ID]'") or die('Error in category selecting query !!!');
$ctgry_rw1 = mysql_fetch_assoc($ctgry_tb1);
if(!empty($ctgry_rw1['CTGRY_ID']))
{ ?> 
<?php 
$prdct_tb_but = mysql_query("SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME FROM prdct_add t1 LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID 
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() 
AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_ID = '$ctgry_rw1[CTGRY_ID]' AND 
t5.CTGRY_NAME = 'New tires'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!"); 
echo $rw_cnt = mysql_num_rows($prdct_tb_but); }  ?></h3>
      <!---<span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last week</span>--->
    </li>
	
	<li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-clock-o"></i> Used Tires</span>
      <h3 class="color-down"><?php $ctgry_tb = mysql_query("SELECT * FROM category WHERE MAIN_CTGRY = 'Car tyre sale'") or die('Error in category selecting query !!!');
$ctgry_rw = mysql_fetch_assoc($ctgry_tb);

$ctgry_tb1 = mysql_query("SELECT * FROM category WHERE SUB_CTGRY = '$ctgry_rw[CTGRY_ID]'") or die('Error in category selecting query !!!');
$ctgry_rw1 = mysql_fetch_assoc($ctgry_tb1);
if(!empty($ctgry_rw1['CTGRY_ID']))
{ ?> 
<?php 
$prdct_tb_but = mysql_query("SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME FROM prdct_add t1 LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID 
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() 
AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_ID = '$ctgry_rw1[CTGRY_ID]' AND 
t5.CTGRY_NAME = 'Used tires'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!"); 
echo $rw_cnt = mysql_num_rows($prdct_tb_but); }  ?></h3>
      <!---<span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last week</span>--->
    </li>
	
  </ul>
  </div>
  <!-- End Top Stats -->


  <!-- Start Fifth Row -->
  <div class="row">


    <!-- Start Project Stats -->
    <div class="col-md-12">
      <div class="panel panel-widget">
        <div class="panel-title">
          Recent Customers <span class="label label-info"></span>
          <ul class="panel-tools">
            <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
            <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>

        <div class="panel-search">
          <form>
            <input type="text" class="form-control" placeholder="Search...">
            <i class="fa fa-search icon"></i>
          </form>
        </div>


        <div class="panel-body table-responsive">

          <table class="table table-hover">
            <thead>
              <tr>
                <td>ID</td>
				<td>TITLE</td>
                <td>Name</td>
                <td>EMAIL</td>
                <td>CITY</td>
				<td>STATE</td>
				<td>COUNTRY</td>
				<td>PHONE</td>
              </tr>
            </thead>
            <tbody>
			<?php 
$cstmr_tb = mysql_query("SELECT * FROM ms_customer ORDER BY CSTMR_ID ASC LIMIT 5");
$i = 1;
while($cstmr_rw = mysql_fetch_assoc($cstmr_tb)){			?>
              <tr>
                <td><?php echo $i; ?></td>
				<td><?php echo $cstmr_rw['TITLE']; ?></td>
                <td><?php echo $cstmr_rw['FIRST_NAME'].'-'.$cstmr_rw['LAST_NAME']; ?></td>
                <td><?php echo $cstmr_rw['EMAIL']; ?></td>
                <td><?php echo $cstmr_rw['CITY']; ?></td>
				<td><?php echo $cstmr_rw['STATE']; ?></td>
				<td><?php echo $cstmr_rw['COUNTRY']; ?></td>
				<td><?php echo $cstmr_rw['PHONE']; ?></td>
              </tr>
			  <?php $i++; } ?>
            </tbody>
          </table>

        </div>
      </div>
    </div>
    <!-- Start Project Stats -->


  </div>
  <!-- End Fifth Row -->




</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START SIDEPANEL -->
<div role="tabpanel" class="sidepanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">TODAY</a></li>
    <li role="presentation"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">TASKS</a></li>
    <li role="presentation"><a href="#chat" aria-controls="chat" role="tab" data-toggle="tab">CHAT</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">

    <!-- Start Today -->
    <div role="tabpanel" class="tab-pane active" id="today">

      <div class="sidepanel-m-title">
        Today
        <span class="left-icon"><a href="#"><i class="fa fa-refresh"></i></a></span>
        <span class="right-icon"><a href="#"><i class="fa fa-file-o"></i></a></span>
      </div>

      <div class="gn-title">NEW</div>

      <ul class="list-w-title">
        <li>
          <a href="#">
            <span class="label label-danger">ORDER</span>
            <span class="date">9 hours ago</span>
            <h4>New Jacket 2.0</h4>
            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
          </a>
        </li>
        <li>
          <a href="#">
            <span class="label label-success">COMMENT</span>
            <span class="date">14 hours ago</span>
            <h4>Bill Jackson</h4>
            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
          </a>
        </li>
        <li>
          <a href="#">
            <span class="label label-info">MEETING</span>
            <span class="date">at 2:30 PM</span>
            <h4>Developer Team</h4>
            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
          </a>
        </li>
        <li>
          <a href="#">
            <span class="label label-warning">EVENT</span>
            <span class="date">3 days left</span>
            <h4>Birthday Party</h4>
            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
          </a>
        </li>
      </ul>

    </div>
    <!-- End Today -->

    <!-- Start Tasks -->
    <div role="tabpanel" class="tab-pane" id="tasks">

      <div class="sidepanel-m-title">
        To-do List
        <span class="left-icon"><a href="#"><i class="fa fa-pencil"></i></a></span>
        <span class="right-icon"><a href="#"><i class="fa fa-trash"></i></a></span>
      </div>

      <div class="gn-title">TODAY</div>

      <ul class="todo-list">
        <li class="checkbox checkbox-primary">
          <input id="checkboxside1" type="checkbox"><label for="checkboxside1">Add new products</label>
        </li>
        
        <li class="checkbox checkbox-primary">
          <input id="checkboxside2" type="checkbox"><label for="checkboxside2"><b>May 12, 6:30 pm</b> Meeting with Team</label>
        </li>
        
        <li class="checkbox checkbox-warning">
          <input id="checkboxside3" type="checkbox"><label for="checkboxside3">Design Facebook page</label>
        </li>
        
        <li class="checkbox checkbox-info">
          <input id="checkboxside4" type="checkbox"><label for="checkboxside4">Send Invoice to customers</label>
        </li>
        
        <li class="checkbox checkbox-danger">
          <input id="checkboxside5" type="checkbox"><label for="checkboxside5">Meeting with developer team</label>
        </li>
      </ul>

      <div class="gn-title">TOMORROW</div>
      <ul class="todo-list">
        <li class="checkbox checkbox-warning">
          <input id="checkboxside6" type="checkbox"><label for="checkboxside6">Redesign our company blog</label>
        </li>
        
        <li class="checkbox checkbox-success">
          <input id="checkboxside7" type="checkbox"><label for="checkboxside7">Finish client work</label>
        </li>
        
        <li class="checkbox checkbox-info">
          <input id="checkboxside8" type="checkbox"><label for="checkboxside8">Call Johnny from Developer Team</label>
        </li>

      </ul>
    </div>    
    <!-- End Tasks -->

    <!-- Start Chat -->
    <div role="tabpanel" class="tab-pane" id="chat">

      <div class="sidepanel-m-title">
        Friend List
        <span class="left-icon"><a href="#"><i class="fa fa-pencil"></i></a></span>
        <span class="right-icon"><a href="#"><i class="fa fa-trash"></i></a></span>
      </div>

      <div class="gn-title">ONLINE MEMBERS (3)</div>
      <ul class="group">
        <li class="member"><a href="#"><img src="img/profileimg.png" alt="img"><b>Allice Mingham</b>Los Angeles</a><span class="status online"></span></li>
        <li class="member"><a href="#"><img src="img/profileimg2.png" alt="img"><b>James Throwing</b>Las Vegas</a><span class="status busy"></span></li>
        <li class="member"><a href="#"><img src="img/profileimg3.png" alt="img"><b>Fred Stonefield</b>New York</a><span class="status away"></span></li>
        <li class="member"><a href="#"><img src="img/profileimg4.png" alt="img"><b>Chris M. Johnson</b>California</a><span class="status online"></span></li>
      </ul>

      <div class="gn-title">OFFLINE MEMBERS (8)</div>
     <ul class="group">
        <li class="member"><a href="#"><img src="img/profileimg5.png" alt="img"><b>Allice Mingham</b>Los Angeles</a><span class="status offline"></span></li>
        <li class="member"><a href="#"><img src="img/profileimg6.png" alt="img"><b>James Throwing</b>Las Vegas</a><span class="status offline"></span></li>
      </ul>

      <form class="search">
        <input type="text" class="form-control" placeholder="Search a Friend...">
      </form>
    </div>
    <!-- End Chat -->

  </div>

</div>
<!-- END SIDEPANEL -->
<!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="js/summernote/summernote.min.js"></script>

<!-- ================================================
Flot Chart
================================================ -->
<!-- main file -->
<script type="text/javascript" src="js/flot-chart/flot-chart.js"></script>
<!-- time.js -->
<script type="text/javascript" src="js/flot-chart/flot-chart-time.js"></script>
<!-- stack.js -->
<script type="text/javascript" src="js/flot-chart/flot-chart-stack.js"></script>
<!-- pie.js -->
<script type="text/javascript" src="js/flot-chart/flot-chart-pie.js"></script>
<!-- demo codes -->
<script type="text/javascript" src="js/flot-chart/flot-chart-plugin.js"></script>

<!-- ================================================
Chartist
================================================ -->
<!-- main file -->
<script type="text/javascript" src="js/chartist/chartist.js"></script>
<!-- demo codes -->
<script type="text/javascript" src="js/chartist/chartist-plugin.js"></script>

<!-- ================================================
Easy Pie Chart
================================================ -->
<!-- main file -->
<script type="text/javascript" src="js/easypiechart/easypiechart.js"></script>
<!-- demo codes -->
<script type="text/javascript" src="js/easypiechart/easypiechart-plugin.js"></script>

<!-- ================================================
Sparkline
================================================ -->
<!-- main file -->
<script type="text/javascript" src="js/sparkline/sparkline.js"></script>
<!-- demo codes -->
<script type="text/javascript" src="js/sparkline/sparkline-plugin.js"></script>

<!-- ================================================
Rickshaw
================================================ -->
<!-- d3 -->
<script src="js/rickshaw/d3.v3.js"></script>
<!-- main file -->
<script src="js/rickshaw/rickshaw.js"></script>
<!-- demo codes -->
<script src="js/rickshaw/rickshaw-plugin.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>

<!-- ================================================
Sweet Alert
================================================ -->
<script src="js/sweet-alert/sweet-alert.min.js"></script>

<!-- ================================================
Kode Alert
================================================ -->
<script src="js/kode-alert/main.js"></script>

<!-- ================================================
Gmaps
================================================ -->
<!-- google maps api -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<!-- main file -->
<script src="js/gmaps/gmaps.js"></script>
<!-- demo codes -->
<script src="js/gmaps/gmaps-plugin.js"></script>

<!-- ================================================
jQuery UI
================================================ -->
<script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Full Calendar
================================================ -->
<script type="text/javascript" src="js/full-calendar/fullcalendar.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>

<!-- ================================================
Below codes are only for index widgets
================================================ -->
<!-- Today Sales -->
<script>

// set up our data series with 50 random data points

var seriesData = [ [], [], [] ];
var random = new Rickshaw.Fixtures.RandomData(20);

for (var i = 0; i < 110; i++) {
  random.addData(seriesData);
}

// instantiate our graph!

var graph = new Rickshaw.Graph( {
  element: document.getElementById("todaysales"),
  renderer: 'bar',
  series: [
    {
      color: "#33577B",
      data: seriesData[0],
      name: 'Photodune'
    }, {
      color: "#77BBFF",
      data: seriesData[1],
      name: 'Themeforest'
    }, {
      color: "#C1E0FF",
      data: seriesData[2],
      name: 'Codecanyon'
    }
  ]
} );

graph.render();

var hoverDetail = new Rickshaw.Graph.HoverDetail( {
  graph: graph,
  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
    return content;
  }
} );

</script>

<!-- Today Activity -->
<script>
// set up our data series with 50 random data points

var seriesData = [ [], [], [] ];
var random = new Rickshaw.Fixtures.RandomData(20);

for (var i = 0; i < 50; i++) {
  random.addData(seriesData);
}

// instantiate our graph!

var graph = new Rickshaw.Graph( {
  element: document.getElementById("todayactivity"),
  renderer: 'area',
  series: [
    {
      color: "#9A80B9",
      data: seriesData[0],
      name: 'London'
    }, {
      color: "#CDC0DC",
      data: seriesData[1],
      name: 'Tokyo'
    }
  ]
} );

graph.render();

var hoverDetail = new Rickshaw.Graph.HoverDetail( {
  graph: graph,
  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
    return content;
  }
} );
</script>



</body>
</html>