<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Customers</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="#">Catalog</a></li>
        <li class="active">Manage Your Customers</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <a href="customer-new-add.php"><button>Add New customer</button></a>		  
        </div>
        <div class="panel-body table-responsive">

            <table id="example0" class="table display">
                <thead>
                    <tr>
                        <th width="20"></th>
						<th style="font-size: 9px; font: bold;">Cstmr ID</th>
                        <th style="font-size: 9px; font: bold;">Gender</th>
                        <th style="font-size: 9px; font: bold;">First Name</th>
                        <th style="font-size: 9px; font: bold;">Last Name</th>                        
						<th style="font-size: 9px; font: bold;">Email Address</th>
					    <th style="font-size: 9px; font: bold;">Sales</th>
						<th style="font-size: 9px; font: bold;">Enabled</th>
                        <th style="font-size: 9px; font: bold;">Newsletter</th>
                        <th style="font-size: 9px; font: bold;">Opt-In</th>
                        <th style="font-size: 9px; font: bold;">Registration</th>                    
					    
                    </tr>
                </thead>          
                
             
                <tbody>
<?php 
$cstmr_tb = mysql_query("SELECT * FROM customers") or die('Error in customer listing select query !!!');
while($cstmr_rw = mysql_fetch_assoc($cstmr_tb)):
?>
                    <tr>
                        <td><input type="checkbox" name="" value=""/></td>                       
                        <td><?=$cstmr_rw['CTSMR_ID'];?></td>
                        <td><?php if($cstmr_rw['GENDER']=='1'){ echo "Male";}else{ echo "Female";}?></td>
                        <td><?=$cstmr_rw['FIRST_NAME'];?></td>
                        <td><?=$cstmr_rw['LAST_NAME'];?></td>
						<td><?=$cstmr_rw['EMAIL_ID'];?></td>
						<td>--</td>
						<td><input type="checkbox" value=""/></td>
						<td><input type="checkbox" value=""/></td>
						<td><input type="checkbox" value=""/></td>
						<td><?=$cstmr_rw['CREATED_ON'];?></td>	
						
                    </tr>                    
                 <?php endwhile; ?>  
                </tbody>
            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->






</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
 

<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>


</body>
</html>