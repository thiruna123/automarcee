<?php include_once('header.php');?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Price rules</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Price rules</a></li>
        <li class="active">Add catalog price rules</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.php" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">

 <!-- Start Row -->
  <div class="row">
  <!--------------------------------->
<!=== CATALOG PRICE RULES START ===>
<!--------------------------------->
    <div class="col-md-12">	
      <div class="panel panel-default" style="border: 1px solid red;">
	  <div class="panel-title"> CATALOG PRICE RULES </div>
	  <div class="panel-body">             

			 <form class="form-horizontal" name="cartrules_form" method="post" enctype="multipart/form-data"> 
               
			   <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Name</label>
                  <div class="col-sm-10">
                    <input style="width: 340px;" type="text" name="name" value="<?php if(isset($emp_rw['NAME'])): echo $emp_rw['FIRST_NAME']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>

                <div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Currency</label>
                  <div class="col-sm-10">
                    <select style="width: 340px;" name="currency" class="form-control"><option value="All currencies">All currencies</option><option value="Indian rupees">Indian rupees</option></select>
                  </div>
                </div>				
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Country</label>
                  <div class="col-sm-10">
                    <select id="country-options" name="country-options">
<option value="AF">Afghanistan</option>
<option value="AX">Åland Islands</option>
<option value="AL">Albania</option>
<option value="DZ">Algeria</option>
<option value="AS">American Samoa</option>
<option value="AD">Andorra</option>
<option value="AO">Angola</option>
<option value="AI">Anguilla</option>
<option value="AQ">Antarctica</option>
<option value="AG">Antigua and Barbuda</option>
<option value="AR">Argentina</option>
<option value="AM">Armenia</option>
<option value="AW">Aruba</option>
<option value="AU">Australia</option>
<option value="AT">Austria</option>
<option value="AZ">Azerbaijan</option>
<option value="BS">Bahamas</option>
<option value="BH">Bahrain</option>
<option value="BD">Bangladesh</option>
<option value="BB">Barbados</option>
<option value="BY">Belarus</option>
<option value="BE">Belgium</option>
<option value="BZ">Belize</option>
<option value="BJ">Benin</option>
<option value="BM">Bermuda</option>
<option value="BT">Bhutan</option>
<option value="BO">Bolivia</option>
<option value="BA">Bosnia and Herzegovina</option>
<option value="BW">Botswana</option>
<option value="BV">Bouvet Island</option>
<option value="BR">Brazil</option>
<option value="IO">British Indian Ocean Territory</option>
<option value="BN">Brunei Darussalam</option>
<option value="BG">Bulgaria</option>
<option value="BF">Burkina Faso</option>
<option value="BI">Burundi</option>
<option value="KH">Cambodia</option>
<option value="CM">Cameroon</option>
<option value="CA">Canada</option>
<option value="CV">Cape Verde</option>
<option value="KY">Cayman Islands</option>
<option value="CF">Central African Republic</option>
<option value="TD">Chad</option>
<option value="CL">Chile</option>
<option value="CN">China</option>
<option value="CX">Christmas Island</option>
<option value="CC">Cocos (Keeling) Islands</option>
<option value="CO">Colombia</option>
<option value="KM">Comoros</option>
<option value="CG">Congo</option>
<option value="CD">Democratic Republic of the Congo</option>
<option value="CK">Cook Islands</option>
<option value="CR">Costa Rica</option>
<option value="CI">Côte D'Ivoire</option>
<option value="HR">Croatia</option>
<option value="CU">Cuba</option>
<option value="CY">Cyprus</option>
<option value="CZ">Czech Republic</option>
<option value="DK">Denmark</option>
<option value="DJ">Djibouti</option>
<option value="DM">Dominica</option>
<option value="DO">Dominican Republic</option>
<option value="EC">Ecuador</option>
<option value="EG">Egypt</option>
<option value="SV">El Salvador</option>
<option value="GQ">Equatorial Guinea</option>
<option value="ER">Eritrea</option>
<option value="EE">Estonia</option>
<option value="ET">Ethiopia</option>
<option value="FK">Falkland Islands (Malvinas)</option>
<option value="FO">Faroe Islands</option>
<option value="FJ">Fiji</option>
<option value="FI">Finland</option>
<option value="FR">France</option>
<option value="GF">French Guiana</option>
<option value="PF">French Polynesia</option>
<option value="TF">French Southern Territories</option>
<option value="GA">Gabon</option>
<option value="GM">Gambia</option>
<option value="GE">Georgia</option>
<option value="DE">Germany</option>
<option value="GH">Ghana</option>
<option value="GI">Gibraltar</option>
<option value="GR">Greece</option>
<option value="GL">Greenland</option>
<option value="GD">Grenada</option>
<option value="GP">Guadeloupe</option>
<option value="GU">Guam</option>
<option value="GT">Guatemala</option>
<option value="GG">Guernsey</option>
<option value="GN">Guinea</option>
<option value="GW">Guinea-Bissau</option>
<option value="GY">Guyana</option>
<option value="HT">Haiti</option>
<option value="HM">Heard Island and McDonald Islands</option>
<option value="VA">Holy See (Vatican City State)</option>
<option value="HN">Honduras</option>
<option value="HK">Hong Kong</option>
<option value="HU">Hungary</option>
<option value="IS">Iceland</option>
<option value="IN">India</option>
<option value="ID">Indonesia</option>
<option value="IR">Iran</option>
<option value="IQ">Iraq</option>
<option value="IE">Ireland</option>
<option value="IM">Isle of Man</option>
<option value="IL">Israel</option>
<option value="IT">Italy</option>
<option value="JM">Jamaica</option>
<option value="JP">Japan</option>
<option value="JE">Jersey</option>
<option value="JO">Jordan</option>
<option value="KZ">Kazakhstan</option>
<option value="KE">Kenya</option>
<option value="KI">Kiribati</option>
<option value="KP">North Korea</option>
<option value="KR">South Korea</option>
<option value="KW">Kuwait</option>
<option value="KG">Kyrgyzstan</option>
<option value="LA">Laos</option>
<option value="LV">Latvia</option>
<option value="LB">Lebanon</option>
<option value="LS">Lesotho</option>
<option value="LR">Liberia</option>
<option value="LY">Libya</option>
<option value="LI">Liechtenstein</option>
<option value="LT">Lithuania</option>
<option value="LU">Luxembourg</option>
<option value="MO">Macao</option>
<option value="MK">Macedonia</option>
<option value="MG">Madagascar</option>
<option value="MW">Malawi</option>
<option value="MY">Malaysia</option>
<option value="MV">Maldives</option>
<option value="ML">Mali</option>
<option value="MT">Malta</option>
<option value="MH">Marshall Islands</option>
<option value="MQ">Martinique</option>
<option value="MR">Mauritania</option>
<option value="MU">Mauritius</option>
<option value="YT">Mayotte</option>
<option value="MX">Mexico</option>
<option value="FM">Micronesia</option>
<option value="MD">Moldova</option>
<option value="MC">Monaco</option>
<option value="MN">Mongolia</option>
<option value="ME">Montenegro</option>
<option value="MS">Montserrat</option>
<option value="MA">Morocco</option>
<option value="MZ">Mozambique</option>
<option value="MM">Myanmar</option>
<option value="NA">Namibia</option>
<option value="NR">Nauru</option>
<option value="NP">Nepal</option>
<option value="NL">Netherlands</option>
<option value="AN">Netherlands Antilles</option>
<option value="NC">New Caledonia</option>
<option value="NZ">New Zealand</option>
<option value="NI">Nicaragua</option>
<option value="NE">Niger</option>
<option value="NG">Nigeria</option>
<option value="NU">Niue</option>
<option value="NF">Norfolk Island</option>
<option value="MP">Northern Mariana Islands</option>
<option value="NO">Norway</option>
<option value="OM">Oman</option>
<option value="PK">Pakistan</option>
<option value="PW">Palau</option>
<option value="PS">Palestinian Territory</option>
<option value="PA">Panama</option>
<option value="PG">Papua New Guinea</option>
<option value="PY">Paraguay</option>
<option value="PE">Peru</option>
<option value="PH">Philippines</option>
<option value="PN">Pitcairn</option>
<option value="PL">Poland</option>
<option value="PT">Portugal</option>
<option value="PR">Puerto Rico</option>
<option value="QA">Qatar</option>
<option value="RE">Réunion</option>
<option value="RO">Romania</option>
<option value="RU">Russia</option>
<option value="RW">Rwanda</option>
<option value="BL">Saint Barthélemy</option>
<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
<option value="KN">Saint Kitts and Nevis</option>
<option value="LC">Saint Lucia</option>
<option value="MF">Saint Martin</option>
<option value="PM">Saint Pierre and Miquelon</option>
<option value="VC">Saint Vincent and the Grenadines</option>
<option value="WS">Samoa</option>
<option value="SM">San Marino</option>
<option value="ST">Sao Tome and Principe</option>
<option value="SA">Saudi Arabia</option>
<option value="SN">Senegal</option>
<option value="RS">Serbia</option>
<option value="SC">Seychelles</option>
<option value="SL">Sierra Leone</option>
<option value="SG">Singapore</option>
<option value="SK">Slovakia</option>
<option value="SI">Slovenia</option>
<option value="SB">Solomon Islands</option>
<option value="SO">Somalia</option>
<option value="ZA">South Africa</option>
<option value="GS">South Georgia and the South Sandwich Islands</option>
<option value="ES">Spain</option>
<option value="LK">Sri Lanka</option>
<option value="SD">Sudan</option>
<option value="SR">Suriname</option>
<option value="SJ">Svalbard and Jan Mayen</option>
<option value="SZ">Swaziland</option>
<option value="SE">Sweden</option>
<option value="CH">Switzerland</option>
<option value="SY">Syria</option>
<option value="TW">Taiwan</option>
<option value="TJ">Tajikistan</option>
<option value="TZ">Tanzania</option>
<option value="TH">Thailand</option>
<option value="TL">East Timor</option>
<option value="TG">Togo</option>
<option value="TK">Tokelau</option>
<option value="TO">Tonga</option>
<option value="TT">Trinidad and Tobago</option>
<option value="TN">Tunisia</option>
<option value="TR">Turkey</option>
<option value="TM">Turkmenistan</option>
<option value="TC">Turks and Caicos Islands</option>
<option value="TV">Tuvalu</option>
<option value="UG">Uganda</option>
<option value="UA">Ukraine</option>
<option value="AE">United Arab Emirates</option>
<option value="GB">United Kingdom</option>
<option value="US" selected="selected">United States</option>
<option value="UY">Uruguay</option>
<option value="UZ">Uzbekistan</option>
<option value="VU">Vanuatu</option>
<option value="VE">Venezuela</option>
<option value="VN">Viet Nam</option>
<option value="VG">Virgin Islands, British</option>
<option value="VI">Virgin Islands, U.S.</option>
<option value="WF">Wallis and Futuna</option>
<option value="EH">Western Sahara</option>
<option value="YE">Yemen</option>
<option value="ZM">Zambia</option>
<option value="ZW">Zimbabwe</option>
</select>
                </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Groups</label>
                  <div class="col-sm-10">
                    <select style="width: 340px;" name="currency" class="form-control"><option value="All groups">All groups</option><option value="Visitor">Visitor</option><option value="Guest">Guest</option><option value="Customer">Customer</option></select>
                  </div>
                </div>	
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">From quantity</label>
                  <div class="col-sm-10">
                    <input style="width: 340px;" type="text" name="name" value="<?php if(isset($emp_rw['NAME'])): echo $emp_rw['FIRST_NAME']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Price (tax excl.)</label>
                  <div class="col-sm-10">
                    <input style="width: 340px;" type="text" name="name" value="<?php if(isset($emp_rw['NAME'])): echo $emp_rw['FIRST_NAME']; endif; ?>" class="form-control" id="input002">
                <label><input type="checkbox" name="Leave_base_price" value="1"/>&nbsp;&nbsp;&nbsp;&nbsp;<span style="position: relative; top: -13px;">Leave base price</span></label> 
				 </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Valid</label>
                  <div class="col-sm-10">
                   <label> <input style="width: 200px;" autocomplete="off" placeholder="From..." type="text" name="from" id="date-picker"  value="<?php if(isset($prdct_rw['DISPLAY_DATE'])): echo $prdct_rw['DISPLAY_DATE']; endif; ?>"/>
					 &nbsp;&nbsp;&nbsp;&nbsp;
					 <input style="width: 200px;" autocomplete="off" placeholder="To..." type="text" name="to" id="date-picker1" value="<?php if(isset($prdct_rw['DISPLAY_DATE'])): echo $prdct_rw['DISPLAY_DATE']; endif; ?>"/>
                  </label>
				  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Reduction type</label>
                  <div class="col-sm-10">
                    <select style="width: 340px;" name="currency" class="form-control"><option value="Amount">Amount</option><option value="Percentage">Percentage</option></select>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Reduction with or without taxes</label>
                  <div class="col-sm-10">
                    <select style="width: 340px;" name="currency" class="form-control"><option value="Tax exclude">Tax exclude</option><option value="Tax include">Tax include</option></select>
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Reduction</label>
                  <div class="col-sm-10">
                    <input style="width: 340px;" type="text" name="name" value="<?php if(isset($emp_rw['NAME'])): echo $emp_rw['FIRST_NAME']; endif; ?>" class="form-control" id="input002">
                  </div>
                </div>

				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="send_employees" value="Update" class="btn btn-default"/>
                  
				  </div>
                </div>

              </form> 

            </div>
			</div>
            </div>
<!--------------------------------->
<!==== CATALOG PRICE RULES END ====>
<!--------------------------------->

<!--------------------------------->
<!====  CONDITION GROUP START =====>
<!--------------------------------->
<div id="cont_grp"></div>
<!---<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-title">CONDITION GROUP</div>
<div class="panel-body table-responsive">
<table id="example0" class="table display">
<thead>
<tr><th>Type</th><th>Value</th><th>Action</th></tr>
<tfoot>
<tr><th>Type</th><th>Value</th><th>Action</th></tr>
</tfoot>
<tbody>
<tr><td><input id="id_type" style="border: none; box-shadow: inset 0px 0px 0px #F1F0F1;" type="text" name="type" value="" disabled/></td><td><input id="id_value" style="border: none; box-shadow: inset 0px 0px 0px #F1F0F1;" type="text" name="value" value="" disabled/></td><td><input type="button" value="Delete"/></td></tr>   
</tbody></table></div></div></div>--->

<!--------------------------------->
<!--=====  CONDITION GROUP END ==-->
<!--------------------------------->

<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-title">
<a><input onclick="show_condition_grp();" type="button" value="Add a new condition group"/></a>
</div>
</div></div>



<!----------------------->
<!--== CONDITION START ===>
<!----------------------->

<div class="col-md-12" id="id_condition" style="display: none;">	
<div class="panel panel-default" style="border: 1px solid red;">
<div class="panel-title"> CONDITION </div>
<div class="panel-body">             
<div class="form-group">
<div class="col-sm-10">
<table border="0">
<tr><td width="150" height="50" align="left"><strong>Category&nbsp;&nbsp;:</strong></td>
<td colspan="2">
<select style="width: 550px;" id="id_ctgry" name="category" class="form-control">
<?php 
$ctgry_tb = mysql_query("SELECT * FROM category") or die("Error in category select query !!!");
while($ctgry_rw = mysql_fetch_assoc($ctgry_tb)){
?>
<option value="<?=$ctgry_rw['MAIN_CTGRY'];?>"><?=$ctgry_rw['MAIN_CTGRY'];?></option>
<?php } ?>
</select></td>
<td width="140" align="right"><input onclick="category();" type="button" value="Add condition"/></td>
</tr>
				   
<tr><td width="150" height="50" align="left"><strong>Attributes&nbsp;&nbsp;:</strong></td>
<td><select onchange="attr_change();" style="width: 250px;" id="id_attribute" name="attribute" class="form-control">
<option>-Select-</option>
<?php 
$attr_tb = mysql_query("SELECT * FROM prdct_attr"); 
while($attr_rw = mysql_fetch_assoc($attr_tb)){
?>
<option value="<?=$attr_rw['ATTR_ID'];?>"><?=$attr_rw['ATTR_NAME'];?></option>
<?php } ?>
</select></td>
<td><div id="id_atval"></div></td>
<td width="140" align="right"><input type="button" value="Add condition"/></td>
</tr>
				   
<tr><td width="150" height="50" align="left"><strong>Features&nbsp;&nbsp;:</strong></td>
<td><select style="width: 250px;" name="currency" class="form-control"><option value="Tax exclude">Tax exclude</option><option value="Tax include">Tax include</option></select></td>
<td><select style="width: 250px;" name="currency" class="form-control"><option value="Tax exclude">Tax exclude</option><option value="Tax include">Tax include</option></select></td>
<td width="140" align="right"><input type="button" value="Add condition"/></td>
</tr>
				   
</table>
</div>
</div>

</div>
</div>
</div>
<!----------------------->
<!--== CONDITION END ====>
<!----------------------->
			
</div>
  <!-- End Row -->

  <!-- Start Row -->
  <div class="row">
  </div>
  <!-- End Row -->

</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="js/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="js/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Moment.js
================================================ -->
<script type="text/javascript" src="js/moment/moment.min.js"></script>

<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>



<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});

$(document).ready(function() {
  $('#date-picker1').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>
<!-- Basic Single Date Picker -->

<script type="text/javascript">
var contn_grp = 0; 
function show_condition_grp()
{
	contn_grp=contn_grp+1; 
	$.ajax({
		   url:"action/condition-group.php",
		   type:"post",
		   data:{contn_grps:contn_grp},
		   success:function(condition_group){	
	   		
           var cndtn_grp=$.parseHTML(condition_group);		
           $("#cont_grp").append(cndtn_grp);
           }
	       });
		   
		   $("#id_condition").show();
	/**$.ajax({
		   url:"action/condition-group.php",
		   type:"post",
		   data:{contn:contn_grp},
		   success:function(contion){	   		
           var cndtn=$.parseHTML(contion);		
           $("#cont").append(cndtn);
           }
	       });***/
}

</script>

<script type="text/javascript">
var clicks = 0;
function category()
{
++clicks;    
if(clicks=1)
{
var ctgry_val = document.getElementById("id_ctgry").value;
document.getElementById("id_value").value = ctgry_val;
document.getElementById("id_type").value = "Category"; 
}
}

function attr_change()
{
var attr_chge = document.getElementById("id_attribute").value;
alert(attr_chge);
$.ajax({
		   url:"action/condition-group.php",
		   type:"post",
		   data:{atr_chge:attr_chge},
		   success:function(atval){	   		
           var attval=$.parseHTML(atval);  		
           $("#id_atval").append(attval);
           }
	       });
}
</script>

</body>
</html>