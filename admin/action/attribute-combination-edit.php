<?php
include_once('../config.php');  
if(isset($_POST["get_attr_actn"]) && isset($_POST["edit_id"]))
{ 
$get_att_act=mysql_real_escape_string($_POST["get_attr_actn"]);
$edit_id=mysql_real_escape_string($_POST["edit_id"]);
$attrcmtn_tb = mysql_query("SELECT * FROM prdct_attr_cmbntn WHERE CBNTN_ID = '$get_att_act'");
$attrcmtn_rw = mysql_fetch_assoc($attrcmtn_tb);
if(isset($attrcmtn_rw['CBNTN_ID']))
{
?>
<div class="panel-title"> Edit attribute combination </div>
<div class="form-group">
<label for="input002" class="col-sm-2 control-label form-label">Combination title</label>
<div class="col-sm-10">
<select class="selectpicker" name="combinate_title">
<option value="">-Select-</option>
<?php $cmt_tb = mysql_query("SELECT * FROM attr_cmbntn_title t1 LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID WHERE  t1.PRDCT_ID = '$edit_id'") or die("Error in combination title select query !!!");
while($cmt_rw = mysql_fetch_assoc($cmt_tb)):
if($cmt_rw['CTGRY_NAME']=='Car sale' || $cmt_rw['CTGRY_NAME']=='Car rental'): 
$cmt_tb = mysql_query("SELECT * FROM attr_cmbntn_title t1 LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID WHERE  t1.PRDCT_ID = '$edit_id' AND t2.CTGRY_NAME = '$cmt_rw[CTGRY_NAME]'") or die("Error in combination title select query !!!");
while($cmt_rw = mysql_fetch_assoc($cmt_tb)):?>
<option value="<?=$cmt_rw['ACT_ID']; ?>"<?php if(isset($attrcmtn_rw['ACT_ID']) && $attrcmtn_rw['ACT_ID'] == $cmt_rw['ACT_ID']): echo "selected"; endif;?>><?=$cmt_rw['CMBNTN_TITLE']; ?></option>
<?php endwhile; endif; endwhile; ?>  </select>
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label form-label">Product attribute</label>
<div class="col-sm-10">

<select onchange="attr_change(this);" class="selectpicker  get-value" name="attribute" id="id_attribute">
<option>-Select-</option>
<?php $attr_tb = mysql_query("SELECT * FROM prdct_attr") or die("Error in product attribute select query !!!");
while($attr_rw = mysql_fetch_assoc($attr_tb)):?>
<option value="<?=$attr_rw['ATTR_ID']; ?>"><?=$attr_rw['ATTR_NAME']; ?></option>
<?php endwhile; ?>  </select> 
 
<?php if(isset($attrcmtn_rw['CBNTN_ID'])): ?>
<input type="hidden" name="atr_cnbntn_edit_id" value="<?php echo $attrcmtn_rw['CBNTN_ID']; ?>"/>
<?php endif; ?>  
           
</div>
</div>
	
<div id="attrval_div"> </div>
               
				
<div class="form-group">
<label for="input002" class="col-sm-2 control-label form-label"></label>
<div class="col-sm-10">				  
<input type="button" value="Add" class="btn btn-default" onclick="edit_add_attrval_cbntn();"/>
<input type="button" value="Remove" class="btn btn-default" onclick="edit_remove_attrval_cbntn();"/><br/><br/>
<select style="width: 230px;" name="attr_cmbntn[]" id="edit_selected_attrval" multiple="multiple" size="6">
<?php $attr_tb = mysql_query("SELECT * FROM prdct_attr_cmbntn WHERE CBNTN_ID = '$get_att_act'") or die("Error in product attribute select query !!!");
$attr_rw = mysql_fetch_assoc($attr_tb);
if(isset($attr_rw['ATTR_CMBNTN']))
{  
$sprt_cbntn = explode(',',$attr_rw['ATTR_CMBNTN']); 
$cbntn_cnt = count($sprt_cbntn);
$cbntn = 0;
while($cbntn<$cbntn_cnt)
{
if(isset($sprt_cbntn))
{	
$sprt_atr_val = explode(':',$sprt_cbntn[$cbntn]); 
$atr_val_cnt = count($sprt_atr_val);
$atr_val = 0;
while($atr_val<$atr_val_cnt)
{
      $sprt_atr_val[$atr_val];

      $atr_tb = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_val[$atr_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rw = mysql_fetch_assoc($atr_tb);	
	  $atr_val = $atr_val+1;
	  $sprt_atr_val[$atr_val];
	  $val_tb = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_val[$atr_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rw = mysql_fetch_assoc($val_tb);	

?> 
<option value="<?php echo $atr_rw['ATTR_ID']; ?>:<?php echo $val_rw['ATTR_VAL_ID']; ?>"><?php echo $atr_rw['ATTR_NAME']; ?>:<?php echo $val_rw['ATTR_VAL']; ?></option>
<?php  $atr_val++;  } } $cbntn++; } } ?>
</select>
</div>
</div>

<div class="form-group">
<label for="input002" class="col-sm-2 control-label form-label">Reference code</label>
<div class="col-sm-10">
<input type="text" name="ref_code" value="<?php if(isset($attrcmtn_rw['REF_CODE'])): echo $attrcmtn_rw['REF_CODE']; endif; ?>" class="form-control" id="input002" style="width: 200px;">
</div>
</div>
				
				<div class="form-group">
                <label class="col-sm-2 control-label form-label">Impact on price</label>
                <div class="col-sm-10">
                        <select name="impact_price" class="selectpicker"> 
                        <option value="<?=$attrcmtn_rw['IMPACT_PRICE']; ?>"><?=$attrcmtn_rw['IMPACT_PRICE']; ?></option>
                        <option value="">None</option>
						<option value="Increase">Increase</option>
						<option value="Decrease">Decrease</option>						                     
                        </select>               
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Tax include</label>
                  <div class="col-sm-10">
                    <input type="text" name="tax_incde" value="<?php if(isset($attrcmtn_rw['TAX_INCDE'])): echo $attrcmtn_rw['TAX_INCDE']; endif; ?>" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
				<div class="form-group">
                  <label for="input002" class="col-sm-2 control-label form-label">Tax exclude</label>
                  <div class="col-sm-10">
                 <input type="text" name="tax_excde" value="<?php if(isset($attrcmtn_rw['TAX_EXCDE'])): echo $attrcmtn_rw['TAX_EXCDE']; endif; ?>" class="form-control" id="input002" style="width: 200px;">
                  </div>
                </div>
				
			
				<div class="form-group">               
                <label  class="col-sm-2 control-label form-label">Availability Date</label>                    
                <div class="col-sm-10">                        
                <input type="text" name="avail_date" value="<?php if(isset($attrcmtn_rw['AVAIL_DATE'])): echo $attrcmtn_rw['AVAIL_DATE']; endif; ?>" id="date-picker1" class="form-control" style="width: 200px;" value="03/18/2015"/>
                </div>
                </div>
				
<div class="form-group">
<label for="input002" class="col-sm-2 control-label form-label">Images</label>
<div class="col-sm-10">
<label>
<?php
$img_tb = mysql_query("SELECT * FROM prdct_image WHERE PRDCT_ID = '$edit_id'") or die("Error in display select query !!!");
$img_rw = mysql_fetch_assoc($img_tb);
?>
<img src="<?php echo 'product_image/'.$img_rw['PRDCT_THUMBNAIL']; ?>" style="border: 1px solid red; border-radius:5px; width: 100px; height: 100px;" />
<img src="" style="border: 1px solid red; border-radius:5px; width: 100px; height: 100px;" />
<img src="" style="border: 1px solid red; border-radius:5px; width: 100px; height: 100px;" />
<img src="" style="border: 1px solid red; border-radius:5px; width: 100px; height: 100px;" />
</label>
</div>
</div>
				
<div class="form-group">
<label for="input002" class="col-sm-2 control-label form-label"></label>
<div class="col-sm-10">
<div class="edit_hid_combination"></div>
<input type="submit" onclick="edit_submit_attr_frm()" name="send_attribute_combination" value="Update" class="btn btn-default"/>
</div>
<?php } } 
$cmbntn_title=mysql_real_escape_string($_POST["cmbntn"]); 
$prdct_id=mysql_real_escape_string($_POST["prdct"]);
if(isset($_POST["cmbntn"]) && $_POST["cmbntn"] != '' && isset($_POST["prdct"]) && $_POST["prdct"] != '')
{
$cmbntn_title=mysql_real_escape_string($_POST["cmbntn"]);
$prdct_id=mysql_real_escape_string($_POST["prdct"]);
mysql_query("INSERT INTO attr_cmbntn_title(PRDCT_ID,CMBNTN_TITLE) VALUES('$prdct_id', '$cmbntn_title')") or die("Error in cmbntn title insert query!!!");
}
?>