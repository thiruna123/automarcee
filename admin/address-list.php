<?php include_once('header.php'); ?>
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Customer Address</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="#">Customer</a></li>
        <li class="active">Customer Address List</li>
      </ol>

    <!-- Start Page Header Right Div -->
    <div class="right">
      <div class="btn-group" role="group" aria-label="...">
        <a href="index.html" class="btn btn-light">Dashboard</a>
        <a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
      </div>
    </div>
    <!-- End Page Header Right Div -->

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-title">
          <a href="address-add-new.php"><button>Add New Customer Address </button></a>
        </div>
        <div class="panel-body table-responsive">

            <table id="example0" class="table display">
                <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>                       
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Address</th>
						<th>Postal code</th>						
					    <th>City</th>
						<th>Country</th>						
						<th>Action</th>
                    </tr>
                </thead>           
                
             
                <tbody>
				    <tr>
                        <td align="left"></td>
						<td align="left"><input style="width: 20px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 30px; height: 30px;" type="text" name="" value=""/></td>												
                        <td align="left"><input style="width: 30px; height: 30px;" type="text" name="" value=""/></td>						
						<td align="left"><input style="width: 30px; height: 30px;" type="text" name="" value=""/></td>
						<td align="left"><input style="width: 50px; height: 30px;" type="submit" name="" value="Search"/></td>					
                     
                    </tr>

<?php
$delete_id = $_GET['delete_id'];
if(isset($_GET['delete_id']) && $_GET['delete_id'] != ''):
$delete_id = (int)$_GET['delete_id'];
mysql_query("DELETE FROM cstmr_adrs WHERE CSTMR_ADRS_ID = '$delete_id' || SUB_CTGRY = '$delete_id'") or die('Category delete query error !!!');
?> <script type="text/javascript"> alert("Deleted Succesfully !!!"); window.location.href="address-list.php"; </script>
<?php endif; 

$ctradrs_tb = mysql_query("SELECT * FROM cstmr_adrs") or die('Error in customer address select query!!!');
while($ctradrs_rw = mysql_fetch_assoc($ctradrs_tb)): 
?>
                    <tr>													
                        <td><input type="checkbox" name="" value=""/></td>
						<td><?=$ctradrs_rw['CSTMR_ADRS_ID'];?></td>
                        <td><?=$ctradrs_rw['FIRST_NAME'];?></td>
                        <td><?=$ctradrs_rw['LAST_NAME'];?></td>
                        <td><?=$ctradrs_rw['ADRS1'];?></td>
                        <td><?=$ctradrs_rw['PSTL_NO'];?></td> 
						<td><?=$ctradrs_rw['CITY'];?></td>
						<td><?=$ctradrs_rw['COUNTRY'];?></td>						
                        <td>
						<a href="address-add-new.php?edit_id=<?=$ctradrs_rw['CSTMR_ADRS_ID']; ?>">Edit</a>
						&nbsp;&nbsp;
						<a href="address-list.php?delete_id=<?=$ctradrs_rw['CSTMR_ADRS_ID']; ?>">Delete</a>
						
						</td>
                    </tr>
<?php endwhile; ?>
                 
                </tbody>
            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->
 </div>
  <!-- End Row -->

</div>
<!-- END CONTAINER -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 


<!-- Start Footer -->
<div class="row footer">
  <div class="col-md-6 text-left">
  Copyright © 2015 <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a> All rights reserved.
  </div>
  <div class="col-md-6 text-right">
    Design and Developed by <a href="http://www.webnexs.com/php-online-booking-script.php" target="_blank">webnexs</a>
  </div> 
</div>
<!-- End Footer -->


</div>
<!-- End Content -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
 

<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>
<!-- ================================================
Bootstrap Date Range Picker
================================================ -->
<script type="text/javascript" src="js/date-range-picker/daterangepicker.js"></script>


<!-- Basic Date Range Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-picker').daterangepicker(null, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Basic Single Date Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-picker').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

<!-- Date Range and Time Picker -->
<script type="text/javascript">
$(document).ready(function() {
  $('#date-range-and-time-picker').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'MM/DD/YYYY h:mm A'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
});
</script>

</body>
</html>