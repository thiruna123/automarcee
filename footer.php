<!-- FOOTER -->
<footer id="footer" >
<div class="container">
<div class="row margin-top-60 margin-bottom-40 size-15 color-black">
<!-- col #1 -->
<div class="col-md-12 col-sm-12">
<div class="row">
<div class="col-md-2">
<h4 class="letter-spacing-1 font-alice">BUSINESS INFO</h4>
<ul class="list-unstyled footer-list half-paddings noborder">
<li><a class="block" href="<?php echo BASEPATH; ?>car-sale"><i class="fa fa-angle-right"></i> Home</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>about-us"><i class="fa fa-angle-right"></i> About Us</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>terms-conditions"><i class="fa fa-angle-right"></i> Terms &amp; Conditions</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>privacy-policy"><i class="fa fa-angle-right"></i> Privacy Policy</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>testimonial"><i class="fa fa-angle-right"></i> Testimonial</a></li>
</ul>
</div>
<div class="col-md-2">
<h4 class="letter-spacing-1 font-alice uppercase">Buy Cars</h4>
<ul class="list-unstyled footer-list half-paddings noborder">
<li><a class="block" href="<?php echo BASEPATH; ?>search-the-car"><i class="fa fa-angle-right"></i> Stock search</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>best-dealer-for-buy-the-car"><i class="fa fa-angle-right"></i> Latest Arrival</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>discounted-cars"><i class="fa fa-angle-right"></i> Discounted cars</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>request-car"><i class="fa fa-angle-right"></i> Request Car</a></li>
</ul>
</div>
<div class="col-md-2">
<h4 class="letter-spacing-1 font-alice uppercase">Cars Rental</h4>
<ul class="list-unstyled footer-list half-paddings noborder">
<li><a class="block" href="<?php echo BASEPATH; ?>best-dealer-for-rental-car"><i class="fa fa-angle-right"></i> Our Cars for rental</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>car-booking"><i class="fa fa-angle-right"></i> Book a car</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>car-booking"><i class="fa fa-angle-right"></i> Prices</a></li>
<li><a class="block" href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-angle-right"></i> Modify My Reservation</a></li>
</ul>
</div>
<div class="col-md-2">
<h4 class="letter-spacing-1 font-alice uppercase">Tyres</h4>
<ul class="list-unstyled footer-list half-paddings noborder">
<li><a class="block" href="<?php echo BASEPATH; ?>discounted-tyre-for-buying"><i class="fa fa-angle-right"></i> Discounted Tires</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>latest-tyre-for-buying"><i class="fa fa-angle-right"></i> New Tires</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>used-tyre-for-buying"><i class="fa fa-angle-right"></i> Used Tires</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>request-tyre-for-ordering"><i class="fa fa-angle-right"></i> Request Tires</a></li>
</ul>
</div>
<div class="col-md-2">
<h4 class="letter-spacing-1 font-alice uppercase">Services</h4>
<ul class="list-unstyled footer-list half-paddings noborder">
<li><a class="block" href="<?php echo BASEPATH; ?>shipping-schedule"><i class="fa fa-angle-right"></i> Shipping Schudule</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>parts"><i class="fa fa-angle-right"></i> Parts</a></li>
<li><a class="block" href="<?php echo BASEPATH; ?>about-team"><i class="fa fa-angle-right"></i> About Our Team</a></li>
</ul>
</div>
<div class="col-md-2 padd-left-40">
<h4 class="letter-spacing-1 font-alice uppercase font-size-20">Contact Us </h4>
<ul class="list-unstyled footer-list half-paddings noborder">
<li><a class="block" href="<?php echo BASEPATH; ?>blogs"><i class="fa fa-angle-right"></i> Blog</a></li>
<li><a class="block" href="contact-us"><i class="fa fa-angle-right"></i> Contact Us</a></li>
<li><a class="block" href="#"><i class="fa fa-angle-right"></i> Follow Us</a></li>
</ul>
    <?
    include('config.php');
$query = mysql_query("SELECT * from social_id where id='1'");
    $row = mysql_fetch_assoc($query);
 
    ?>
<!-- Social Icons -->
<div class="clearfix">
<a href="<? echo $row['FB'];?>" class="social-icon social-icon-sm social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">
<i class="icon-facebook"></i>
<i class="icon-facebook"></i>
</a>
<a href="<? echo $row['Twitter'];?>" class="social-icon social-icon-sm social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">
<i class="icon-twitter"></i>
<i class="icon-twitter"></i>
</a>
<a href="<? echo $row['Gplus'];?>" class="social-icon social-icon-sm social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
<i class="icon-gplus"></i>
<i class="icon-gplus"></i>
</a>
<a href="<? echo $row['Linkedin'];?>" class="social-icon social-icon-sm social-icon-border social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
<i class="icon-linkedin"></i>
<i class="icon-linkedin"></i>
</a>
</div>
<!-- /Social Icons -->
<ul class="list-unstyled footer-list half-paddings noborder">
<li class="contact"><a href="#"><i class="glyphicon glyphicon-earphone" style="color: #b11f27;"></i> <span> <? echo $row['Phone1'];?> / <br />  <? echo $row['Phone2'];?></span></a></li>
</ul>
</div>
</div>
</div>
<!-- /col #2 -->
</div>
<div class="row size-13">
<!-- col #1 -->
<div class="col-md-2 col-sm-2">
<!-- Footer Logo -->
<a href="best-dealer-for-rental-car"><img class="logo-footer" src="<?php echo BASEPATH; ?>assets/img/auto-sales.png" alt="" /></a>
<p class="text-center nomargin color-black">East Street</p>
<p class="color-black text-center">+1-242-394 42 62</p>
</div>
<!-- /col #1 -->
<!-- col #2 -->
<div class="col-md-4 col-sm-4 col-sm-offset-2 text-center">
<!-- Footer Logo -->
<a href="best-dealer-for-buy-the-tire"><img class="logo-footer" src="<?php echo BASEPATH; ?>assets/img/car-rental.png" alt="" /></a>
<p class="nomargin color-black">East Street</p>
<p class="color-black">+1-242-394 42 62</p>
</div>
<!-- /col #2 -->
<!-- col #3 -->
<div class="col-md-4 col-sm-4 padd-right-20">
<div class="pull-right">
<!-- Footer Logo -->
<a href="best-dealer-for-buy-the-tire"><img class="logo-footer" src="<?php echo BASEPATH; ?>assets/img/tire-sales.png" alt="" /></a>
<p class="text-center nomargin color-black">Soldier Road</p>
<p class="text-center color-black">+1-242-394 42 62</p>
</div>
</div>
<!-- /col #3 -->                        
</div>
</div>
<div class="copyright">
<div class="container color-black text-center">
&copy; Sitemap * Copyright © 2016. Marce &amp; Steve Auto Sales. All Rights Reserved. * Created, Maintained &  Online Marketed by williamchristober. *  Login
</div>
</div>
</footer>
<!-- /FOOTER -->
</div>
<!-- /wrapper -->
<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>
<!-- PRELOADER -->
<!-- /PRELOADER -->
<!-- JAVASCRIPT FILES -->
<script type="text/javascript">var plugin_path = '<?php echo BASEPATH; ?>assets/plugins/';</script>
<script type="text/javascript" src="<?php echo BASEPATH; ?>assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo BASEPATH; ?>assets/js/scripts.js"></script>
<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="<?php echo BASEPATH; ?>assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo BASEPATH; ?>assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo BASEPATH; ?>assets/js/view/demo.revolution_slider.js"></script>
<!-- PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<?php echo BASEPATH; ?>assets/js/view/demo.shop.js"></script>
<!--- Date Picker for pick up and drop off ----->
<!-- Load jQuery UI CSS  -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!-- Load jQuery UI Main JS  -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Load SCRIPT.JS which will create datepicker for input field  -->
<script src="<?php echo BASEPATH;?>date/script_2.js"></script>
<script type="text/javascript">
//var baseurl='<?php echo BASEPATH; ?>';
//var uri='<?php echo $_SERVER["REQUEST_URI"]; ?>';
function change_crncy(t)
{
	// alert(uri);
	// alert(baseurl);
	// return false;
	var chng_cnry = $(t).val(); //alert(chng_cnry);
	var act_prc = $('#price_id').val(); //alert(act_prc);
	//return false;
	window.location.href="?chng_cnry="+chng_cnry;
}
</script>
</body>
</html>