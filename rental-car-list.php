<?php include_once('header.php'); ?><!-- Header -->
<script>
$().ready(function(){
$("#quick_book_id").validate();
$("#manage_book_id").validate();
});	
</script>
			<section class="page-header page-header nopadding-bottom" style="background:url('assets/img/slider1.png') center scroll;"> 
                <div class="container">
                    <div class="row">

<div class="col-md-4 col-sm-4 col-sm-offset-7">
                            
                            
                            <!-- CHECKOUT -->


<div class="col-lg-12 col-sm-12 border-box-white">
<ul class="nav nav-tabs header-tabs nav-clean nopadding museo-300">
<li class="active"><a href="#home" data-toggle="tab">Quick Booking</a></li>
<li><a href="#profile" data-toggle="tab">Manage Booking</a></li>
</ul>

<div class="tab-content padding-20">
<div class="tab-pane fade in active" id="home">
<form id="quick_book_id" class="row clearfix nomargin-bottom" method="post" action="<?php echo BASEPATH; ?>car-booking" autocomplete="off">                     
<!-- BILLING -->
<fieldset class="margin-top-10">
<div class="row">
<div class="col-md-12 col-sm-12 museo-300">

<label for="billing_email"> Pick-up Location  </label> 
<select name="pickup_loc" class="form-control required form-border" required>
 <option value="">-Select Pickup Location-</option>
<?php 
$slct_pick_tb = mysql_query("SELECT * FROM selected_location") or die("Error in select location select que"); 	
while($slct_pick_rw = mysql_fetch_assoc($slct_pick_tb))
{
$sprt_pick_loc = explode(',',$slct_pick_rw['PICKUP_LOC']); 
$sprt_pick_cnt = count($sprt_pick_loc);
$pick_loc_val = 0;
while($pick_loc_val<$sprt_pick_cnt)
{ $sprt_pick_loc[$pick_loc_val];	
$pick_loc_tb = mysql_query("SELECT * FROM pickup_location WHERE PICKUP_ID = '$sprt_pick_loc[$pick_loc_val]'") or die("Error in pick up location select query !!!"); 	
while($pick_loc_rw = mysql_fetch_assoc($pick_loc_tb))
{ 
?> 
<option value="<?php echo $pick_loc_rw['PICKUP_ID']; ?>"><?php echo $pick_loc_rw['PICKUP_STREET_NAME']; ?>&nbsp;IN&nbsp;<?php echo $pick_loc_rw['PICKUP_TOWN_NAME']; ?></option> 
<?php } $pick_loc_val++;  } } ?>
 </select>  

<label class="checkbox pull-left margin-top-10"><!-- see assets/js/view/demo.shop.js - CHECKOUT section -->
<input id="drop_id" name="drop_click" type="checkbox" value="1" onchange="dropclick(this);"  />
<i></i> <span class="weight-300 size-13">Return Car to a different location</span>
</label>
</div>
</div>

<div class="row" id="show_drop" style="display: none;">
<div class="col-md-12 col-sm-12 museo-300">

<label for="billing_email"> Drop-off Location  </label> 
<select name="drop_loc" class="form-control required form-border">
<option value="">-Select Drop Location-</option>
<?php 
$slct_drop_tb = mysql_query("SELECT * FROM selected_location") or die("Error in select location select que"); 	
while($slct_drop_rw = mysql_fetch_assoc($slct_drop_tb))
{
$sprt_drop_loc = explode(',',$slct_drop_rw['DROP_LOC']); 
$sprt_drop_cnt = count($sprt_drop_loc);
$drop_loc_val = 0;
while($drop_loc_val<$sprt_drop_cnt)
{ $sprt_drop_loc[$drop_loc_val];	
$drop_loc_tb = mysql_query("SELECT * FROM drop_location WHERE DROP_ID = '$sprt_drop_loc[$drop_loc_val]'") or die("Error in pick up location select query !!!"); 	
while($drop_loc_rw = mysql_fetch_assoc($drop_loc_tb))
{ 
?> 
<option value="<?php echo $drop_loc_rw['DROP_ID']; ?>"><?php echo $drop_loc_rw['DROP_STREET_NAME']; ?>&nbsp;&nbsp;IN &nbsp;&nbsp;<?php echo $drop_loc_rw['DROP_TOWN_NAME']; ?></option> 
<?php }  $drop_loc_val++;  } } ?>
 </select>  

</div>
</div>

<div class="row">

<div class="col-md-12 col-sm-12 museo-300">

<label for="billing_email">Pick-up Date and Time: </label>

<div class="col-md-5 col-sm-5 museo-300 nopadding-left">
<input type="text" name="pickup_date" id="pickup_date" value="" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" class="form-control form-border" required/>
</div>

<div class="col-md-7 col-sm-7 museo-300 nopadding-right nopadding-left">
<input type="text" name="pickup_time" value="" class="form-control form-border timepicker" required/>
</div>  
</div>    
</div>

<div class="row">

<div class="col-md-12 col-sm-12 museo-300">        


<label for="billing_email">Return Date and Time: </label>

<div class="col-md-5 col-sm-5 museo-300 nopadding-left">
<input type="text" name="drop_date" id="dropoff_date" value="" id="dropoff_date" data-format="mm/dd/yyyy" data-lang="en" data-RTL="false" class="form-control form-border" required/>
</div>

<div class="col-md-7 col-sm-7 museo-300 nopadding-right nopadding-left">
<input type="text" name="drop_time"  value=""  class="form-control form-border timepicker" required/>
</div>    
</div>
</div>    

<div class="row">
<div class="col-md-12 col-sm-12 museo-300">
<button type="submit" name="quick_book" class="btn btn-primary btn-lg btn-block size-25 museo-500 uppercase noborder-radius"> Book Now</button>
</div>
</div>
</fieldset>
                                    <!-- /BILLING -->
</div>
</form>


<div class="tab-pane fade" id="profile">
<form id="manage_id" class="row clearfix nomargin-bottom" method="post" action="<?php echo BASEPATH; ?>car-booking">
<fieldset class="margin-top-10">
<div class="row">
<div class="col-md-12 col-sm-12 museo-300 margin-top-30">
<label for="billing_email"> Order Number  </label>
<input id="billing_firstname" name="billing[firstname]" type="text" class="form-control form-border" required/>
</div>
</div>
                                        
<div class="row">
<div class="col-md-12 col-sm-12 museo-300 margin-top-50">
<label for="billing_email"> Your Email  </label>
<input id="billing_firstname" name="billing[firstname]" type="text" class="form-control form-border" required/>
</div>
</div>


<div class="row">
<div class="col-md-12 col-sm-12 museo-300 margin-top-60">
<button class="btn btn-primary btn-lg btn-block size-15 museo-500 uppercase noborder-radius"> Show Order Details </button>
</div>
</div>
</fieldset>
                                    <!-- /BILLING -->
</div>
</div>

</div>
</form>

					
					<!-- /CHECKOUT -->
                        </div>
                    </div>
                </div>
            </section>
			<!-- /Header -->


			



			<!-- INFO BAR -->
			<section class="nopadding noborder">
				<div class="container">
                        <div class="row">

                            <div class="col-md-4">

                                <div class="box-icon bordered">
                                    <a href="#">
                                        <i><img src="<?php echo BASEPATH; ?>assets/img/rental-cars/cars.png" /></i>
                                        <h4 class="pull-right museo-300 color-grey nomargin">Our Cars</h4>
                                    </a>
                                </div>

                            </div>

                            <div class="col-md-4 ">

                                <div class="box-icon bordered">
                                    <a href="#">
                                        <i><img src="<?php echo BASEPATH; ?>assets/img/rental-cars/locations.png" /></i>
                                        <h4 class="pull-right museo-300 color-grey nomargin">Our Locations</h4>
                                    </a>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="box-icon bordered">
                                    <a href="#">
                                        <i><img src="<?php echo BASEPATH; ?>assets/img/rental-cars/reservation.png" /></i>
                                        <h4 class="pull-right museo-300 color-grey nomargin">Manage Reservations</h4>
                                    </a>
                                </div>

                            </div>
                    </div>
				</div>
			</section>
			<!-- /INFO BAR -->



			<!-- FEATURED -->
			<section class="padding-30">
				<div class="container">

                    <h1 class="text-center latest-head border-bg uppercase"><strong>Our Rental Fleet</strong></h1>
                    <hr style="margin-top: -25px;"/>
<div class="row">	
<ul class="nav nav-tabs product type museo-300">
<li class="active first"><a href="#all" data-toggle="tab" onclick="show_allbodytype();">All</a></li>
<li><a href="#" data-toggle="tab" onclick="show_hatchback();">HatchBack</a></li>
<li><a data-toggle="tab" href="#" onclick="show_sedan();">Sedan</a></li>
<li><a data-toggle="tab" href="#" onclick="show_jeep();">SUV/Jeep</a></li>
</ul>
</div>
                    
                    
<div class="tab-content col-sm-12">
<div class="tab-pane fade in active" id="all">
<div class="row padding-top-50">                                      
<!-- item -->
                                           <!-- /item -->

</div> 

<div class="row padding-top-50" id="allbody_id">                                      
<!-- item -->
<?php
$gt = mysql_query("SELECT * FROM rental_details WHERE DROPOFF_DATE <= CURDATE()") or die("Error in check the rental customer !!!");
while($get_rw = mysql_fetch_assoc($gt))
{
if(!empty($get_rw['PRDCT_ID']))
{
mysql_query("UPDATE prdct_add SET RENT = 'No' WHERE PRDCT_ID = '$get_rw[PRDCT_ID]'") or die("Error in product add update the rental details !!!");
}
}
$prdct_tb = mysql_query(
"SELECT 
t1.PRDCT_ID, t1.PRDCT_NAME, t1.RENT, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT, t4.ACT_ID, 
t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL FROM prdct_add t1 
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes'  AND t5.CTGRY_NAME = 'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!"); 
$i=1; while($prdct_rw = mysql_fetch_assoc($prdct_tb)){ ?>
<div class="col-sm-4">
<div class="shop-item">
<div class="noborder nopadding"> 
<!-- product image(s) -->
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<img width="326" height="193" src="<?php echo BASEPATH.'admin/product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>"/>
</a>
<!-- /product image(s) -->
</div>
<div class="shop-item-summary text-center">
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<h2 class="nomargin-bottom text-center size-30 museo-700 color-black"><?php echo $prdct_rw['PRDCT_NAME']; ?></h2>
<!-- price -->
<div class="shop-item-price size-40 museo-700 pull-left">
<span class="price-tag rental-tag">
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'.$prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; }elseif(isset($prdct_rw['TOTAL_PRICE'])){ echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE']; } ?>
</span>Day</div>
<!-- /price -->
</a>    
 </div>
</div>
</div>
 <?php } ?>
</div>
    <div class="row padding-top-50" id="hatch_id" style="display: none;"> 
<?php
$cbntn_tb = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn");
while($cbntn_rw  = mysql_fetch_assoc($cbntn_tb)): 
$sprt_atr_vall = explode(':',$cbntn_rw['ATTR_CMBNTN']); 
$atr_val_cntl = count($sprt_atr_vall);
$atr_vall = 0;
while($atr_vall<$atr_val_cntl)
{
      $sprt_atr_vall[$atr_vall];
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);	
	  $atr_vall = $atr_vall+1;
	  $sprt_atr_vall[$atr_vall];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl);

if($atr_rwl['ATTR_NAME'] == 'Body type' && $val_rwl['ATTR_VAL'] == 'Hatchback'):
$one =  $atr_rwl['ATTR_ID']; $two = ':'; $three = $val_rwl['ATTR_VAL_ID'];
$hatchback = $one."$two".$three;
$prdct_tb = mysql_query(
"SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT,
t4.ACT_ID, t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL
FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes' AND FIND_IN_SET('$hatchback', t4.ATTR_CMBNTN) 
AND t5.CTGRY_NAME =  'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!");
$i=1; while($prdct_rw  = mysql_fetch_assoc($prdct_tb)):


?>
<div class="col-sm-4">
<div class="shop-item">
<div class="noborder nopadding">
<!-- product image(s) -->
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<img width="326" height="193" src="<?php echo BASEPATH.'admin/product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>" alt="" />
</a>
<!-- /product image(s) -->
</div>
<div class="shop-item-summary text-center">
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<h2 class="nomargin-bottom text-center size-30 museo-700 color-black"><?php echo $prdct_rw['PRDCT_NAME']; ?></h2>
<!-- price -->
<div class="shop-item-price size-40 museo-700 pull-left">
<span class="price-tag rental-tag">
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'.$prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; }elseif(isset($prdct_rw['TOTAL_PRICE'])){ echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE']; } ?>
</span>
Day
</div>
<!-- /price -->
</a>    
 </div>
</div>
</div>
<?php endwhile; endif;  $atr_vall++;  }  endwhile; ?>  <!-- /item -->
</div>

<div class="row padding-top-50" id="sedan_id" style="display: none;"> 
<?php
$cbntn_tb = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn");
while($cbntn_rw  = mysql_fetch_assoc($cbntn_tb)): 
$sprt_atr_vall = explode(':',$cbntn_rw['ATTR_CMBNTN']); 
$atr_val_cntl = count($sprt_atr_vall);
$atr_vall = 0;
while($atr_vall<$atr_val_cntl)
{
      $sprt_atr_vall[$atr_vall];
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);	
	  $atr_vall = $atr_vall+1;
	  $sprt_atr_vall[$atr_vall];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl);

if($atr_rwl['ATTR_NAME'] == 'Body type' && $val_rwl['ATTR_VAL'] == 'Sedan'):
$one =  $atr_rwl['ATTR_ID']; $two = ':'; $three = $val_rwl['ATTR_VAL_ID'];
$sedan = $one."$two".$three;
$prdct_tb = mysql_query(
"SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE,
t4.ACT_ID, t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL
FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes' AND FIND_IN_SET('$sedan', t4.ATTR_CMBNTN) 
AND t5.CTGRY_NAME =  'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!");
$i=1; while($prdct_rw  = mysql_fetch_assoc($prdct_tb)):


?>
<div class="col-sm-4">
<div class="shop-item">
<div class="noborder nopadding">
<!-- product image(s) -->
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<img width="326" height="193" src="<?php echo BASEPATH.'admin/product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>" alt="" />
</a>
<!-- /product image(s) -->
</div>
<div class="shop-item-summary text-center">
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<h2 class="nomargin-bottom text-center size-30 museo-700 color-black"><?php echo $prdct_rw['PRDCT_NAME']; ?></h2>
<!-- price -->
<div class="shop-item-price size-40 museo-700 pull-left">
<span class="price-tag rental-tag">
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'.$prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; }elseif(isset($prdct_rw['TOTAL_PRICE'])){ echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE']; } ?>
</span>
Day
</div>
<!-- /price -->
</a>    
 </div>
</div>
</div>
<?php endwhile; endif;  $atr_vall++;  }  endwhile; ?>  <!-- /item -->
</div>

<div class="row padding-top-50" id="jeep_id" style="display: none;"> 
<?php
$cbntn_tb = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn");
while($cbntn_rw  = mysql_fetch_assoc($cbntn_tb)): 
$sprt_atr_vall = explode(':',$cbntn_rw['ATTR_CMBNTN']); 
$atr_val_cntl = count($sprt_atr_vall);
$atr_vall = 0;
while($atr_vall<$atr_val_cntl)
{
      $sprt_atr_vall[$atr_vall];
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);	
	  $atr_vall = $atr_vall+1;
	  $sprt_atr_vall[$atr_vall];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atr_vall[$atr_vall]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl);

if($atr_rwl['ATTR_NAME'] == 'Body type' && $val_rwl['ATTR_VAL'] == 'Jeep'):
$one =  $atr_rwl['ATTR_ID']; $two = ':'; $three = $val_rwl['ATTR_VAL_ID'];
$jeep = $one."$two".$three;
$prdct_tb = mysql_query(
"SELECT t1.PRDCT_ID, t1.PRDCT_NAME, t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE,
t4.ACT_ID, t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL
FROM prdct_add t1
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() AND t1.ENABLE = '1' AND  t1.RENT != 'Yes' AND FIND_IN_SET('$jeep', t4.ATTR_CMBNTN) 
AND t5.CTGRY_NAME =  'Car rental'
GROUP BY t1.PRDCT_ID") or die("Error in product listing selecting query !!!");
$i=1; while($prdct_rw  = mysql_fetch_assoc($prdct_tb)):


?>
<div class="col-sm-4">
<div class="shop-item">
<div class="noborder nopadding">
<!-- product image(s) -->
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<img width="326" height="193" src="<?php echo BASEPATH.'admin/product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>" alt="" />
</a>
<!-- /product image(s) -->
</div>
<div class="shop-item-summary text-center">
<a href="<?php echo BASEPATH; ?>rentalcars/<?php echo $prdct_rw['SEO_URL']; ?>">
<h2 class="nomargin-bottom text-center size-30 museo-700 color-black"><?php echo $prdct_rw['PRDCT_NAME']; ?></h2>
<!-- price -->
<div class="shop-item-price size-40 museo-700 pull-left">
<span class="price-tag rental-tag">
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'.$prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; }elseif(isset($prdct_rw['TOTAL_PRICE'])){ echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE']; } ?>
</span>
Day
</div>
<!-- /price -->
</a>    
 </div>
</div>
</div>
<?php endwhile; endif;  $atr_vall++;  }  endwhile; ?> <!-- /item -->
</div>

  

                                    
 <!-- View All-->
 <!--<div class="shop-item-buttons text-center padding-top-30" id="jeep_id_view" style="display: none;">
 <a href="#" class="btn btn-default btn-red noborder-radius btn-danger" ><i class="fa fa-plus"></i> View more</a>
 </div>-->
 <!-- /View All -->
                                </div>


                                <div class="tab-pane fade" id="hatchback">
                                    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
                                </div>

                                <div class="tab-pane fade" id="sedan">
                                    <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold.</p>
                                </div>

                                <div class="tab-pane fade" id="jeep">
                                    <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party.</p>
                                </div>
                            </div>
                        
	<hr style="margin-bottom: -20px;"/>
				</div>
			</section>
			<!-- /FEATURED -->
<script type="text/javascript">
function show_allbodytype()
{
$("#allbody_id").show(); $("#hatch_id").hide(); $("#sedan_id").hide(); $("#jeep_id").hide();	
}
function show_hatchback()
{
$("#hatch_id").show(); $("#allbody_id").hide(); $("#sedan_id").hide(); $("#jeep_id").hide();
}
function show_sedan()
{
$("#sedan_id").show(); $("#hatch_id").hide(); $("#allbody_id").hide(); $("#jeep_id").hide();
}
function show_jeep()
{
$("#jeep_id").show(); $("#jeep_id_view").show(); $("#sedan_id").hide(); $("#hatch_id").hide(); $("#allbody_id").hide();
}
</script>
<script type="text/javascript">
function dropclick(t)
{ 
 // alert($("#drop_id").is(":checked"));
 if($("#drop_id").is(":checked")){ $('#show_drop').show(); }else{ $('#show_drop').hide(); } 

}

</script>
<?php include_once('footer.php'); ?>