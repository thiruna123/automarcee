<?php include_once('header.php'); 
if(isset($_POST['car_and_tire']))
{
$prdct_id = implode(',',$_POST['prdct_id']);
$prdct_name = implode(',',$_POST['prdct_name']);
$qnty = implode(',',$_POST['qnty']);
$price = implode(',',$_POST['price']);
$sub_tot = implode(',',$_POST['sub_tot']);
$total = $_POST['total'];
}
?>
<script>
$().ready(function(){
$("#exist_cst").validate();
$("#quote_id").validate();
});	
</script>
<!-- CART -->
<section>
<div class="container">
<?php if($uid == 0){ ?>
<!-- NOT LOGGED IN -->
<div class="panel panel-default">
<div class="panel-body">
<!---<strong>You are not logged in!</strong>
Please, <a href="page-login-1.html">login</a> or <a href="javascript:;" onclick="jQuery('#accountswitch').trigger('click'); _scrollTo('#newaccount', 200);">create an account</a> for later use.--->
<h2 class="text-center museo-700 size-30 padding-top-30 color-black uppercase">			
SEARCH FOR EXISTING CUSTOMER DETAILS</h2>						
<fieldset>
<form id="exist_cst">
<div class="row">
<div class="form-group fetch  museo-300">
<div class="col-md-4 col-sm-4 col-sm-offset-1">
<input type="text" name="email" id="id_email"  placeholder="Email Address" value="" class="form-control" required>
</div>
<div class="col-md-4 col-sm-4">
<input type="text" name="pswd" id="id_pswd" placeholder="Password" value="" class="form-control " required>
</div>
<div class="col-md-2 col-sm-2">
<input type="button" onclick="exist_cstmr();" class="btn btn-primary btn-green noborder noborder-radius" value="Fetch Details">
</div>
</div>
</div>
</form>
</fieldset>						
</div>
</div><!-- /NOT LOGGED IN -->					
<?php } 
$sql = mysql_query("SELECT * FROM ms_customer WHERE CSTMR_ID = '$uid'");
$row = mysql_fetch_assoc($sql);
?>
<!-- CHECKOUT -->
<form class="row clearfix paypal" action="<?php echo BASEPATH; ?>billing-for-products" method="post" id="paypal_form">
<div class="col-lg-7 col-sm-7">
<div class="heading-title">
<h4>Billing &amp; Shipping</h4>
</div>
<input type="hidden" name="prdct_id" value="<?php echo $prdct_id; ?>"/>
<input type="hidden" name="prdct_name" value="<?php echo $prdct_name; ?>"/>
<input type="hidden" name="qnty" value="<?php echo $qnty; ?>"/>
<input type="hidden" name="price" value="<?php echo $price; ?>"/>
<input type="hidden" name="sub_tot" value="<?php echo $sub_tot; ?>"/>
<input type="hidden" name="total" value="<?php echo $total; ?>"/>
<!-- BILLING -->
<div id="new_cstmr">
<fieldset class="margin-top-60">
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="billing_company">Title</label>
<select name="blg_title" class="form-control">
<option value="Mr" <? if(isset($row['TITLE']) && $row['TITLE'] == 'Mr'): echo 'selected'; endif;?>>Mr</option>
<option value="Miss" <? if(isset($row['TITLE']) && $row['TITLE'] == 'Miss'): echo 'selected'; endif;?>>Miss</option>
<option value="Mrs" <? if(isset($row['TITLE']) && $row['TITLE'] == 'Mrs'): echo 'selected'; endif;?>>Mrs</option>
</select>
<input type="hidden" name="cstmr_id" value="<?php echo $row['CSTMR_ID']; ?>"/>
</div>
<div class="col-md-6 col-sm-6">
<label for="billing_firstname">First Name *</label>
<input id="billing_firstname" name="blg_firstname" type="text" value="<?php echo $row['FIRST_NAME']; ?>" class="form-control" />
</div>									
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="billing_lastname">Last Name *</label>
<input id="billing_lastname" name="blg_lastname" type="text" value="<?php echo $row['LAST_NAME']; ?>" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="billing_email">Email *</label>
<input id="billing_email" name="blg_email" type="email" value="<?php echo $row['EMAIL']; ?>" class="form-control" />
</div>									
</div>
<div class="row">
<div class="col-lg-12">
<label for="billing_address1">Address *</label>
<input id="billing_address1" name="blg_address1" type="text" value="<?php echo $row['ADDRESS1']; ?>" class="form-control" placeholder="Address 1" />
<input id="billing_address2" name="blg_address2" type="text" value="<?php echo $row['ADDRESS2']; ?>" class="form-control margin-top-10" placeholder="Address 2" />
</div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="billing_city">City *</label>
<input id="billing_city" name="blg_city" type="text" value="<?php echo $row['CITY']; ?>" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="billing_state">State *</label>
<select id="billing_state" name="blg_state" class="form-control pointer" >
<option value="<?php echo $row['STATE']; ?>"><?php echo $row['STATE']; ?></option>
<option value="">Select...</option>
<option value="1">Alabama</option>
<option value="2">Alaska</option>
<option value="">..............</option>
</select>
</div>
</div>
<div class="row">									
<div class="col-md-6 col-sm-6">
<label for="billing_country">Country *</label>
<select id="billing_country" name="blg_country" class="form-control pointer" >
<option value="<?php echo $row['COUNTRY']; ?>"><?php echo $row['COUNTRY']; ?></option>
<option value="">Select...</option>
<option value="1">united States</option>
<option value="2">united Kingdom</option>
<option value="">..............</option>
</select>
</div>
<div class="col-md-6 col-sm-6">
<label for="billing_zipcode">Zip/Postal Code *</label>
<input id="billing_zipcode" name="blg_postal" type="text" value="<?php echo $row['PINCODE']; ?>" class="form-control" />
</div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="billing_phone">Phone *</label>
<input id="billing_phone" name="blg_phone" type="text" value="<?php echo $row['PHONE']; ?>" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="billing_fax">Fax</label>
<input id="billing_fax" name="blg_fax" type="text" value="<?php echo $row['FAX']; ?>" class="form-control" />
</div>
</div>
</fieldset>
</div>
<div id="exst_cstmr"></div>
<fieldset class="margin-top-70">
<hr />
<div class="row">
<div class="col-lg-12 nomargin clearfix">
<label class="checkbox pull-left"><!-- see assets/js/view/demo.shop.js - CHECKOUT section -->
<input id="shipswitch" name="same_as_billing" type="checkbox" value="1" checked="checked" />
<i></i> <span class="weight-300">Ship to the same address</span>
</label>
</div>
</div>
</fieldset>
<!-- /BILLING -->
<!-- SHIPPING -->
<fieldset id="shipping" class="margin-top-80 softhide">
<h4>Shipping Address</h4>
<hr />
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="shipping:company">Title *</label>
<select name="shp_title" class="form-control required">
<option value="Mr" <? if(isset($row['SHP_TITLE']) && $row['SHP_TITLE'] == 'Mr'): echo 'selected'; endif;?>>Mr</option>
<option value="Mrs" <? if(isset($row['SHP_TITLE']) && $row['SHP_TITLE'] == 'Mrs'): echo 'selected'; endif;?>>Mrs</option>
<option value="Miss" <? if(isset($row['SHP_TITLE']) && $row['SHP_TITLE'] == 'Miss'): echo 'selected'; endif;?>>Miss</option>
</select>
</div>
<div class="col-md-6 col-sm-6">
<label for="shipping:firstname">First Name *</label>
<input id="shipping:firstname" name="shp_firstname" type="text" value="" class="form-control"/>
</div>									
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="shipping:lastname">Last Name *</label>
<input id="shipping:lastname" name="shp_lastname" type="text" value="" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="shipping:email">Email *</label>
<input id="shipping:email" name="shp_email" type="text" value="" class="form-control"/>
</div>									
</div>
<div class="row">
<div class="col-lg-12">
<label for="shipping:address1">Address *</label>
<input id="shipping:address1" name="shp_address1" type="text" value="" class="form-control" placeholder="Address 1" />
<input id="shipping:address2" name="shp_address2" type="text" value="" class="form-control margin-top-10" placeholder="Address 2" />
</div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="shipping:city">City *</label>
<input id="shipping:city" name="shp_city" type="text" value="" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="shipping:state">State *</label>
<select id="shipping:state" name="shp_state" class="form-control pointer" >
<option value="">Select...</option>
<option value="1">Alabama</option>
<option value="2">Alaska</option>
<option value="">..............</option>
</select>
</div>
</div>
<div class="row">									
<div class="col-md-6 col-sm-6">
<label for="shipping:country">Country *</label>
<select id="shipping:country" name="shp_country" class="form-control pointer" >
<option value="">Select...</option>
<option value="1">united States</option>
<option value="2">united Kingdom</option>
<option value="">..............</option>
</select>
</div>
<div class="col-md-6 col-sm-6">
<label for="shipping:zipcode">Zip/Postal Code *</label>
<input id="shipping:zipcode" name="shp_postal" type="text" value="" class="form-control" />
</div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<label for="shipping:phone">Phone *</label>
<input id="shipping:phone" name="shp_phone" type="text" value="" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="shipping:fax">Fax</label>
<input id="shipping:fax" name="shp_fax" type="text" value="" class="form-control" />
</div>
</div>
</fieldset>
<!-- /SHIPPING -->
</div>
<div class="col-lg-5 col-sm-5">
<div class="heading-title">
<h4>Payment Method</h4>
</div>
<!-- CREATE ACCOUNT -->
<div class="toggle-transparent toggle-bordered-full margin-top-30 clearfix">
<div class="toggle active">
<div class="toggle-content">
<div class="clearfix">
<label class="checkbox pull-left">
<input id="accountswitch" name="create-account[yes]" type="checkbox" value="1" />
<i></i> <span class="weight-300">Create an account for later use</span>
</label>
</div>
<!-- CREATE ACCOUNT FORM -->
<div id="newaccount" class="margin-top-10 margin-bottom-30 softhide">
<div class="row nomargin-bottom">
<div id="new_acc">
<div class="col-md-6 col-sm-6">
<label for="account:password">Password *</label>
<input name="blg_password" type="password" value="<?php echo $row['PASSWORD']; ?>" class="form-control" />
</div>
<div class="col-md-6 col-sm-6">
<label for="account:password2">Confirm Password *</label>
<input name="blg_cnfm_password" type="password" value="<?php echo $row['CONFIRM_PASSWORD']; ?>" class="form-control" />
</div>
</div>
<div id="exst_acc"></div>
</div>
<small class="text-warning">NOTE: Email address will be used to login</small>
</div>
<!-- /CREATE ACCOUNT FORM -->
</div>
</div>
</div>
<!-- /CREATE ACCOUNT -->
<!-- PAYMENT METHOD -->
<fieldset class="margin-top-60">
<div class="toggle-transparent toggle-bordered-full clearfix">
<div class="toggle active">
<div class="toggle-content">
<div class="row nomargin-bottom">
<div class="col-lg-12 nomargin clearfix">
<label class="radio pull-left nomargin-top">
<input id="payment_check" name="Payment_Option" type="radio" value="2" />
<i></i> <span class="weight-300">Bank transfer</span>
</label>

<label class="radio pull-left nomargin-top">
<input id="payment_check" name="Payment_Option" type="radio" value="3" />
<i></i> <span class="weight-300">Pay at pickup</span>
</label>
</div>
<div class="col-lg-12 nomargin clearfix">
<label class="radio pull-left">
<input  name="Payment_Option" type="radio" value="1" />
<!---<input id="payment_card" name="payment[method]" type="radio" value="2" />--->
<i></i> <span class="weight-300">Paypal</span>
</label>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<!-- /PAYMENT METHOD -->
<!-- CREDIT CARD PAYMENT -->
<fieldset id="ccPayment" class="margin-top-30 softhide">
<div class="toggle-transparent toggle-bordered-full clearfix">
<div class="toggle active">
<div class="toggle-content">
<div class="row">
<div class="col-lg-12">
<label for="payment:name">Name on Card *</label>
<input id="payment:name" name="crd_name" type="text" value="" class="form-control required" autocomplete="off" />
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label for="payment:name">Credit Card Type *</label>
<select id="payment:state" name="crd_card_type" class="form-control pointer required">
<option value="">Select...</option>
<option value="AE">American Express</option>
<option value="VI">Visa</option>
<option value="MC">Mastercard</option>
<option value="DI">Discover</option>
</select>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label for="payment:cc_number">Credit Card Number *</label>
<input id="payment:cc_number" name="crd_card_no" type="text" value="" class="form-control required" autocomplete="off" />
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label for="payment:cc_exp_month">Card Expiration *</label>
<div class="row nomargin-bottom">
<div class="col-lg-6 col-sm-6">
<select id="payment:cc_exp_month" name="crd_card_exp_month" class="form-control pointer required">
<option value="0">Month</option>
<option value="01">01 - January</option>
<option value="02">02 - February</option>
<option value="03">03 - March</option>
<option value="04">04 - April</option>
<option value="05">05 - May</option>
<option value="06">06 - June</option>
<option value="07">07 - July</option>
<option value="08">08 - August</option>
<option value="09">09 - September</option>
<option value="10">10 - October</option>
<option value="11">11 - November</option>
<option value="12">12 - December</option>
</select>
</div>
<div class="col-lg-6 col-sm-6">
<select id="payment:cc_exp_year" name="crd_card_exp_year" class="form-control pointer required">
<option value="0">Year</option>
<option value="2015">2015</option>
<option value="2016">2016</option>
<option value="2017">2017</option>
<option value="2018">2018</option>
<option value="2019">2019</option>
<option value="2020">2020</option>
<option value="2021">2021</option>
<option value="2022">2022</option>
<option value="2023">2023</option>
<option value="2024">2024</option>
<option value="2025">2025</option>
</select>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label for="payment:cc_cvv">CVV2 *</label>
<input id="payment:cc_cvv" name="crd_card_cvv_no" type="text" value="" class="form-control required" autocomplete="off" maxlength="4" />
</div>
</div>
</div>
</div>
</div>
</fieldset>
<!-- /CREDIT CARD PAYMENT -->
<!-- TOTAL / PLACE ORDER -->
<div class="toggle-transparent toggle-bordered-full clearfix">
<div class="toggle active">
<div class="toggle-content">
<!---<span class="clearfix">
<span class="pull-right">$120.75</span>
<strong class="pull-left">Subtotal:</strong>
</span>
<span class="clearfix">
<span class="pull-right">$0.00</span>
<span class="pull-left">Discount:</span>
</span>
<span class="clearfix">
<span class="pull-right">$8.00</span>
<span class="pull-left">Shipping:</span>
</span>
<hr />
<span class="clearfix">
<span class="pull-right size-20">$128.75</span>
<strong class="pull-left">TOTAL:</strong>
</span>
<hr />-->
<!---<form action='expresscheckout.php' METHOD='POST'>
<input type='image' name='billandship' src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif' border='0' align='top' alt='Check out with PayPal'/>
</form>	--->
<button type="submit" name="billandship" class="btn btn-primary btn-lg btn-block size-15"><i class="fa fa-mail-forward"></i> Place Order Now</button>
</div>
</div>
</div>
<!-- /TOTAL / PLACE ORDER -->
</form>
<!-- /CHECKOUT -->
</div>
</section>
<!-- /CART -->
<script type="text/javascript">
function exist_cstmr(){
var email = $("#id_email").val();  
var pswd = $("#id_pswd").val();
$.ajax({
url:"<?php echo BASEPATH; ?>ajax/exist-cstmr.php",
type:"post",
data:{email:email,pswd:pswd},
success:function(ext_cstm)
{		   
var cstmr=$.parseHTML(ext_cstm); 
$("#exst_cstmr").append(cstmr);
}
});
$.ajax({
url:"<?php echo BASEPATH; ?>ajax/exist-cstmr.php",
type:"post",
data:{email_pwd:email,paswrd:pswd},
success:function(ext_acc)
{		   
var cstmr_pwd=$.parseHTML(ext_acc);     
$("#exst_acc").append(cstmr_pwd);			
}
});
$("#new_cstmr").hide();
$("#new_acc").hide();
}
</script>
<?php include_once('footer.php'); ?>