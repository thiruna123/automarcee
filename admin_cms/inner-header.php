<? session_start();
if(!isset($_SESSION['view']) || $_SESSION['view'] != 1):
?>
	<script type="text/javascript">
		window.location.href="login.php";
	</script>
<?
endif;
require(dirname(__FILE__).'/../class/class.php');
require(dirname(__FILE__).'/../class/config.php');
require(dirname(__FILE__).'/../class/class.phpmailer.php');
function getScriptName($url){
	$name = "Undefined Page"; //default name
	$pattern = "/\/[a-zA-Z0-9\_\-]+\.php/"; //finds file name based on extension
	preg_match($pattern ,$url, $matches);
	
	if(is_array($matches) && !empty($matches[0])){
	$name = $matches[0]; //gets script name
	}
	$name = str_replace("/", "", $name); //replaces first slash
	$ext = pathinfo($name, PATHINFO_EXTENSION);
	$name = basename($name, ".".$ext);
	//$name = preg_replace("/\.(php¦¦html)/", "", $name);//replaces the file extension
	
	return $name; //returns name of script
}
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title><?=getScriptName($_SERVER['PHP_SELF']);?></title>
        <meta name="description" content="Page Description">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="styles/d6220a84.bootstrap.css">


        <!-- Page-specific Plugin CSS: -->
        <link rel="stylesheet" href="styles/vendor/select2/select2.css">
        <link rel="stylesheet" href="styles/vendor/uniformjs/css/uniform.default.css">


        <!-- Proton CSS: -->
        <link rel="stylesheet" href="styles/1b2c4b33.proton.css">
        <link rel="stylesheet" href="styles/vendor/animate.css">

        <!-- adds CSS media query support to IE8   -->
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="scripts/vendor/respond.min.js"></script>
        <![endif]-->

        <!-- Fonts CSS: -->
        <link rel="stylesheet" href="styles/9a41946e.font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="styles/4d9a7458.font-titillium.css" type="text/css" />

        <!-- Common Scripts: -->
        <script src="scripts/jquery.min.js"></script>
        <script src="scripts/vendor/modernizr.js"></script>
        <script src="scripts/vendor/jquery.cookie.js"></script>
    </head>

    <body onload="submit.disabled = true">
        
        <script>
	        var theme = $.cookie('protonTheme') || 'default';
	        $('body').removeClass (function (index, css) {
	            return (css.match (/\btheme-\S+/g) || []).join(' ');
	        });
	        if (theme !== 'default') $('body').addClass(theme);
        </script>
        <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
       <nav class="main-menu">
         <ul>
            <li>
                <a href="dashboard.php">
                    <i class="icon-home nav-icon"></i>
                    <span class="nav-text">
                        Dashboard
                    </span>
                </a>
                </li>
				
				<li>
                <a href="menu.php">
                    <i class="icon-list nav-icon"></i>
                    <span class="nav-text">
                        Header Menu
                    </span>
                </a>
                </li>
				<li>
                <a href="header_content.php">
                    <i class="icon-file-text nav-icon"></i>
                    <span class="nav-text">
                       Header Content
                    </span>
                </a>
                </li>
				<li>
                <a href="home_content.php">
                    <i class="icon-list nav-icon"></i>
                    <span class="nav-text">
                       Home Content
                    </span>
                </a>
                </li>
						
			<li class="has-subnav">
                <a href="cms.php">
                    <i class="icon-laptop nav-icon"></i>
                    <span class="nav-text">
                        Inner page content
                    </span>
                </a>
            </li> 
			<!---<li>
                <a href="team.php">
                     <i class="icon-bar-chart nav-icon"></i>
                    <span class="nav-text">
                      Team
                    </span>
                </a>
            </li>
			<li>
                <a href="client.php">
                     <i class="icon-list nav-icon"></i>
                    <span class="nav-text">
                      Client
                    </span>
                </a>
            </li>
			<li>
                <a href="contact_address.php">
                    <i class="icon-file-text nav-icon"></i>
                    <span class="nav-text">
                      Contact Address
                    </span>
                </a>
            </li>--->
        </ul>

        <ul class="logout">
            <li>
                <a href="logout.php">
                    <i class="icon-off nav-icon"></i>
                    <span class="nav-text">
                        Logout
                    </span>
                </a>
            </li>  
        </ul>
    </nav>
        