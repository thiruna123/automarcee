<? 	require_once('inner-header.php'); 
	error_reporting(0);
	
	
	if(isset($_GET['p_id'])):
		$product=$_GET['p_id'];
	endif;
	
	if(isset($_GET['img_id'])):
		$id=$_GET['img_id'];
		/*read product images*/
		$res_item = $db->fetch_all_array("SELECT * FROM ss_pro_images WHERE IMG_ID=".$id);
	endif;
	
	if(isset($_GET['delete'])):
		$id=$_GET['id'];
		/*delete product images */
		if($id!=0):
			$delete=$db->query("DELETE FROM ss_pro_images WHERE IMG_ID=".$id);
			if($delete):?>
				<script>
				window.location.href="product_images.php?msg=Your Record Successfully deleted...&p_id=<?=$product;?>";
				</script>
			<?else:?>
				<script>
				window.location.href="product_images.php?error=Please Try Aftersometime";
				</script>
			<?endif;
		endif;
	endif;
	
	if(isset($_POST['smt_image'])):
		if(!isset($_GET['img_id'])):
			for($i=0; $i<count($_FILES['files']['name']); $i++): 
				$tmpFilePath = $_FILES['files']['tmp_name'][$i];    
					if ($tmpFilePath != ""):    
						$path = "../images/product_images/";
						$name = $_FILES['files']['name'][$i];
						$size = $_FILES['files']['size'][$i];
						list($txt, $ext) = explode(".", $name);
						$file= time().substr(str_replace(" ", "_", $txt), 0);
						$info = pathinfo($file);
						$filename = $file.".".$ext;
						if(move_uploaded_file($_FILES['files']['tmp_name'][$i], $path.$filename)): 
							$file_name_all.=$filename.",";
						endif;
					endif;
			endfor;
			$filepath = rtrim($file_name_all, ','); //imagepath if it is present    
			$limg = explode(',',$filepath);
			foreach($limg as $img):
				$result=$db->query("INSERT INTO ss_pro_images(PRO_ID, IMG_NAME) VALUES ('".$product."','".$img."')");
			endforeach;
		else:
			/* update images */
			$id=$_GET['img_id'];
			define ("MAX_SIZE","2000");	
				 function getExtension($str) {
						 $i = strrpos($str,".");
						 if (!$i) { return ""; }
						 $l = strlen($str) - $i;
						 $ext = substr($str,$i+1,$l);
						 return $ext;
				 } 
				$errors=0;
				if($_FILES['files']["error"] >= 0){
						 $image=$_FILES['files']['name'];
						if ($image) 
						{
							$filename = stripslashes($_FILES['files']['name']);
							$extension = getExtension($filename);
							$extension = strtolower($extension);

							if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
							{
								echo '<h1>Unknown extension!</h1>';
								$errors=1;
							}
							else
							{
								$size=filesize($_FILES['files']['tmp_name']);
								if ($size > MAX_SIZE*1024)
								{
									echo '<h1>You have exceeded the size limit!</h1>';
									$errors=1;
								}
					
								$image_name=time().'1.'.$extension;
								$newname="../images/product_images/".$image_name;
								$copied = copy($_FILES['files']['tmp_name'], $newname);
					
								if (!$copied) 
								{
									echo '<h1>Copy unsuccessfully!</h1>';
									$errors=1;
								}
							}			
						}
				}	
				$result=$db->query("UPDATE ss_pro_images SET PRO_ID='".$product."' WHERE IMG_ID=".$id);
			if(isset($_FILES['files']['name']) && $_FILES['files']['name'] != '' && $image_name != ''):
				$result=$db->query("UPDATE ss_pro_images SET IMG_NAME='".$image_name."' WHERE IMG_ID=".$id);
			endif;
		endif;
		if($result):?>
        	<script type="text/javascript">
				window.location.href="product_images.php?msg=Your Record Successfully Updated&p_id=<?=$product;?>";
			</script>
        <?else:?>
			<script>
				window.location.href="product_images.php?error=Please Try After Sometime";
			</script>
		<?endif;
	endif;
?>
<script language = "JavaScript" >

    function deleteItem(id) {
        if (confirm("Do your really want to delete your record?"))
        {
            window.location.href= 'product_images.php?p_id=<?=$product;?>&delete=true&id='+id; 
        }
        else
        {
           window.location.href = 'images.php?act=record'; 
        }
    }

</script>
        <section class="sidebar extended">
            <script>
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
                }
            </script>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="clearfix">
                        <img src="images/msas_logo.png" style="width: 220px; height: 85px;" alt="Blessingtv-Logo">
                        <h5>
                            <span class="title">
                                
                            </span>
                            <span class="subtitle">
                                
                            </span>
                        </h5>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="title">
                        <i class="icon-shopping-cart"></i>
                        <span>
                            Images
                        </span>
                        <a href="javascript:;" class="add">
                            <i class="icon-plus-sign"></i>
                            <span>
                                ADD NEW
                            </span>
                        </a>
                    </div>
                    <div class="input-group">
						<div id="proton-tree" class="scrollable"></div>
					</div>
                </div>
                
            </div>
            <div class="sidebar-handle">
                <i class="icon-ellipsis-horizontal"></i>
                <i class="icon-ellipsis-vertical"></i>
            </div>
        </section>

        <section class="wrapper retracted scrollable">
            
            <script>
                if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
                    if ($.cookie('protonSidebar') == 'retracted') {
                        $('.wrapper').removeClass('retracted').addClass('extended');
                    }
                    if ($.cookie('protonSidebar') == 'extended') {
                        $('.wrapper').removeClass('extended').addClass('retracted');
                    }
                }
            </script>
            
            <nav class="user-menu">
                <a href="javascript:;" class="main-menu-access">
                    <i class="icon-Blessingtv-Logo"></i>
                    <i class="icon-reorder"></i>
                </a>
            </nav>
            
            <ol class="breadcrumb breadcrumb-nav">
                <li><a href=".html"><i class="icon-home"></i></a></li>
                <li class="group">
                    <a data-toggle="dropdown" href="#">Images</a>
                </li>
                <li class="active">
                    <a class="bread-page-title" data-toggle="dropdown" href="#"></a>
                    <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
                    </ul>
                </li>
            </ol>
                <div class="panel panel-default panel-block panel-title-block">
                    <div class="panel-heading">
                        <div>
                            <i class="icon-edit"></i>
                            <h1>
                                <span class="page-title"></span>
                                <small>
                                    Create Product Images or Manage the existing Product Images.. 
                                </small>
							</h1>
                            
                        </div>
                    </div>
                </div>
			
			<? if(isset($_GET['msg']) && $_GET['msg']!=''):?>                    
					<div class="alert alert-dismissable alert-success fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> Success</span>
						<?=$_GET['msg'];?>.
					</div>                    
			<? elseif(isset($_GET['error']) && $_GET['error']!=''):?> 
					<div class="alert alert-dismissable alert-danger fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> ERROR</span>
						<?=$_GET['error']?>.
					</div>					
			<? endif; ?> 
			
            <div class="row">
                <div class="col-md-6 col-lg-12">
                                       
                     <form role="form" action="" method="post" enctype="multipart/form-data">
                  <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group" >
                            <div class="list-group-item">
								<h4 class="section-title">Create Images</h4>
								<?if(isset($_GET['img_id'])):?>
								<div class="form-group">
                                    <label for="basic-input">Product Image</label>
                                    <input type="file" name="files" class="form-control" />
									<img src="../images/product_images/<?=$res_item[0]['IMG_NAME']?>" width="50" height="50" />
                                </div>
								<?else:?>
                                <div class="form-group">
                                    <label for="basic-input">Product Images</label>
                                    <input type="file" name="files[]" multiple="multiple" accept="image/*" class="form-control" />
                                </div>
								<?endif;?>
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="smt_image" class="btn btn-success" value="Save Image" />
                                </div>
                                
                            </div>
                        </div>
                    </div>
					</form>

                </div>
            </div>
			
			
			<div class="panel panel-default panel-block" id="images-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title">Images List</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>
						<tr>
							<th>Image ID</th>
							<th>Product Title</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<? $images = $db->fetch_all_array("SELECT pro.IMG_ID,pro.PRO_ID,pro.IMG_NAME,img.IMG_TITLE FROM ss_pro_images pro LEFT JOIN ss_images img on img.IMG_ID=pro.PRO_ID 
						WHERE img.IMG_ID=".$product); 
						foreach($images as $imglist):
						?>
						<tr>
						<td>#<?=$imglist['IMG_ID'];?></td>
						<td><?=$imglist['IMG_TITLE'];?></td>
						<td><img src="../images/product_images/<?=$imglist['IMG_NAME'];?>" width="25" height="25"></td>
						<td><a href="product_images.php?img_id=<?=$imglist['IMG_ID'];?>&p_id=<?=$product;?>">Edit</a>
						/<a onClick="deleteItem(<?=$imglist['IMG_ID'];?>)">Delete</td>
						</tr>
						<?endforeach;?>
					</tbody>
				</table>
			</div>
        </section>

        
        <script src="scripts/9e25e8e2.bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        <script src="scripts/3fa227ae.proton.js"></script>


        <!-- Page-specific scripts: -->
        <script src="scripts/proton/6c42db75.sidebar.js"></script>
        <script src="scripts/proton/7d8c8d18.forms.js"></script>
        <!-- jsTree -->
        <script src="scripts/vendor/jquery.jstree.js"></script>
        <!-- Select2 For Bootstrap3 -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="scripts/vendor/select2.min.js"></script>

        <!-- uniformJs -->
            <script src="scripts/vendor/jquery.uniform.min.js"></script>
		<!-- Date Time Picker -->
        <!-- https://github.com/smalot/bootstrap-datetimepicker -->
        <!-- NOTE: Original JS file is modified: Proton is forcing bootstrap 2 plugin mode in order to support font icons -->
            <script src="scripts/vendor/bootstrap-datetimepicker.js"></script>
        <!-- Character Counter -->
        <!-- http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas -->
            <script src="scripts/vendor/charCount.js"></script>
		<!-- Page-specific scripts: -->
        <script src="scripts/proton/5558cd34.tables.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <script src="scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->	
        <!-- Word Counter -->
        <!-- http://bavotasan.com/2011/simple-textarea-word-counter-jquery-plugin/ -->
            <script src="scripts/vendor/jquery.textareaCounter.js"></script>
        <!-- WYSIWYG Editor -->
        <!-- http://hackerwins.github.io/summernote/ -->
        <!-- JS Modified to use info buttons instead of default buttons for toolbar -->
			<script src="scripts/vendor/summernote.js"></script>
    </body>
</html>
