<? require_once('header.php'); ?>
        
        <section class="wrapper scrollable">
        
            <nav class="user-menu">
                <a href="javascript:;" class="main-menu-access">
                    <i class="icon-Blessingtv-Logo"></i>
                    <i class="icon-reorder"></i>
                </a>

            </nav>
            <section class="title-bar">
                <div>
                    <span>DASHBOARD</span>
                </div>
            </section>
            <nav class="quick-launch-bar">
                <ul>
                    <li>
                        <a href="header_content.php">
                            <i class="icon-home nav-icon"></i>
                            <span>Home page</span>
                        </a>

                    </li>
					<li>
                        <a href="cms.php">
                            <i class="icon-calendar-empty"></i>
                            <span>Inner Page</span>
                        </a>
                    </li>
                </ul>
            </nav>
			
        </section>
        <script src="scripts/9e25e8e2.bootstrap.min.js"></script>
		<!-- Proton base scripts: -->
        <script src="scripts/3fa227ae.proton.js"></script>

        <!-- Page-specific scripts: -->
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="scripts/proton/73f27b75.dashboard.js"></script>
        <script src="scripts/proton/217183f0.dashdemo.js"></script>

        <!-- Bootstrap Tags Input -->
        <!-- http://timschlechter.github.io/bootstrap-tagsinput/examples/ -->
            <script src="scripts/vendor/bootstrap-tagsinput.min.js"></script>

        <!-- Raphael, used for graphs -->
        <!-- http://raphaeljs.com/ -->
            <script src="scripts/vendor/raphael-min.js"></script>
        
        <!-- Morris graphs -->
        <!-- https://github.com/oesmith/morris.js -->
            <script src="scripts/vendor/morris.min.js"></script>

        <!-- Select2 For Bootstrap3 -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="scripts/vendor/select2.min.js"></script>
            
        <!-- Number formating for dashboard demo -->
            <script src="scripts/vendor/numeral.min.js"></script>

        <!-- Notifications -->
        <!-- http://pinesframework.org/pnotify/ -->
            <script src="scripts/vendor/jquery.pnotify.min.js"></script>
			
    </body>
</html>