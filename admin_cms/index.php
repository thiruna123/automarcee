<?
session_start();

if(!isset($_SESSION['view']) || $_SESSION['view'] != 1):
?>
	<script type="text/javascript">
		window.location.href="login.php";
	</script>
<?
else:
?>
<script type="text/javascript">
		window.location.href="dashboard.php";
	</script>
<?
endif;

?>