<? 	require_once('inner-header.php');
	/*For Image upload*/
	//require('../class/ImgUploader.class.php'); 
	error_reporting(0); ?>	
	
        <section class="sidebar extended">
            <script>
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
                }
            </script>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="clearfix">
                        <img src="images/msas_logo.png" style="width: 220px; height: 85px;" alt="Blessingtv-Logo">
                        <h5>
                            <span class="title">
                                
                            </span>
                            <span class="subtitle">
                                
                            </span>
                        </h5>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="title">
                        <i class="icon-shopping-cart"></i>
                        <span>
                            Images
                        </span>
                        <a href="javascript:;" class="add">
                            <i class="icon-plus-sign"></i>
                            <span>
                                ADD NEW
                            </span>
                        </a>
                    </div>
                    <div class="input-group">
                         <div id="proton-tree" class="scrollable"></div>
                    </div>
                </div>
                
            </div>
            <div class="sidebar-handle">
                <i class="icon-ellipsis-horizontal"></i>
                <i class="icon-ellipsis-vertical"></i>
            </div>
        </section>

        <section class="wrapper retracted scrollable">
            
            <script>
                if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
                    if ($.cookie('protonSidebar') == 'retracted') {
                        $('.wrapper').removeClass('retracted').addClass('extended');
                    }
                    if ($.cookie('protonSidebar') == 'extended') {
                        $('.wrapper').removeClass('extended').addClass('retracted');
                    }
                }
            </script>
            
            <nav class="user-menu">
                <a href="javascript:;" class="main-menu-access">
                    <i class="icon-Blessingtv-Logo"></i>
                    <i class="icon-reorder"></i>
                </a>
            </nav>
            
            <ol class="breadcrumb breadcrumb-nav">
                <li><a href="dashboard.php"><i class="icon-home"></i></a></li>
                <li class="group">
                    <a data-toggle="dropdown" href="#">ADMIN</a>
                </li>
                <li class="active">
                    <a class="bread-page-title" data-toggle="dropdown" href="#"></a>
                    <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
                    </ul>
                </li>
            </ol>
                <div class="panel panel-default panel-block panel-title-block">
                    <div class="panel-heading">
                        <div>
                            <i class="icon-edit"></i>
                            <h1>
                                <span class="page-title"></span>
                                <small>
                                    Create and Update footer title and link .. 
                                </small> 
                            </h1>
                            
                        </div>
                    </div>
                </div>
				<? if(isset($_GET['msg']) && $_GET['msg']!=''):?>                    
					<div class="alert alert-dismissable alert-success fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> Success</span>
						<?=$_GET['msg'];?>.
					</div>                    
				<? elseif(isset($_GET['error']) && $_GET['error']!=''):?> 
					<div class="alert alert-dismissable alert-danger fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> ERROR</span>
						<?=$_GET['error']?>.
					</div>					
				<? endif; ?> 
				
				
				<!--=======================-->
			<!---== USEFULL LINKS FIRST COLUMN START =====--->
				<!--=======================-->
				
<?php if(isset($_GET['fc_id'])): $fc_id=$_GET['fc_id']; 
$fc_item = $db->fetch_all_array("SELECT * FROM ss_footer_fc WHERE FC_ID=".$fc_id); endif; 
		
	if(isset($_GET['delete'])):	$fc_id=$_GET['fc_id'];
		if($fc_id!=0):	$delete=$db->query("DELETE FROM ss_footer_fc WHERE FC_ID=".$fc_id);		if($delete):?>
			<script>window.location.href="footer.php?msg=Your Record Successfully deleted...";</script>
			<?else:?>
			<script>window.location.href="footer.php?error=Please Try Aftersometime";</script>
			<?endif;	endif;	endif;
	
	if(isset($_POST['first_column'])):	$fc_title=$_POST['fc_title']; $fc_link=$_POST['fc_link'];		
		
		 if(!isset($_POST['hid'])): 
			/*Insert Usefull Title and link */
			$result=$db->query("INSERT INTO ss_footer_fc(FC_TITLE,FC_LINK) VALUES ('".$fc_title."', '".$fc_link."')");
			?> <script> alert("Insert Succesfully !!"); location = "footer.php"; </script> <?php
		 else:
			/*update Usefull Title and link */
			$fc_id = $_POST['hid'];
			$result=$db->query("UPDATE ss_footer_fc SET FC_TITLE='".$fc_title."', FC_LINK='".$fc_link."' WHERE FC_ID=".$fc_id); 
		endif; ?> <script> alert("Updated Succesfully !!"); location = "footer.php"; </script> <?php 	endif; ?>
<script language = "JavaScript" >
    function fcdeleteItem(fc_id) {
        if (confirm("Do your really want to delete your record?"))
        { window.location.href= 'footer.php?delete=true&fc_id='+fc_id; }
        else {  window.location.href = 'footer.php?act=record';  }
    }
</script>
				
            <div class="row">
                <div class="col-md-6 col-lg-12">                                       
                     <form role="form" action="" method="post" enctype="multipart/form-data">
                  <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group" >
                            <div class="list-group-item">
								<h4 class="section-title">Usefull Links - First Column</h4>                                
								<div class="form-group">
                                    <label for="basic-input">Title</label>
                                    <input id="basic-input" name="fc_title" class="form-control" placeholder="Image Title" 
									value="<? if(isset($fc_item[0]['FC_TITLE'])): echo $fc_item[0]['FC_TITLE']; endif;?>" />
									<? if(isset($fc_item[0]['FC_ID'])): ?>
                                    <input type="hidden" name="hid" value="<?=$fc_item[0]['FC_ID'];?>" />
                                    <? endif; ?>
								</div>								
								<div class="form-group" id="ext_link">
                                    <input id="basic-input" class="form-control" name="fc_link" placeholder="Link URL" value="<? if(isset($fc_item[0]['FC_LINK'])): echo $fc_item[0]['FC_LINK']; endif; ?>" />
                                </div>															
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="first_column" class="btn btn-success" value="Save Titles" />
                                </div>                                
                            </div>
                        </div>
                    </div>
					</form>
                </div>
            </div>
			
			
			<div class="panel panel-default panel-block" id="images-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title"> List of Usefull Links - First Column</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>	<tr><th> ID</th><th> Title</th> <th>Link</th>	<th>Action</th>	</tr></thead>
					<tbody>
						<?  $fc_tb = $db->fetch_all_array("SELECT  * FROM ss_footer_fc "); 
						foreach($fc_tb as $fc_row):
						?>
						<tr class="gradeX">
							<td>#<?=$fc_row['FC_ID'];?></td>
							<td><?=$fc_row['FC_TITLE'];?></td>
							<td><?=$fc_row['FC_LINK'];?></td>
							<td class="center">
								<a href="footer.php?fc_id=<?=$fc_row['FC_ID'];?>" >Edit</a> /
								<a href="#" onClick="fcdeleteItem(<?=$fc_row['FC_ID'];?>)">Delete</a>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			</div>
			  
			  <!--=======================-->
			<!---== USEFULL LINKS FIRST COLUMN END =====--->
				<!--=======================-->
				
				
				<!--=======================-->
			<!---== USEFULL LINKS SECOND COLUMN START =====--->
				<!--=======================-->
				
<?php if(isset($_GET['sc_id'])): $sc_id=$_GET['sc_id']; $sc_item = $db->fetch_all_array("SELECT * FROM ss_footer_sc WHERE SC_ID=".$sc_id); endif; 
		
	if(isset($_GET['delete'])):	$sc_id=$_GET['sc_id'];
		if($sc_id!=0):	$delete=$db->query("DELETE FROM ss_footer_sc WHERE SC_ID=".$sc_id);		if($delete):?>
			<script>window.location.href="footer.php?msg=Your Record Successfully deleted...";</script>
			<?else:?>
			<script>window.location.href="footer.php?error=Please Try Aftersometime";</script>
			<?endif;	endif;	endif;
	
	if(isset($_POST['second_column'])):	$sc_title=$_POST['sc_title']; $sc_link=$_POST['sc_link'];		
		
		 if(!isset($_POST['hid'])): 
			/*Insert Usefull Title and link */
			$result=$db->query("INSERT INTO ss_footer_sc(SC_TITLE,SC_LINK) VALUES ('".$sc_title."', '".$sc_link."')");
			?> <script> alert("Insert Succesfully !!"); location = "footer.php"; </script> <?php
		 else:
			/*update Usefull Title and link */
			$sc_id = $_POST['hid'];
			$result=$db->query("UPDATE ss_footer_sc SET SC_TITLE='".$sc_title."', SC_LINK='".$sc_link."' WHERE SC_ID=".$sc_id); 
		endif; ?> <script> alert("Updated Succesfully !!"); location = "footer.php"; </script> <?php 	endif; ?>
<script language = "JavaScript" >
    function scdeleteItem(sc_id) {
        if (confirm("Do your really want to delete your record?"))
        { window.location.href= 'footer.php?delete=true&sc_id='+sc_id; }
        else {  window.location.href = 'footer.php?act=record';  }
    }
</script>
				
            <div class="row">
                <div class="col-md-6 col-lg-12">                                       
                     <form role="form" action="" method="post" enctype="multipart/form-data">
                  <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group" >
                            <div class="list-group-item">
								<h4 class="section-title">Usefull Links - Second Column</h4>                                
								<div class="form-group">
                                    <label for="basic-input">Title</label>
                                    <input id="basic-input" name="sc_title" class="form-control" placeholder="Image Title" 
									value="<? if(isset($sc_item[0]['SC_TITLE'])): echo $sc_item[0]['SC_TITLE']; endif;?>" />
									<? if(isset($sc_item[0]['SC_ID'])): ?>
                                    <input type="hidden" name="hid" value="<?=$sc_item[0]['SC_ID'];?>" />
                                    <? endif; ?>
								</div>								
								<div class="form-group" id="ext_link">
                                    <input id="basic-input" class="form-control" name="sc_link" placeholder="Link URL" value="<? if(isset($sc_item[0]['SC_LINK'])): echo $sc_item[0]['SC_LINK']; endif; ?>" />
                                </div>															
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="second_column" class="btn btn-success" value="Save Titles" />
                                </div>                                
                            </div>
                        </div>
                    </div>
					</form>
                </div>
            </div>
			
			
			<div class="panel panel-default panel-block" id="images-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title"> List of Usefull Links - First Column</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>	<tr><th> ID</th><th> Title</th> <th>Link</th>	<th>Action</th>	</tr></thead>
					<tbody>
						<?  $sc_tb = $db->fetch_all_array("SELECT  * FROM ss_footer_sc "); 
						foreach($sc_tb as $sc_row):
						?>
						<tr class="gradeX">
							<td>#<?=$sc_row['SC_ID'];?></td>
							<td><?=$sc_row['SC_TITLE'];?></td>
							<td><?=$sc_row['SC_LINK'];?></td>
							<td class="center">
								<a href="footer.php?sc_id=<?=$sc_row['SC_ID'];?>" >Edit</a> /<a href="#" onClick="scdeleteItem(<?=$sc_row['SC_ID'];?>)">Delete</a>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			</div>
			  
			  <!--=======================-->
			<!---== USEFULL LINKS FIRST COLUMN END =====--->
				<!--=======================-->
			
			
			<!--=======================-->
			<!---== FOOTER THIRD COLUMN START =====--->
				<!--=======================-->
				
<?php if(isset($_GET['tc_id'])): $tc_id=$_GET['tc_id']; $tc_item = $db->fetch_all_array("SELECT * FROM ss_footer_tc WHERE TC_ID=".$tc_id); endif; 
		
	if(isset($_GET['delete'])):	$tc_id=$_GET['tc_id'];
		if($tc_id!=0):	$delete=$db->query("DELETE FROM ss_footer_tc WHERE TC_ID=".$tc_id);		if($delete):?>
			<script>window.location.href="footer.php?msg=Your Record Successfully deleted...";</script>
			<?else:?>
			<script>window.location.href="footer.php?error=Please Try Aftersometime";</script>
			<?endif;	endif;	endif;
	
	if(isset($_POST['third_column'])):	$tc_title=$_POST['tc_title']; $tc_value=$_POST['tc_value'];		
		
		 if(!isset($_POST['hid'])): 
			/*Insert Usefull Title and value */
			$result=$db->query("INSERT INTO ss_footer_tc(TC_TITLE,TC_VALUE) VALUES ('".$tc_title."', '".$tc_value."')");
			?> <script> alert("Insert Succesfully !!"); location = "footer.php"; </script> <?php
		 else:
			/*update Usefull Title and value */
			$tc_id = $_POST['hid'];
			$result=$db->query("UPDATE ss_footer_tc SET TC_TITLE='".$tc_title."', TC_VALUE='".$tc_value."' WHERE TC_ID=".$tc_id); 
		endif; ?> <script> alert("Updated Succesfully !!"); location = "footer.php"; </script> <?php 	endif; ?>
<script language = "JavaScript" >
    function tcdeleteItem(tc_id) {
        if (confirm("Do your really want to delete your record?"))
        { window.location.href= 'footer.php?delete=true&tc_id='+tc_id; }
        else {  window.location.href = 'footer.php?act=record';  }
    }
</script>
				
            <div class="row">
                <div class="col-md-6 col-lg-12">                                       
                     <form role="form" action="" method="post" enctype="multipart/form-data">
                  <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group" >
                            <div class="list-group-item">
								<h4 class="section-title">Third Column</h4>                                
								<div class="form-group">
                                    <label for="basic-input">Title</label>
                                    <input id="basic-input" name="tc_title" class="form-control" placeholder="Image Title" 
									value="<? if(isset($tc_item[0]['TC_TITLE'])): echo $tc_item[0]['TC_TITLE']; endif;?>" />
									<? if(isset($tc_item[0]['TC_ID'])): ?>
                                    <input type="hidden" name="hid" value="<?=$tc_item[0]['TC_ID'];?>" />
                                    <? endif; ?>
								</div>								
								<div class="form-group">
                                    <label for="basic-input">VALUE</label>
                                    <input id="basic-input" name="tc_value" class="form-control" placeholder="Value" value="<? if(isset($tc_item[0]['TC_VALUE'])): echo $tc_item[0]['TC_VALUE']; endif;?>" />
                                </div>														
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="third_column" class="btn btn-success" value="Save Titles" />
                                </div>                                
                            </div>
                        </div>
                    </div>
					</form>
                </div>
            </div>
			
			
			<div class="panel panel-default panel-block" id="images-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title"> List of Third Column</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>	<tr><th> ID</th><th>Title</th> <th>Value</th>	<th>Action</th>	</tr></thead>
					<tbody>
						<?  $tc_tb = $db->fetch_all_array("SELECT  * FROM ss_footer_tc "); 
						foreach($tc_tb as $tc_row):
						?>
						<tr class="gradeX">
							<td>#<?=$tc_row['TC_ID'];?></td>
							<td><?=$tc_row['TC_TITLE'];?></td>
							<td><?=$tc_row['TC_VALUE'];?></td>
							<td class="center">
								<a href="footer.php?tc_id=<?=$tc_row['TC_ID'];?>" >Edit</a> /<a href="#" onClick="tcdeleteItem(<?=$tc_row['TC_ID'];?>)">Delete</a>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			</div>
			  
			  <!--=======================-->
			<!---== FOOTER THIRD COLUMN END =====--->
				<!--=======================-->
				
				<!--=======================-->
			<!---== FOOTER FOURTH COLUMN START =====--->
				<!--=======================-->
				
<?php if(isset($_GET['frc_id'])): $frc_id=$_GET['frc_id']; $frc_item = $db->fetch_all_array("SELECT * FROM ss_footer_frc WHERE FRC_ID=".$frc_id); endif; 
		
	if(isset($_GET['delete'])):	$frc_id=$_GET['frc_id'];
		if($frc_id!=0):	$delete=$db->query("DELETE FROM ss_footer_frc WHERE FRC_ID=".$frc_id);		if($delete):?>
			<script>window.location.href="footer.php?msg=Your Record Successfully deleted...";</script>
			<?else:?>
			<script>window.location.href="footer.php?error=Please Try Aftersometime";</script>
			<?endif;	endif;	endif;
	
	if(isset($_POST['fourth_column'])):	$frc_title=$_POST['frc_title']; $frc_address=$_POST['frc_address'];		
		
		 if(!isset($_POST['hid'])): 
			/*Insert Usefull Title and value */
			$result=$db->query("INSERT INTO ss_footer_frc(FRC_TITLE,FRC_ADDRESS) VALUES ('".$frc_title."', '".$frc_address."')");
			?> <script> alert("Insert Succesfully !!"); location = "footer.php"; </script> <?php
		 else:
			/*update Usefull Title and value */
			$frc_id = $_POST['hid'];
			$result=$db->query("UPDATE ss_footer_frc SET FRC_TITLE='".$frc_title."', FRC_ADDRESS='".$frc_address."' WHERE FRC_ID=".$frc_id); 
		endif; ?> <script> alert("Updated Succesfully !!"); location = "footer.php"; </script> <?php 	endif; ?>
<script language = "JavaScript" >
    function frcdeleteItem(frc_id) {
        if (confirm("Do your really want to delete your record?"))
        { window.location.href= 'footer.php?delete=true&frc_id='+frc_id; }
        else {  window.location.href = 'footer.php?act=record';  }
    }
</script>
				
            <div class="row">
                <div class="col-md-6 col-lg-12">                                       
                     <form role="form" action="" method="post" enctype="multipart/form-data">
                  <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group" >
                            <div class="list-group-item">
								<h4 class="section-title">Fourth Column</h4>                                
								<div class="form-group">
                                    <label for="basic-input">Branch</label>
                                    <input id="basic-input" name="frc_title" class="form-control" placeholder=" Title" 
									value="<? if(isset($frc_item[0]['FRC_TITLE'])): echo $frc_item[0]['FRC_TITLE']; endif;?>" />
									<? if(isset($frc_item[0]['FRC_ID'])): ?>
                                    <input type="hidden" name="hid" value="<?=$frc_item[0]['FRC_ID'];?>" />
                                    <? endif; ?>
								</div>								
								<div class="form-group">
                                    <label for="basic-input"> Address</label>
                                     <textarea name="frc_address" rows="6" class="form-control summernote">
										 <? if(isset($frc_item[0]['FRC_ADDRESS'])): echo $frc_item[0]['FRC_ADDRESS']; endif;?>
                                        </textarea>
                                </div>													
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="fourth_column" class="btn btn-success" value="Save Titles" />
                                </div>                                
                            </div>
                        </div>
                    </div>
					</form>
                </div>
            </div>
			
			
			<div class="panel panel-default panel-block" id="images-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title"> List of Third Column</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>	<tr><th> ID</th><th>Title</th> <th>Value</th>	<th>Action</th>	</tr></thead>
					<tbody>
						<?  $frc_tb = $db->fetch_all_array("SELECT  * FROM ss_footer_frc "); 
						foreach($frc_tb as $frc_row):
						?>
						<tr class="gradeX">
							<td>#<?=$frc_row['FRC_ID'];?></td>
							<td><?=$frc_row['FRC_TITLE'];?></td>
							<td><?=$frc_row['FRC_ADDRESS'];?></td>
							<td class="center">
								<a href="footer.php?frc_id=<?=$frc_row['FRC_ID'];?>" >Edit</a> /
								<a href="#" onClick="frcdeleteItem(<?=$frc_row['FRC_ID'];?>)">Delete</a>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			</div>
			  
			  <!--=======================-->
			<!---== FOOTER FOURTH COLUMN END =====--->
				<!--=======================-->
			
			
        </section>
		
		<script src="scripts/9e25e8e2.bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        <script src="scripts/3fa227ae.proton.js"></script>


        <!-- Page-specific scripts: -->
        <script src="scripts/proton/6c42db75.sidebar.js"></script>
        <script src="scripts/proton/7d8c8d18.forms.js"></script>
        <!-- jsTree -->
        <script src="scripts/vendor/jquery.jstree.js"></script>
        <!-- Select2 For Bootstrap3 -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="scripts/vendor/select2.min.js"></script>

        <!-- uniformJs -->
            <script src="scripts/vendor/jquery.uniform.min.js"></script>
		<!-- Date Time Picker -->
        <!-- https://github.com/smalot/bootstrap-datetimepicker -->
        <!-- NOTE: Original JS file is modified: Proton is forcing bootstrap 2 plugin mode in order to support font icons -->
            <script src="scripts/vendor/bootstrap-datetimepicker.js"></script>
        <!-- Character Counter -->
        <!-- http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas -->
            <script src="scripts/vendor/charCount.js"></script>
        <!-- Word Counter -->
        <!-- http://bavotasan.com/2011/simple-textarea-word-counter-jquery-plugin/ -->
            <script src="scripts/vendor/jquery.textareaCounter.js"></script>
		<!-- Page-specific scripts: -->
        <script src="scripts/proton/5558cd34.tables.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <script src="scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
		<!-- WYSIWYG Editor -->
        <!-- http://hackerwins.github.io/summernote/ -->
        <!-- JS Modified to use info buttons instead of default buttons for toolbar -->
		<script src="scripts/vendor/summernote.js"></script>
        <script src="scripts/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({
			selector: "textarea",
			// ===========================================
			// INCLUDE THE PLUGIN
			// ===========================================
			plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
			],
			// ===========================================
			// PUT PLUGIN'S BUTTON on the toolbar
			// ===========================================
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
			image_advtab: true,
			// ===========================================
			// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
			// ===========================================
			relative_urls: false
			});
			</script>
			
    </body>
</html>
