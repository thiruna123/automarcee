<? 	require_once('inner-header.php');
	/*For Image upload*/
	//require('../class/ImgUploader.class.php'); 
	error_reporting(0); ?>	
	
        <section class="sidebar extended">
            <script>
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
                }
            </script>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="clearfix">
                        <img src="images/msas_logo.png" style="width: 220px; height: 85px;" alt="MSAS-Logo">
                        <h5>
                            <span class="title">
                                
                            </span>
                            <span class="subtitle">
                                
                            </span>
                        </h5>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="title">
                        <i class="icon-shopping-cart"></i>
                        <span>
                            Images
                        </span>
                        <a href="javascript:;" class="add">
                            <i class="icon-plus-sign"></i>
                            <span>
                                ADD NEW
                            </span>
                        </a>
                    </div>
                    <div class="input-group">
                         <div id="proton-tree" class="scrollable"></div>
                    </div>
                </div>
                
            </div>
            <div class="sidebar-handle">
                <i class="icon-ellipsis-horizontal"></i>
                <i class="icon-ellipsis-vertical"></i>
            </div>
        </section>

        <section class="wrapper retracted scrollable">
            
            <script>
                if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
                    if ($.cookie('protonSidebar') == 'retracted') {
                        $('.wrapper').removeClass('retracted').addClass('extended');
                    }
                    if ($.cookie('protonSidebar') == 'extended') {
                        $('.wrapper').removeClass('extended').addClass('retracted');
                    }
                }
            </script>
            
            <nav class="user-menu">
                <a href="javascript:;" class="main-menu-access">
                    <i class="icon-Blessingtv-Logo"></i>
                    <i class="icon-reorder"></i>
                </a>
            </nav>
            
            <ol class="breadcrumb breadcrumb-nav">
                <li><a href="dashboard.php"><i class="icon-home"></i></a></li>
                <li class="group">
                    <a data-toggle="dropdown" href="#">ADMIN</a>
                </li>
                <li class="active">
                    <a class="bread-page-title" data-toggle="dropdown" href="#"></a>
                    <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
                    </ul>
                </li>
            </ol>
                <div class="panel panel-default panel-block panel-title-block">
                    <div class="panel-heading">
                        <div>
                            <i class="icon-edit"></i>
                            <h1>
                                <span class="page-title"></span>
                                <small>
                                    Create and Update footer title and link .. 
                                </small> 
                            </h1>
                            
                        </div>
                    </div>
                </div>
				<? if(isset($_GET['msg']) && $_GET['msg']!=''):?>                    
					<div class="alert alert-dismissable alert-success fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> Success</span>
						<?=$_GET['msg'];?>.
					</div>                    
				<? elseif(isset($_GET['error']) && $_GET['error']!=''):?> 
					<div class="alert alert-dismissable alert-danger fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> ERROR</span>
						<?=$_GET['error']?>.
					</div>					
				<? endif; ?> 
				
				
				<!--=======================-->
			<!---== HEADER CONTENT START =====--->
				<!--=======================-->
				
<?php if(isset($_GET['hc_id'])): $hc_id=$_GET['hc_id']; $hc_item = $db->fetch_all_array("SELECT * FROM ms_header_content WHERE HD_ID=".$hc_id); endif; 
		
	if(isset($_POST['header_content'])):
		$hd_marquee=$_POST['hd_marquee']; 
		$hd_contact_line=$_POST['hd_contact_line'];		
				
		
		/* $time = time();
		$img_name = "";
		$img_name_uploaded = $_FILES['img_nam']['name'];
		$img_name_only = $name = pathinfo($img_name_uploaded, PATHINFO_FILENAME);
		$imageData = getimagesize($_FILES['img_nam']['tmp_name']);
		$extension = image_type_to_extension($imageData[2]);
		$img = new imgUploader($_FILES['img_nam']);	
		$img_full_name = $img_name_only.'_'.$time;		
		$full = $img->upload_unscaled('/shinesoft/images/all_images/', $img_full_name);
		if($full):
			$img_name = $img_name_only.'_'.$time;
		else:
			$img_name = "";
			$error_msg =  'ERROR! '.$img->getError();	
		endif;
		
		if($extension == '.jpeg'):
			$extension = '.jpg';
		endif;
		
		$img_name = $img_full_name.$extension;	 */
		define ("MAX_SIZE","2000");	
		 function getExtension($str) {
				 $i = strrpos($str,".");
				 if (!$i) { return ""; }
				 $l = strlen($str) - $i;
				 $ext = substr($str,$i+1,$l);
				 return $ext;
		 } 
		$errors=0;
		if($_FILES['img_nam']["error"] >= 0){
				 $image=$_FILES['img_nam']['name'];
				if ($image) 
				{
					$filename = stripslashes($_FILES['img_nam']['name']);
					$extension = getExtension($filename);
					$extension = strtolower($extension);

					if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
					{
						echo '<h1>Unknown extension!</h1>';
						$errors=1;
					}
					else
					{
						$size=filesize($_FILES['img_nam']['tmp_name']);
						if ($size > MAX_SIZE*1024)
						{
							echo '<h1>You have exceeded the size limit!</h1>';
							$errors=1;
						}
			
						$image_name=time().'1.'.$extension;
						$newname="../images/all_images/".$image_name;
						$copied = copy($_FILES['img_nam']['tmp_name'], $newname);
			
						if (!$copied) 
						{
							echo '<h1>Copy unsuccessfully!</h1>';
							$errors=1;
						}
					}			
				}
		}
		
		 if(!isset($_POST['hid'])): 
			/*create product and banner*/
			$result=$db->query("INSERT INTO header_content(HD_MARQUEE, HD_LOGO, HD_CONTACT_LINE) VALUES ('".$hd_marquee."','".$image_name."', '".$hd_contact_line."')"); 
		 else:
			/*update product and banner*/
			$id = $_POST['hid'];
			$result=$db->query("UPDATE header_content SET HD_MARQUEE='".$hd_marquee."', HD_CONTACT_LINE='".$hd_contact_line."' WHERE HD_ID=".$hc_id); 
			
			if(isset($_FILES['img_nam']['name']) && $_FILES['img_nam']['name'] != '' && $image_name != ''):
				$result = $db->query("UPDATE header_content SET HD_LOGO='".$image_name."' WHERE HD_ID=".$hc_id);
			endif;
		endif; 
		
		if($result):?>
		<script>
			window.location.href="header_content.php?msg=Your Record Successfully Updated";
		</script>
		<?else:?>
			<script>
				window.location.href="header_content.php?error=Please Try After Sometime";
			</script>
		<?endif;
	endif;
?>
				
            <div class="row">
                <div class="col-md-6 col-lg-12">                                       
                     <form role="form" action="" method="post" enctype="multipart/form-data">
                  <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group" >
                            <div class="list-group-item">
								<h4 class="section-title">Header Content</h4>                                
								<div class="form-group">
                                    <label for="basic-input">Title</label>
                                    <input id="basic-input" name="hd_content" class="form-control" placeholder="Header Title" 
									value="<? if(isset($hc_item[0]['HD_DIALOGUE'])): echo $hc_item[0]['HD_DIALOGUE']; endif;?>" />
									<? if(isset($hc_item[0]['HD_ID'])): ?>
                                    <input type="hidden" name="hid" value="<?=$hc_item[0]['HD_ID'];?>" />
                                    <? endif; ?>
								</div>	
                                
								<div class="form-group">
                                    <label for="basic-input">Image</label>&nbsp;&nbsp;&nbsp;<span style="color: red; font-size: 12px;"> Image size should be 250*100 pixels</span>
                                    <input type="file" name="img_nam" class="form-control" value="<? if(isset($hc_item[0]['HD_LOGO'])): echo $hc_item[0]['HD_LOGO']; endif;?>">
									<? if(isset($hc_item[0]['HD_LOGO'])): ?>
										<img src="../images/all_images/<?=$hc_item[0]['HD_LOGO']?>" width="250" height="100" />
									<? endif; ?>
                                </div>
								
								<div class="form-group" id="Praise Number">
								 <label for="basic-input">content</label>
                                    <input id="basic-input" class="form-control" name="content" placeholder="Content" value="<? if(isset($hc_item[0]['CONTENT'])): echo $hc_item[0]['CONTENT']; endif; ?>" />
                                </div>															
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="header_content" class="btn btn-success" value="Save Titles" />
                                </div>                                
                            </div>
                        </div>
                    </div>
					</form>
                </div>
            </div>
			
			
			<div class="panel panel-default panel-block" id="images-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title"> Header Content</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>	<tr><th> Title</th> <th>Image</th> <th>Prayer Line</th>	<th>Action</th>	</tr></thead>
					<tbody>
						<?  $hc_tb = $db->fetch_all_array("SELECT  * FROM header_content "); 
						foreach($hc_tb as $hc_row):
						?>
						<tr class="gradeX">
							
							<td><?=$hc_row['HD_DIALOGUE'];?></td>
							<td><img src="../images/all_images/<?=$hc_row['HD_LOGO'];?>" width="25" height="25"></td>
							<td><?=$hc_row['HD_PRAYER_LINE'];?></td>
							<td class="center">
								<a href="header_content.php?hc_id=<?=$hc_row['HD_ID'];?>" >Edit</a> 
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			</div>
			  
			  <!--=======================-->
			<!---== HEADER CONTENT END =====--->
				<!--=======================-->
				
				
				
			
			
        </section>
		
		<script src="scripts/9e25e8e2.bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        <script src="scripts/3fa227ae.proton.js"></script>


        <!-- Page-specific scripts: -->
        <script src="scripts/proton/6c42db75.sidebar.js"></script>
        <script src="scripts/proton/7d8c8d18.forms.js"></script>
        <!-- jsTree -->
        <script src="scripts/vendor/jquery.jstree.js"></script>
        <!-- Select2 For Bootstrap3 -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="scripts/vendor/select2.min.js"></script>

        <!-- uniformJs -->
            <script src="scripts/vendor/jquery.uniform.min.js"></script>
		<!-- Date Time Picker -->
        <!-- https://github.com/smalot/bootstrap-datetimepicker -->
        <!-- NOTE: Original JS file is modified: Proton is forcing bootstrap 2 plugin mode in order to support font icons -->
            <script src="scripts/vendor/bootstrap-datetimepicker.js"></script>
        <!-- Character Counter -->
        <!-- http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas -->
            <script src="scripts/vendor/charCount.js"></script>
        <!-- Word Counter -->
        <!-- http://bavotasan.com/2011/simple-textarea-word-counter-jquery-plugin/ -->
            <script src="scripts/vendor/jquery.textareaCounter.js"></script>
		<!-- Page-specific scripts: -->
        <script src="scripts/proton/5558cd34.tables.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <script src="scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
		<!-- WYSIWYG Editor -->
        <!-- http://hackerwins.github.io/summernote/ -->
        <!-- JS Modified to use info buttons instead of default buttons for toolbar -->
		<script src="scripts/vendor/summernote.js"></script>
        <script src="scripts/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({
			selector: "textarea",
			// ===========================================
			// INCLUDE THE PLUGIN
			// ===========================================
			plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
			],
			// ===========================================
			// PUT PLUGIN'S BUTTON on the toolbar
			// ===========================================
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
			image_advtab: true,
			// ===========================================
			// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
			// ===========================================
			relative_urls: false
			});
			</script>
    </body>
</html>
