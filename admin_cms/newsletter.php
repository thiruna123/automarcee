<? require_once('inner-header.php');
	error_reporting(0);
	
	/*read news letter*/
	if(isset($_GET['nid']) && $_GET['nid']!=null):
		$nid=$_GET['nid'];
		$res_item = $db->fetch_all_array("SELECT * FROM ss_newsletter WHERE NEWS_ID=".$nid);
	endif;
	
	/*active and inactive*/
	if(isset($_GET['sts_val']) && isset($_GET['news_id'])):
		$sts_val=$_GET['sts_val'];
		$id=$_GET['news_id'];
		if($sts_val==1):
			$sts=0;
		else:
			$sts=1;
		endif;
		
		$res=$db->query("UPDATE ss_newsletter SET NEWS_STS='".$sts."' WHERE NEWS_ID=".$id);
		if($res):?>
			<script>
				window.location.href="newsletter.php?msg=Status Successfully Changed"
			</script>
		<?endif;
	endif;
	
	if(isset($_GET['delete'])):
		$id=$_GET['id'];
		/*delete news letter */
		if($id!=0):
			$delete=$db->query("DELETE FROM ss_newsletter WHERE NEWS_ID=".$id);
			if($delete):?>
				<script>
				window.location.href="newsletter.php?msg=Your Record Successfully deleted...";
				</script>
			<?else:?>
				<script>
				window.location.href="newsletter.php?error=Please Try Aftersometime";
				</script>
			<?endif;
		endif;
	endif;
	if(isset($_POST['smt_news'])):
		if(!$_POST['news_mail']):
			$to = "test12345@gmail.com";
		else:
			$to = $_POST['news_mail'];
		endif;
		
		
		$from = "test12345@gmail.com";
		$from_name = "Shine Soft Technologies";
		
		$subject = "New";
		$message = $_POST['test_desc'];
		$cname = "Shine Soft Customer";
		
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPSecure = 'tls'; 
		$mail->SetFrom($from,$from_name);  
		
		$mail->AddAddress($to);
		$mail->AddReplyTo($from);
		if(isset($_POST['all_mails'])):
			$allmails = $_POST['all_mails'];
			if($allmails == 1):
				$email_ids = $db->fetch_all_array("SELECT NEWS_MAIL FROM ss_newsletter WHERE NEWS_STS=1");
				foreach($email_ids as $email):
					$mail->AddAddress($email['NEWS_MAIL']);
				endforeach;
			endif;
		endif;
		$mail->IsHTML(true);			
		$mail->WordWrap = 50;           
		$mail->Subject  = $subject;
		$mail->Body = "New updates From Shine Soft<br>"; 
		$mail->Body .= $message;
		if($mail->Send()):?>
			<script type="text/javascript">
				window.location.href="newsletter.php?msg=News Letter Send Successfully...";
			</script>
		<?else:?>
			<script type="text/javascript">
				window.location.href="newsletter.php?error=Mail Not Send...";
			</script>
		<?endif;
	endif;
?>
<script language = "JavaScript" >

    function deleteItem(id) {
        if (confirm("Do your really want to delete your record?"))
        {
            window.location.href= 'newsletter.php?delete=true&id='+id; 
        }
        else
        {
           window.location.href = 'newsletter.php?act=record'; 
        }
    }

</script>
        <section class="sidebar extended">
            <script>
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
                }
            </script>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="clearfix">
                        <img src="images/msas_logo.png" style="width: 220px; height: 85px;" alt="Blessingtv-Logo">
                        <h5>
                            <span class="title">
                                
                            </span>
                            <span class="subtitle">
                                
                            </span>
                        </h5>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="title">
                        <i class="icon-shopping-cart"></i>
						<a href="javascript:;" class="add">
                            <i class="icon-plus-sign"></i>
                            <span>
                                ADD NEW
                            </span>
                        </a>
                        <span>
                         NewsLetter
                        </span>
                    </div>
                   <div class="input-group">
                         <div id="proton-tree" class="scrollable"></div>
                    </div>
                </div>
            <div class="sidebar-handle">
                <i class="icon-ellipsis-horizontal"></i>
                <i class="icon-ellipsis-vertical"></i>
            </div>
        </section>

        <section class="wrapper retracted scrollable">
            
            <script>
                if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
                    if ($.cookie('protonSidebar') == 'retracted') {
                        $('.wrapper').removeClass('retracted').addClass('extended');
                    }
                    if ($.cookie('protonSidebar') == 'extended') {
                        $('.wrapper').removeClass('extended').addClass('retracted');
                    }
                }
            </script>
            
            <nav class="user-menu">
                <a href="javascript:;" class="main-menu-access">
                    <i class="icon-Blessingtv-Logo"></i>
                    <i class="icon-reorder"></i>
                </a>
            </nav>
            
            <ol class="breadcrumb breadcrumb-nav">
                <li><a href=".html"><i class="icon-home"></i></a></li>
                <li class="group">
                    <a data-toggle="dropdown" href="#">Newsletter</a>
                </li>
                <li class="active">
                    <a class="bread-page-title" data-toggle="dropdown" href="#"></a>
                    <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
                    </ul>
                </li>
            </ol>
            	
                <div class="panel panel-default panel-block panel-title-block">
                    <div class="panel-heading">
                        <div>
                            <i class="icon-edit"></i>
                            <h1>
                                <span class="page-title"></span>
                                <small>
                                    Manage mails and send newsletter...
                                </small>
                            </h1>
                        </div>
                    </div>
                </div>
				 <? if(isset($_GET['msg']) && $_GET['msg']!=''):?>                    
					<div class="alert alert-dismissable alert-success fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> Success</span>
						<?=$_GET['msg'];?>.
					</div>                    
				<? elseif(isset($_GET['error']) && $_GET['error']!=''):?> 
					<div class="alert alert-dismissable alert-danger fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
						<span class="title"><i class="icon-remove-sign"></i> ERROR</span>
						<?=$_GET['error']?>.
					</div>					
				<? endif; ?> 
                
            <div class="row">
                <div class="col-md-6 col-lg-12">
                                       
                                        <!-- INPUT FIELDS -->
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item">
                            <form action="" method="post" >
								<h4 class="section-title">Create Newsletter</h4>
								<div class="form-group">
                                    <label for="basic-input">Send to all</label>
                                    <input type="checkbox" name="all_mails" id="send_mail_all" class="form-control" value="1" >
                                </div>
								
                                <div class="form-group" id="send_single">
                                    <label for="basic-input">Emails</label>
                                    <input id="basic-input" name="news_mail" class="form-control" placeholder="Emails" 
									value="<?if(isset($res_item[0]['NEWS_MAIL'])): echo $res_item[0]['NEWS_MAIL']; endif;?>" >
                                </div>
                                
                                <div class="form-group">
                                    <h4 class="basic-input">Description</h4>
                                    <div>
                                        <textarea name="test_desc" id="desc" rows="6" class="form-control summernote"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                	<input type="reset" class="btn btn-default" value="Cancel" />
                                    <input type="submit" name="smt_news" class="btn btn-success" value="Send Mail" />
                                </div>
                               </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            
            <div class="panel panel-default panel-block" id="category-list">
				<div id="data-table" class="panel-heading datatable-heading">
					<h4 class="section-title" id="newsletter-list">NewsLetter List</h4>
				</div>
				<table class="table table-bordered table-striped" id="tableSortable">
					<thead>
						<tr>
							<th>Subscription No</th>
							<th>Mail ID</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<? $newsletters = $db->fetch_all_array("SELECT * FROM ss_newsletter"); 
						foreach($newsletters as $news):
						?>
						<tr class="gradeX">
						<td><?='#'.$news['NEWS_ID'];?></td>
							<td><?=$news['NEWS_MAIL'];?></td>
							<td><a href="newsletter.php?news_id=<?=$news['NEWS_ID'];?>&sts_val=<?=$news['NEWS_STS'];?>"><?if($news['NEWS_STS']==1): echo "Active"; else: echo "InActive"; endif;?></td>
							<td class="center"><a href="newsletter.php?nid=<?=$news['NEWS_ID'];?>#Send-Mail">Send Mail</a> / <a onClick="deleteItem(<?=$news['NEWS_ID'];?>)">Delete</a></td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			</div>
        </section>

        
        <script src="scripts/9e25e8e2.bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        <script src="scripts/3fa227ae.proton.js"></script>


        <!-- Page-specific scripts: -->
        <script src="scripts/proton/6c42db75.sidebar.js"></script>
        <script src="scripts/proton/7d8c8d18.forms.js"></script>
        <!-- jsTree -->
        <script src="scripts/vendor/jquery.jstree.js"></script>
        <!-- Select2 For Bootstrap3 -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="scripts/vendor/select2.min.js"></script>

        <!-- uniformJs -->
            <script src="scripts/vendor/jquery.uniform.min.js"></script>

        <!-- Date Time Picker -->
        <!-- https://github.com/smalot/bootstrap-datetimepicker -->
        <!-- NOTE: Original JS file is modified: Proton is forcing bootstrap 2 plugin mode in order to support font icons -->
            <script src="scripts/vendor/bootstrap-datetimepicker.js"></script>
        <!-- Character Counter -->
        <!-- http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas -->
            <script src="scripts/vendor/charCount.js"></script>
        <!-- Word Counter -->
        <!-- http://bavotasan.com/2011/simple-textarea-word-counter-jquery-plugin/ -->
            <script src="scripts/vendor/jquery.textareaCounter.js"></script>
        <!-- WYSIWYG Editor -->
		<!-- Page-specific scripts: -->
        <script src="scripts/proton/5558cd34.tables.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <script src="scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- http://hackerwins.github.io/summernote/ -->
        <!-- JS Modified to use info buttons instead of default buttons for toolbar -->
            <script src="scripts/vendor/summernote.js"></script>
			<script src="scripts/tinymce/tinymce.min.js"></script>
			<script>
			$(document).ready(function(){
				$('#send_mail_all').change(function(){
					if(this.checked)
						$('#send_single').fadeOut('slow');
					else
						$('#send_single').fadeIn('slow');
				});
			});	
			</script>
			<script>
			tinymce.init({
			selector: "textarea",
			// ===========================================
			// INCLUDE THE PLUGIN
			// ===========================================
			plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
			],
			// ===========================================
			// PUT PLUGIN'S BUTTON on the toolbar
			// ===========================================
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
			image_advtab: true,
			// ===========================================
			// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
			// ===========================================
			relative_urls: false
			});
			</script>
    </body>
</html>
