<?php error_reporting(0);
include_once('config.php');
include_once('login_submit.php');


?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>MSAS-AUTO SALES</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/dropzone.css" />
      <script type="text/javascript" src="js/dropzone.js"></script>
  
<!-- ========== Html-Editor ========== -->
  <script type="text/javascript" src="html_editor/ckeditor.js"></script>
  </head>
  <body>
  <!-- Start Page Loading -->
  <!--<div class="loading"><img src="img/loading.gif" alt="loading-img"></div>
   End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
  <!-- START TOP -->
  <div id="top" class="clearfix">

    <!-- Start App Logo -->
    <div class="applogo">
      <a href="http://www.automarce.com" target="_blank" class="logo"><img class="logo-align" src="img/logo.png"/></a>
    </div>
    <!-- End App Logo -->

    <!-- Start Sidebar Show Hide Button -->
    <!---<a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>-->
    <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
    <!-- End Sidebar Show Hide Button -->

    <!-- Start Searchbox -->
    <!---<form class="searchform">
      <input type="text" class="searchbox" id="searchbox" placeholder="Search">
      <span class="searchbutton"><i class="fa fa-search"></i></span>
    </form>-->
    <!-- End Searchbox -->

    <!-- Start Top Menu -->
    <!---<ul class="topmenu">
      <li><a href="#">Files</a></li>
      <li><a href="#">Authors</a></li>
      <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">My Files <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Videos</a></li>
          <li><a href="#">Pictures</a></li>
          <li><a href="#">Blog Posts</a></li>
        </ul>
      </li>
    </ul>--->
    <!-- End Top Menu -->

    <!-- Start Sidepanel Show-Hide Button -->
    <!----<a href="#sidepanel" class="sidepanel-open-button"><i class="fa fa-outdent"></i></a>--->
    <!-- End Sidepanel Show-Hide Button -->

    <!-- Start Top Right -->
    <ul class="top-right">

   <!--- <li class="dropdown link">
      <a href="#" data-toggle="dropdown" class="dropdown-toggle hdbutton">Create New <span class="caret"></span></a>
        <ul class="dropdown-menu dropdown-menu-list">
          <li><a href="#"><i class="fa falist fa-paper-plane-o"></i>Product or Item</a></li>
          <li><a href="#"><i class="fa falist fa-font"></i>Blog Post</a></li>
          <li><a href="#"><i class="fa falist fa-file-image-o"></i>Image Gallery</a></li>
          <li><a href="#"><i class="fa falist fa-file-video-o"></i>Video Gallery</a></li>
        </ul>
    </li>-->

    <!---<li class="link">
      <a href="#" class="notifications">6</a>
    </li>--->

    <li class="dropdown link">
      <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><img src="img/profileimg.png" alt="img"><b><? echo $_SESSION['admin_name'];?></b><span class="caret"></span></a>
        <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">

          <li><a href="logout.php"><i class="fa falist fa-power-off"></i> Logout</a></li>
        </ul>
    </li>
    </ul>
    <!-- End Top Right -->
  </div>
  <!-- END TOP -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START SIDEBAR -->
<div class="sidebar clearfix">

<ul class="sidebar-panel nav">
 <!--- <li class="sidetitle">MAIN</li>-->
  <li><a href="index.php?file_name=dashboard"><span class="icon color5"><i class="fa fa-home"></i></span>Dashboard<!--<span class="label label-default">2</span>---></a></li>
  <!---<li><a href="mailbox.php"><span class="icon color6"><i class="fa fa-envelope-o"></i></span>Mailbox<span class="label label-default">19</span></a></li>-->
  <li><a href="#"><span class="icon color7"><i class="fa fa-flask"></i></span>Catalog<span class="caret"></span></a>
    <ul>
     <!--- <li><a href="icons.php">Icons</a></li>
      <li><a href="tabs.php">Tabs</a></li>
      <li><a href="buttons.php">Buttons</a></li>
      <li><a href="panels.php">Panels</a></li>
      <li><a href="notifications.php">Notifications</a></li>---->
	  <li><a href="index.php?file_name=product-list">Products</a></li>
      <li><a href="index.php?file_name=ctgry-list">Category</a></li>
	  <!--<li><a href="monitoring.php">Monitoring</a></li>-->
	  <li><a href="index.php?file_name=prdct-attribute-list">Product-Attributes</a></li>
	 
	  
	  <!---<li><a href="#">Product-Features</a></li>
	   <li><a href="#">Manufacturers</a></li>
	    <li><a href="#">Suppliers</a></li>
		 <li><a href="#">Tags</a></li>
		  <li><a href="#">Attachments</a></li>
      <li><a href="progress-bars.php">Progress Bars</a></li>
      <li><a href="others.php">Others<span class="label label-danger">NEW</span></a></li>--->
    </ul>
  </li>
  
 <li><a href="#"><span class="icon color9"><i class="fa fa-th"></i></span>Orders<span class="caret"></span></a>
    <ul>
      <li><a href="index.php?file_name=order-list">Orders-List</a></li>
      <!---<li><a href="order-add-new.php">Orders-Add</a></li>--->
	  <li><a href="index.php?file_name=order-invoice">Orders-Invoice</a></li>
    </ul>
  </li>
  
 <!-- <li><a href="#"><span class="icon color10"><i class="fa fa-check-square-o"></i></span>Customers<span class="caret"></span></a>
    <ul>
      <li><a href="customer-list.php">Customers-List</a></li>
      <li><a href="address-list.php">Address</a></li>
      <li><a href="shopping-carts.php">Shopping carts</a></li>
    </ul>
  </li>
  
  <li><a href="#"><span class="icon color9"><i class="fa fa-th"></i></span>Shipping<span class="caret"></span></a>
    <ul>
      <li><a href="carriers-list.php">Carriers</a></li>
      <li><a href="#">Preference</a></li>
    </ul>
  </li>--->
  
  <li><a href="#"><span class="icon color10"><i class="fa fa-check-square-o"></i></span>Administration<span class="caret"></span></a>
    <ul>
	 <li><a href="index.php?file_name=admin-request">Admin Request</a></li>
	 <li><a href="index.php?file_name=admin-approval">Admin Approval</a></li>
      <!---<li><a href="employees-list.php">Employee</a></li>
      <li><a href="profile-list.php">Profile</a></li>
      <li><a href="permission.php">Permission</a></li>--->
    </ul>
  </li>
 
  <li><a href="#"><span class="icon color7"><i class="fa fa-flask"></i></span>Customer<span class="caret"></span></a>
    <ul>
	<li><a href="index.php?file_name=customer-details">Customer Details</a></li>
        
      <!---<li><a href="cart-rules.php">Cart rules</a></li>
      <li><a href="catalog-price-rules.php">Catalog price rules</a></li>-->
    </ul>
  </li>
    <li><a href="index.php?file_name=social-user"><span class="icon color7"><i class="fa  fa-male"></i></span>Social Link</a>
      <!---<li><a href="cart-rules.php">Cart rules</a></li>
      <li><a href="catalog-price-rules.php">Catalog price rules</a></li>-->
  
  </li>
  <li><a href="#"><span class="icon color9"><i class="fa fa-th"></i></span>Price rules<span class="caret"></span></a>
    <ul>
	<li><a href="index.php?file_name=currency-list">Currency</a></li>
    <li><a href="index.php?file_name=edit-climate">Manage Port</a></li>
      <!---<li><a href="cart-rules.php">Cart rules</a></li>
      <li><a href="catalog-price-rules.php">Catalog price rules</a></li>-->
    </ul>
  </li>
  
  <!--- <li><a href="charts.php"><span class="icon color8"><i class="fa fa-bar-chart"></i></span>Charts</a></li>  
  <li><a href="widgets.php"><span class="icon color11"><i class="fa fa-diamond"></i></span>Widgets</a></li>
  <li><a href="calendar.php"><span class="icon color8"><i class="fa fa-calendar-o"></i></span>Calendar<span class="label label-danger">NEW</span></a></li>
  <li><a href="typography.php"><span class="icon color12"><i class="fa fa-font"></i></span>Typography</a></li>
  <li><a href="#"><span class="icon color14"><i class="fa fa-paper-plane-o"></i></span>Extra Pages<span class="caret"></span></a>
    <ul>
      <li><a href="social-profile.php">Social Profile</a></li>
      <li><a href="invoice.php">Invoice<span class="label label-danger">NEW</span></a></li>
      <li><a href="login.php">Login Page</a></li>
      <li><a href="register.php">Register</a></li>
      <li><a href="forgot-password.php">Forgot Password</a></li>
      <li><a href="lockscreen.php">Lockscreen</a></li>
      <li><a href="blank.php">Blank Page</a></li>
      <li><a href="contact.php">Contact</a></li>
      <li><a href="404.php">404 Page</a></li>
      <li><a href="500.php">500 Page</a></li>
    </ul>
  </li>--->
</ul>

<!---<ul class="sidebar-panel nav">
  <li class="sidetitle">MORE</li>
  <li><a href="grid.php"><span class="icon color15"><i class="fa fa-columns"></i></span>Grid System</a></li>
  <li><a href="maps.php"><span class="icon color7"><i class="fa fa-map-marker"></i></span>Maps</a></li>
  <li><a href="customizable.php"><span class="icon color10"><i class="fa fa-lightbulb-o"></i></span>Customizable</a></li>
  <li><a href="helper-classes.php"><span class="icon color8"><i class="fa fa-code"></i></span>Helper Classes</a></li>
  <li><a href="changelogs.php"><span class="icon color12"><i class="fa fa-file-text-o"></i></span>Changelogs</a></li>
</ul>--->

<!---<div class="sidebar-plan">
  Pro Plan<a href="#" class="link">Upgrade</a>
  <div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
  </div>
</div>
<span class="space">42 GB / 100 GB</span>
</div>--->

</div>
<!-- END SIDEBAR -->
<!-- //////////////////////////////////////////////////////////////////////////// --> 
