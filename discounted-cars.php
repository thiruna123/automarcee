<?php 
include_once('header.php'); 
$discounted_cars_view = $_GET['discounted_cars_view'] + 3;
?>	
<script>
$().ready(function(){
$("#discount_car_id").validate(); 
});	
</script>
<!-- REVOLUTION SLIDER -->
			<section id="slider" class="slider fullwidthbanner-container roundedcorners">
				
				<div class="fullwidthbanner" data-height="550" data-navigationStyle="">
					<ul class="hide">

						<!-- SLIDE  -->
						<li data-transition="random" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">

							<img data-lazyload="<?php echo BASEPATH; ?>assets/img/slider1.png" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />

							<div class="tp-caption customin ltl tp-resizeme large_bold_black"
								data-x="center"
								data-y="110"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1200"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								Buy Your New Car on Auto Dealer
							</div>

							<div class="tp-caption customin ltl tp-resizeme small_light_red"
								data-x="center"
								data-y="180"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1400"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center;">
								Everything you need to Build an Amazing Dealership
							</div>

							<div class="tp-caption customin ltl tp-resizeme"
								data-x="center"
								data-y="313"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1550"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								<a href="#purchase" class="btn btn-default btn-lg">
									<span>View Now &raquo;</span> 
								</a>
							</div>

						</li>

<!-- SLIDE  -->
						<li data-transition="random" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">

							<img data-lazyload="<?php echo BASEPATH; ?>assets/img/slider1.png" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />

							<div class="tp-caption customin ltl tp-resizeme large_bold_black"
								data-x="center"
								data-y="110"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1200"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								Buy Your New Car on Auto Dealer
							</div>

							<div class="tp-caption customin ltl tp-resizeme small_light_red"
								data-x="center"
								data-y="180"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1400"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center;">
								Everything you need to Build an Amazing Dealership
							</div>

							<div class="tp-caption customin ltl tp-resizeme"
								data-x="center"
								data-y="313"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1550"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								<a href="#purchase" class="btn btn-default btn-lg">
									<span>View Now &raquo;</span> 
								</a>
							</div>

						</li>
                        
<!-- SLIDE  -->
						<li data-transition="random" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">

							<img data-lazyload="<?php echo BASEPATH; ?>assets/img/slider1.png" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />

							<div class="tp-caption customin ltl tp-resizeme large_bold_black"
								data-x="center"
								data-y="110"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1200"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								Buy Your New Car on Auto Dealer
							</div>

							<div class="tp-caption customin ltl tp-resizeme small_light_red"
								data-x="center"
								data-y="180"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1400"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10; width: 100%; max-width: 750px; white-space: normal; text-align:center;">
								Everything you need to Build an Amazing Dealership
							</div>

							<div class="tp-caption customin ltl tp-resizeme"
								data-x="center"
								data-y="313"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1550"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								<a href="#purchase" class="btn btn-default btn-lg">
									<span>View Now &raquo;</span> 
								</a>
							</div>

						</li>                        
						

					</ul>
				</div>
			</section>
			<!-- /REVOLUTION SLIDER -->

            <!-- INFO BAR -->
			<section class="info-bar">
				<div class="container">
                    
                <div class="row">

                <div class="col-sm-12"><h3 class="uppercase text-center"> Choose Your Dream Car</h3> </div>
            </div>    

					<div class="row">

<form id="discount_car_id" name="srch_car" method="post">
<div class="col-sm-2">
<select class="form-control cndtn_cls" name="condition" id="cndtn_id" onchange="chk_cndtn(this);" onmouseover="remove_duplicate_condition();" required>
<option value="">Condition</option>
<?php
$cbntn_tb = mysql_query("SELECT t1.ATTR_CMBNTN FROM prdct_attr_cmbntn t1 LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID WHERE t2.CTGRY_NAME = 'Discounted Cars'") or die("Error in sprt the attribute selecting query !!!");	
while($cbntn_rw = mysql_fetch_assoc($cbntn_tb))
{
if(isset($cbntn_rw['ATTR_CMBNTN']))
{ 
$sprt_attr = explode(',',$cbntn_rw['ATTR_CMBNTN']);
$sprt_attr_cnt = count($sprt_attr);
$attr_val = 0; 
while($attr_val<$sprt_attr_cnt)
{
if(isset($sprt_attr[$attr_val]))
{ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]);
$sprt_atrval_cnt = count($sprt_atrval);
$atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{ 
      $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Condition'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
     <option value="<?php echo $val_rwl['ATTR_ID'].":".$val_rwl['ATTR_VAL_ID']; ?>"><?php echo $val_rwl['ATTR_VAL']; ?></option> 
<?php }	 $atrval_val++; } } $attr_val++; } } }  ?> 					
</select>
</div>

<div class="col-sm-2" id="branddiv_id">
<select class="form-control brand_class" name="brand" id="brand_id" onchange="chk_brand(this);" onmouseover="remove_duplicate_brand();" required>
<option value="">Brand</option>
<?php
$cbntn_tb = mysql_query("SELECT t1.ATTR_CMBNTN FROM prdct_attr_cmbntn t1 LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID WHERE t2.CTGRY_NAME = 'Discounted Cars'") or die("Error in sprt the attribute selecting query !!!");	
while($cbntn_rw = mysql_fetch_assoc($cbntn_tb))
{
if(isset($cbntn_rw['ATTR_CMBNTN']))
{ 
$sprt_attr = explode(',',$cbntn_rw['ATTR_CMBNTN']);
$sprt_attr_cnt = count($sprt_attr);
$attr_val = 0; 
while($attr_val<$sprt_attr_cnt)
{
if(isset($sprt_attr[$attr_val]))
{ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]);
$sprt_atrval_cnt = count($sprt_atrval);
$atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{ 
      $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Brand'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
     <option value="<?php echo $val_rwl['ATTR_ID'].":".$val_rwl['ATTR_VAL_ID']; ?>"><?php echo $val_rwl['ATTR_VAL']; ?></option> 
<?php } $atrval_val++; } } $attr_val++; } } } ?>

</select>
 </div>

<div class="col-sm-2" id="modeldiv_id">
<select class="form-control model_cls" name="model"  id="model_id" onchange="chk_model(this);" onmouseover="remove_duplicate_model();" required>
<option value="">Model</option>
<?php
$cbntn_tb = mysql_query("SELECT t1.ATTR_CMBNTN FROM prdct_attr_cmbntn t1 LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID WHERE t2.CTGRY_NAME = 'Discounted Cars'") or die("Error in sprt the attribute selecting query !!!");	
while($cbntn_rw = mysql_fetch_assoc($cbntn_tb))
{
if(isset($cbntn_rw['ATTR_CMBNTN']))
{ 
$sprt_attr = explode(',',$cbntn_rw['ATTR_CMBNTN']);
$sprt_attr_cnt = count($sprt_attr);
$attr_val = 0; 
while($attr_val<$sprt_attr_cnt)
{
if(isset($sprt_attr[$attr_val]))
{ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]);
$sprt_atrval_cnt = count($sprt_atrval);
$atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{ 
      $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Model'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
     <option value="<?php echo $val_rwl['ATTR_ID'].":".$val_rwl['ATTR_VAL_ID']; ?>"><?php echo $val_rwl['ATTR_VAL']; ?></option> 
<?php }	 $atrval_val++; } } $attr_val++; } } } ?>

</select>
	</div>
				
<div class="col-sm-2" id="geardiv_id">
<select class="form-control gear_cls" name="geartype" id="gear_id" onchange="chk_gear(this);" onmouseover="remove_duplicate_gear();" required>
<option value="">Gear type</option>
<?php
$cbntn_tb = mysql_query("SELECT t1.ATTR_CMBNTN FROM prdct_attr_cmbntn t1 LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID WHERE t2.CTGRY_NAME = 'Discounted Cars'") or die("Error in sprt the attribute selecting query !!!");	
while($cbntn_rw = mysql_fetch_assoc($cbntn_tb))
{
if(isset($cbntn_rw['ATTR_CMBNTN']))
{ 
$sprt_attr = explode(',',$cbntn_rw['ATTR_CMBNTN']);
$sprt_attr_cnt = count($sprt_attr);
$attr_val = 0; 
while($attr_val<$sprt_attr_cnt)
{
if(isset($sprt_attr[$attr_val]))
{ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]);
$sprt_atrval_cnt = count($sprt_atrval);
$atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{ 
      $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Gear type'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
     <option value="<?php echo $val_rwl['ATTR_ID'].":".$val_rwl['ATTR_VAL_ID']; ?>"><?php echo $val_rwl['ATTR_VAL']; ?></option> 
<?php }	 $atrval_val++; } } $attr_val++; } } } ?>

</select>
 </div>
				
<div class="col-sm-2" id="bodydiv_id"> 
<select class="form-control" name="bodytype" id="body_id" onmouseover="remove_duplicate_body();" required>
<option value="">Body type</option>
<?php
$cbntn_tb = mysql_query("SELECT t1.ATTR_CMBNTN FROM prdct_attr_cmbntn t1 
LEFT JOIN prdct_in_ctgry t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
WHERE t2.CTGRY_NAME = 'Discounted Cars'") or die("Error in sprt the attribute selecting query !!!");	
while($cbntn_rw = mysql_fetch_assoc($cbntn_tb))
{
if(isset($cbntn_rw['ATTR_CMBNTN']))
{ 
$sprt_attr = explode(',',$cbntn_rw['ATTR_CMBNTN']);
$sprt_attr_cnt = count($sprt_attr);
$attr_val = 0; 
while($attr_val<$sprt_attr_cnt)
{
if(isset($sprt_attr[$attr_val]))
{ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]);
$sprt_atrval_cnt = count($sprt_atrval);
$atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{ 
      $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Body type'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
     <option value="<?php echo $val_rwl['ATTR_ID'].":".$val_rwl['ATTR_VAL_ID']; ?>"><?php echo $val_rwl['ATTR_VAL']; ?></option> 
<?php }	 $atrval_val++; } } $attr_val++; } } } ?>

</select>
</div>
				
<div class="col-sm-2">
<button type="submit" name="send_car_info" class="btn btn-primary"><i class="fa fa-search"></i> Search Cars</button>
</div>
</form>
</div>

				</div>
			</section>
			<!-- /INFO BAR -->

			<!-- DISCOUNTED CARS -->
			<section>
				<div class="container">

                    <h1 class="text-center latest-head"><strong>Discounted Cars</strong></h1>
					<hr style="margin-top: -25px;"/>	
                    <!-- item -->
<?php 
if(isset($_POST['send_car_info'])): 
$condition = $_POST['condition'];
$brand = $_POST['brand'];
$model = $_POST['model'];
$geartype = $_POST['geartype'];
$bodytype = $_POST['bodytype'];
$needed = array('condition','brand','model','geartype','bodytype');
foreach ($_POST as $key=>$data)
{
if($data!="" && in_array($key,$needed))
{ $str .=" '$data',"; }
}
$condition= rtrim($str,",");
//echo "SELECT * FROM prdct_attr_cmbntn WHERE $condition"; die();
$cars_tb = mysql_query("SELECT * FROM prdct_attr_cmbntn WHERE ATTR_CMBNTN IN ($condition) GROUP BY PRDCT_ID") or die("Error in car search select query !!!");
while($cars_rw = mysql_fetch_assoc($cars_tb)):
$ctgry_tb1 = mysql_query("SELECT * FROM category WHERE MAIN_CTGRY = 'Discounted Cars'") or die('Error in category selecting query !!!');
while($ctgry_rw1 = mysql_fetch_assoc($ctgry_tb1))
{ 
if(!empty($ctgry_rw1['CTGRY_ID']))
{

$prdct_tb = mysql_query(
"SELECT 
t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT, 
t4.ATTR_CMBNTN, t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL FROM prdct_add t1 
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() 
AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t1.PRDCT_ID = '$cars_rw[PRDCT_ID]' AND 
t5.CTGRY_ID = '$ctgry_rw1[CTGRY_ID]' AND t5.CTGRY_NAME = 'Discounted Cars'
GROUP BY t1.PRDCT_ID ORDER BY t1.PRDCT_ID DESC limit $discounted_cars_view") or die("Error in product listing selecting query !!!"); 
$i=1; while($prdct_rw = mysql_fetch_assoc($prdct_tb)):
?>
<div class="col-sm-4 shop-item margin-bottom-30">
<div class="thumbnail">
 <div class="float-right shop-item-info">
 <span class="label label-danger">Now : $
  <?php 
if($prdct_rw['DISCOUNT_TYPE']==1)
{
if(isset($_SESSION['CURRENCY_VALUE']))
{ 
$multi = $prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; 
echo $_SESSION['CURRENCY_NAME']; echo '-'; echo $multi - $prdct_rw['DISCOUNT_PRICE'];
}
elseif(isset($prdct_rw['TOTAL_PRICE']))
{ 
echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE'] - $prdct_rw['DISCOUNT_PRICE'];
} 
} 
?></span>
								</div>								<!-- product image(s) -->
<a class="shop-item-image" href="#">
<img class="img-responsive" src="<?php echo BASEPATH.'admin/product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>" alt="Honda CR V" />
<!---<img class="img-responsive" src="assets/img/honda-crv.png" alt="Honda CR v" />--->
</a>
								<!-- /product image(s) -->
<div class="shop-item-summary text-center">
<h2 class="product-name uppercase"><?php echo $prdct_rw['PRDCT_NAME']; ?></h2>
<h2 class="product-desc"><?php echo substr($prdct_rw['SHORT_DESC'], 0, 80). '...'; ?> </h2>
								<!-- price -->
<div class="shop-item-details">
<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Year'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php } $atrval_val++; } } $attr_val++; } } ?>

<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      	  
      if($atr_rwl['ATTR_NAME']=='Fuel'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php }  $atrval_val++; } } $attr_val++; } } ?> 

<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
     	  
      if($atr_rwl['ATTR_NAME']=='Model'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php }  $atrval_val++; } } $attr_val++; } } ?> 

<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='KMs driven'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php }  $atrval_val++; } } $attr_val++; } } ?> 	  
</div>
								<!-- /price -->
</div>

								<!-- buttons -->
<div class="shop-item-buttons text-center">
<a class="btn btn-default was-now color-white font-roboto weight-500" 
href="<?php echo BASEPATH; ?>buycars/<?php echo $prdct_rw['SEO_URL']; ?>"><span class="color-red border-red margin-right-10">Was</span> 
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'.$prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; }elseif(isset($prdct_rw['TOTAL_PRICE'])){ echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE']; } ?>
 <i class="padding-left-30 fa fa-arrow-right"></i></a>
</div>

								<!-- /buttons -->    
</div>
</div>

<?php
endwhile; } } endwhile; else:
$ctgry_tb1 = mysql_query("SELECT * FROM category WHERE MAIN_CTGRY = 'Discounted Cars'") or die('Error in category selecting query !!!');
while($ctgry_rw1 = mysql_fetch_assoc($ctgry_tb1))
{ 
if(!empty($ctgry_rw1['CTGRY_ID']))
{
$prdct_tb = mysql_query(
"SELECT 
t1.PRDCT_ID, t1.PRDCT_NAME, t1.SHORT_DESC, 
t2.PRDCT_THUMBNAIL, t3.TOTAL_PRICE, t3.DISCOUNT_TYPE, t3.DISCOUNT_PRICE, t3.DISCOUNT_PRODUCT, t4.ATTR_CMBNTN, 
t5.PRDCT_ID, t5.CTGRY_NAME, t6.PRDCT_ID, t6.SEO_URL FROM prdct_add t1 
LEFT JOIN prdct_image t2 ON t1.PRDCT_ID = t2.PRDCT_ID 
LEFT JOIN prdct_price t3 ON t1.PRDCT_ID = t3.PRDCT_ID 
LEFT JOIN prdct_attr_cmbntn t4 ON t1.PRDCT_ID = t4.PRDCT_ID
LEFT JOIN prdct_in_ctgry t5 ON t1.PRDCT_ID = t5.PRDCT_ID
LEFT JOIN prdct_seo t6 ON t1.PRDCT_ID = t6.PRDCT_ID
WHERE (CURDATE() BETWEEN t1.FROM_DATE AND t1.TO_DATE) AND t1.DISPLAY_DATE <= CURDATE() 
AND t1.ENABLE = '1' AND t1.SOLD != 'Yes' AND t5.CTGRY_ID = '$ctgry_rw1[CTGRY_ID]' AND 
t5.CTGRY_NAME = 'Discounted Cars'
GROUP BY t1.PRDCT_ID ORDER BY t1.PRDCT_ID DESC limit $discounted_cars_view") or die("Error in product listing selecting query !!!"); 
while($prdct_rw = mysql_fetch_assoc($prdct_tb)): $prdct_rw['PRDCT_ID'];
?>

<div class="col-sm-4 shop-item margin-bottom-30">
<div class="thumbnail">
 <div class="float-right shop-item-info">
 <span class="label label-danger">Now : 
 <?php 
if($prdct_rw['DISCOUNT_TYPE']==1)
{
if(isset($_SESSION['CURRENCY_VALUE']))
{ 
$multi = $prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; 
echo $_SESSION['CURRENCY_NAME']; echo '-'; echo $multi - $prdct_rw['DISCOUNT_PRICE'];
}
elseif(isset($prdct_rw['TOTAL_PRICE']))
{ 
echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE'] - $prdct_rw['DISCOUNT_PRICE'];
} 
} 
?></span>
								</div>								<!-- product image(s) -->
<a class="shop-item-image" href="#">
<img class="img-responsive" src="<?php echo BASEPATH.'admin/product_image/'.$prdct_rw['PRDCT_THUMBNAIL']; ?>" alt="Honda CR V" />
<!---<img class="img-responsive" src="assets/img/honda-crv.png" alt="Honda CR v" />--->
</a>
								<!-- /product image(s) -->
<div class="shop-item-summary text-center">
<h2 class="product-name uppercase"><?php echo $prdct_rw['PRDCT_NAME']; ?></h2>
<h2 class="product-desc"><?php echo substr($prdct_rw['SHORT_DESC'], 0, 80). '...'; ?> </h2>
								<!-- price -->
<div class="shop-item-details">
<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='Year'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php } $atrval_val++; } } $attr_val++; } } ?>

<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      	  
      if($atr_rwl['ATTR_NAME']=='Fuel'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php }  $atrval_val++; } } $attr_val++; } } ?> 

<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
     	  
      if($atr_rwl['ATTR_NAME']=='Model'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php }  $atrval_val++; } } $attr_val++; } } ?> 

<?php 
$prdct_tbs = mysql_query("SELECT DISTINCT ATTR_CMBNTN FROM prdct_attr_cmbntn WHERE PRDCT_ID = '$prdct_rw[PRDCT_ID]' ") or die('Error in prdct id !!!');
while($prdct_rws = mysql_fetch_assoc($prdct_tbs)){ 
$sprt_attr = explode(',',$prdct_rws['ATTR_CMBNTN']); $sprt_attr_cnt = count($sprt_attr); $attr_val = 0; 
while($attr_val<$sprt_attr_cnt){ if(isset($sprt_attr[$attr_val])){ 
$sprt_atrval = explode(':',$sprt_attr[$attr_val]); $sprt_atrval_cnt = count($sprt_atrval); $atrval_val = 0; 
while($atrval_val<$sprt_atrval_cnt)
{     $sprt_atrval[$atrval_val];      
      $atr_tbl = mysql_query("SELECT * FROM prdct_attr WHERE ATTR_ID = '$sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $atr_rwl = mysql_fetch_assoc($atr_tbl);
      if($atr_rwl['ATTR_NAME']=='KMs driven'){ $atrval_val = $atrval_val+1; $sprt_atrval[$atrval_val];
	  $val_tbl = mysql_query("SELECT * FROM prdct_attr_value WHERE ATTR_VAL_ID = ' $sprt_atrval[$atrval_val]'") or die("Error in sprt the attribute selecting query !!!");	
      $val_rwl = mysql_fetch_assoc($val_tbl); ?> 
      <span><?php echo $val_rwl['ATTR_VAL']; ?></span> 
      <?php }  $atrval_val++; } } $attr_val++; } } ?> 	  
</div>
								<!-- /price -->
</div>

								<!-- buttons -->
<div class="shop-item-buttons text-center">
<a class="btn btn-default was-now color-white font-roboto weight-500" 
href="<?php echo BASEPATH; ?>buycars/<?php echo $prdct_rw['SEO_URL']; ?>"><span class="color-red border-red margin-right-10">Was</span> 
<?php if(isset($_SESSION['CURRENCY_VALUE'])){ echo $_SESSION['CURRENCY_NAME'].'-'.$prdct_rw['TOTAL_PRICE'] * $_SESSION['CURRENCY_VALUE']; }elseif(isset($prdct_rw['TOTAL_PRICE'])){ echo 'USD'.'-'.$prdct_rw['TOTAL_PRICE']; } ?>
 <i class="padding-left-30 fa fa-arrow-right"></i></a>
</div>

								<!-- /buttons -->    
</div>
</div>
<?php endwhile;  } } endif; ?>
						<!-- /item -->
  <br/>
                    
                     <!-- View All-->
								<div class="col-sm-12 shop-item-buttons text-center margin-bottom-30">
									<a class="btn btn-default btn-grey" href="?discounted_cars_view=100"><i class="fa fa-plus"></i> View All</a>
								 <hr style="margin-bottom: -20px;"/>
								</div>
				    <!-- /View All --> 

				</div>
			</section>
			<!-- /DISCOUNTED CARS -->
 
<!---Remove the duplicate value in select box for search the car  ---->
<script type="text/javascript">
function remove_duplicate_condition(){
var usedNames = {};
$("select[name='condition'] > option").each(function () {
    if(usedNames[this.text]) {
        $(this).remove();
    } else {
        usedNames[this.text] = this.value;
    }
});
}

function remove_duplicate_brand(){
var usedNames = {};
$("select[name='brand'] > option").each(function () {
    if(usedNames[this.text]) {
        $(this).remove();
    } else {
        usedNames[this.text] = this.value;
    }
});
}

function remove_duplicate_model(){
var usedNames = {};
$("select[name='model'] > option").each(function () {
    if(usedNames[this.text]) {
        $(this).remove();
    } else {
        usedNames[this.text] = this.value;
    }
});
}

function remove_duplicate_gear(){
var usedNames = {};
$("select[name='geartype'] > option").each(function () {
    if(usedNames[this.text]) {
        $(this).remove();
    } else {
        usedNames[this.text] = this.value;
    }
});
}

function remove_duplicate_body(){
var usedNames = {};
$("select[name='bodytype'] > option").each(function () {
    if(usedNames[this.text]) {
        $(this).remove();
    } else {
        usedNames[this.text] = this.value;
    }
});
}
</script>
<!---Remove the duplicate value in select box for search the car  ---->
<script type="text/javascript"> 
function chk_cndtn(t)
{
var cndtn = $("#cndtn_id").val();
if($(t).hasClass("cndtn_cls"))
{
}
$.ajax({
	   url:"<?php echo BASEPATH; ?>ajax/search-car.php",
	   type:"post",
	   data:{dc_cndtn:cndtn},
	   success:function(brand_received)
	   {		   
		 var brand=$.parseHTML(brand_received); 
         $("#brand_id").html(brand);		 
	   }
   });  
}

function chk_brand(t)
{
var brand = $("#brand_id").val();
if($(t).hasClass("brand_cls"))
{
}
$.ajax({
	   url:"<?php echo BASEPATH; ?>ajax/search-car.php",
	   type:"post",
	   data:{dc_brand:brand},
	   success:function(model_received)
	   {		   
		 var model=$.parseHTML(model_received); 
         $("#model_id").html(model);		 
	   }
   });
}

function chk_model(t)
{
var model = $("#model_id").val(); 
if($(t).hasClass("model_cls"))
{
}
$.ajax({
	   url:"<?php echo BASEPATH; ?>ajax/search-car.php",
	   type:"post",
	   data:{dc_model:model},
	   success:function(gear_received)
	   {		   
		 var gear=$.parseHTML(gear_received); 
         $("#gear_id").html(gear);		 
	   }
   });
}

function chk_gear(t)
{
var gear = $("#gear_id").val(); 
if($(t).hasClass("gear_cls"))
{
}
$.ajax({
	   url:"<?php echo BASEPATH; ?>ajax/search-car.php",
	   type:"post",
	   data:{dc_gear:gear},
	   success:function(body_received)
	   {		   
		 var body=$.parseHTML(body_received); 
         $("#body_id").html(body);		 
	   }
   });
}
</script>
<?php include_once('footer.php'); ?>